package com.alfaplus.tracker;

import android.content.Context;
import android.content.SharedPreferences;

import com.alfaplus.tracker.adapters.DevicesConfig.DevicesConfig;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.google.gson.Gson;

import java.util.List;

public class Pref {

    private final SharedPreferences preferences;

    public final static String CONFIG_FILE = "config";
    public final static String APP_VERSION_NAME = "version";
    public final static String DEVICE_MOBILE = "deviceNumber";
    public final static String DEVICES_CONFIG = "devicesJson";
    public final static String DEVICE_PASSWORD = "password";
    public final static String DEVICE_NEW_PASSWORD = "new_password";
    public final static String POWER_STATE = "power";
    public final static String ALERT_STATE = "alert";
    public final static String SPY_STATE = "spy";
    public final static String GPRS_STATE = "gprs";
    public final static String USER_API_HASH = "hash";
    public final static String DEMO_MODE = "demoMode";
    public final static String LAST_EVENT_ID = "last_event_id";
    public final static String SENS_LEVEL = "sens";
    public final static String USES_BIOMETRIC = "finger";
    public final static String LANGUAGE = "lang";
    public final static String FIRST_TIME = "first_time";
    public final static String SERVER = "server";
    public final static String MAP_REFRESH_INTERVAL = "map_refresh_interval";
    public final static String EVENT_REFRESH_INTERVAL = "event_refresh_interval";
    public final static String REMOVE_ADMIN_NUMBER = "remove_admin_number";


    public Pref(Context context) {
        preferences = context.getSharedPreferences(CONFIG_FILE, Context.MODE_PRIVATE);
    }

    public String getDeviceMobile() {
        return preferences.getString(DEVICE_MOBILE, "").replaceFirst("0", "+98");
    }

    public String getAppVersionName() {
        return preferences.getString(APP_VERSION_NAME, "");
    }

    public String getLanguage() {
        return preferences.getString(LANGUAGE, "fa");
    }

    public String getPassword() {
        return preferences.getString(DEVICE_PASSWORD, "123456");
    }

    public String getNewPassword() {
        return preferences.getString(DEVICE_NEW_PASSWORD, "123456");
    }

    public boolean getPowerState() {
        return preferences.getBoolean(POWER_STATE, false);
    }

    public boolean getGPRSState() {
        return preferences.getBoolean(GPRS_STATE, false);
    }

    public boolean isDemoMode() {
        return preferences.getBoolean(DEMO_MODE, false);
    }

    public boolean getSpyState() {
        return preferences.getBoolean(SPY_STATE, false);
    }

    public boolean getAlertState() {
        return preferences.getBoolean(ALERT_STATE, true);
    }

    public boolean isFirstTime() {
        return preferences.getBoolean(FIRST_TIME, true);
    }

    public String getUserApiHash() {
        return preferences.getString(USER_API_HASH, "");
    }

    public String getRemoveAdminNumber() {
        return preferences.getString(REMOVE_ADMIN_NUMBER, "");
    }

    public List<DevicesItem> getDevicesConfigs() {
        if (preferences.getString(DEVICES_CONFIG, "").isEmpty()) {
            return null;
        }
        return new Gson().fromJson(preferences.getString(DEVICES_CONFIG, ""),
                DevicesConfig.class).getDevices();
    }

    public boolean getUsesBiometric() {
        return preferences.getBoolean(USES_BIOMETRIC, false);
    }

    public int getSensLevel() {
        return preferences.getInt(SENS_LEVEL, 0);
    }

    public int getLastEventID() {
        return preferences.getInt(LAST_EVENT_ID, 0);
    }

    public String getAPiServer() {
//        return preferences.getString(SERVER, Params.BASE_SERVER_1 + "api/") + "api/";
        return getServer() + "api/";
    }

    public int getMapDelay() {
        return preferences.getInt(MAP_REFRESH_INTERVAL, 15000);
    }

    public int getEventDelay() {
        return preferences.getInt(EVENT_REFRESH_INTERVAL, 15000);
    }

    public String getServer() {
        return preferences.getString(SERVER, Params.BASE_SERVER_1);
    }

    public void setRemoveAdminNumber(String number) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(REMOVE_ADMIN_NUMBER, number);
        editor.apply();
    }

    public void setMapRefreshInterval(int refreshInterval) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(MAP_REFRESH_INTERVAL, refreshInterval);
        editor.apply();
    }

    public void setEventRefreshInterval(int refreshInterval) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(EVENT_REFRESH_INTERVAL, refreshInterval);
        editor.apply();
    }

    public void setServer(String serverAddress) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SERVER, serverAddress);
        editor.apply();
    }

    public void setAppVersionName() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(APP_VERSION_NAME, BuildConfig.VERSION_NAME);
        editor.apply();
    }


    public void setSensLevel(int sensLevel) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(SENS_LEVEL, sensLevel);
        editor.apply();
    }

    public void setLastEventID(int lastEventID) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(LAST_EVENT_ID, lastEventID);
        editor.apply();
    }

    public void setDevicesConfig(List<DevicesItem> devices) {
        DevicesConfig devicesConfig = new DevicesConfig();
        devicesConfig.setDevices(devices);
        SharedPreferences.Editor editor = preferences.edit();
        String json = new Gson().toJson(devicesConfig);
        editor.putString(DEVICES_CONFIG, json);
        editor.apply();
        System.err.println("saved " + json);
    }

    public void setUserApiHash(String userApiHash) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_API_HASH, userApiHash);
        editor.apply();
    }

    public void setLanguage(String language) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LANGUAGE, language);
        editor.apply();
    }

    public void setDeviceMobile(String deviceMobile) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(DEVICE_MOBILE, deviceMobile);
        editor.apply();
    }

    public void setPassword(String password) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(DEVICE_PASSWORD, password);
        editor.apply();
    }

    public void setNewPassword(String password) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(DEVICE_NEW_PASSWORD, password);
        editor.apply();
    }

    public void serPowerState(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(POWER_STATE, state);
        editor.apply();
    }

    public void setFirstTime(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(FIRST_TIME, state);
        editor.apply();
    }

    public void setSpyState(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(SPY_STATE, state);
        editor.apply();
    }

    public void setAlertState(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ALERT_STATE, state);
        editor.apply();
    }

    public void setDemoMode(boolean isDemo) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(DEMO_MODE, isDemo);
        editor.apply();
    }

    public void setGPRSState(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(GPRS_STATE, state);
        editor.apply();
    }

    public void setUsesBiometric(boolean state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(USES_BIOMETRIC, state);
        editor.apply();
    }


    public void clearPrefs() {
        String language = getLanguage();
        String serverAddress = getServer();
        boolean firstTime = isFirstTime();
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
//        editor.remove(DEVICE_MOBILE);
//        editor.remove(DEVICE_PASSWORD);
//        editor.remove(DEVICE_NEW_PASSWORD);
//        editor.remove(USER_API_HASH);
//        editor.remove(POWER_STATE);
//        editor.remove(ALERT_STATE);
//        editor.remove(SPY_STATE);
//        editor.remove(GPRS_STATE);
//        editor.remove(DEVICES_CONFIG);
//        editor.remove(DEMO_MODE);
//        editor.remove(LAST_EVENT_ID);
//        editor.remove(SENS_LEVEL);
        editor.apply();
        setLanguage(language);
        setServer(serverAddress);
        setFirstTime(firstTime);
    }
}
