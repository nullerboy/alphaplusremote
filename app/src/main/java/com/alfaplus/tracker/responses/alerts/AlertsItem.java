package com.alfaplus.tracker.responses.alerts;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AlertsItem{

	@SerializedName("schedule")
	private int schedule;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("zone")
	private int zone;

	@SerializedName("devices")
	private List<Integer> devices;

	@SerializedName("name")
	private String name;

	@SerializedName("active")
	private int active;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	public void setSchedule(int schedule){
		this.schedule = schedule;
	}

	public int getSchedule(){
		return schedule;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setZone(int zone){
		this.zone = zone;
	}

	public int getZone(){
		return zone;
	}

	public void setDevices(List<Integer> devices){
		this.devices = devices;
	}

	public List<Integer> getDevices(){
		return devices;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setActive(int active){
		this.active = active;
	}

	public int getActive(){
		return active;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	@Override
 	public String toString(){
		return 
			"AlertsItem{" + 
			"schedule = '" + schedule + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",user_id = '" + userId + '\'' + 
			",zone = '" + zone + '\'' + 
			",devices = '" + devices + '\'' + 
			",name = '" + name + '\'' + 
			",active = '" + active + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",type = '" + type + '\'' + 
			"}";
		}
}