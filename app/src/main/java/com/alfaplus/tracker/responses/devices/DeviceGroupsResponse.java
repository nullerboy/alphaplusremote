package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeviceGroupsResponse{

	@SerializedName("DeviceGroupsResponse")
	private List<DeviceGroupsResponseItem> deviceGroupsResponse;

	public void setDeviceGroupsResponse(List<DeviceGroupsResponseItem> deviceGroupsResponse){
		this.deviceGroupsResponse = deviceGroupsResponse;
	}

	public List<DeviceGroupsResponseItem> getDeviceGroupsResponse(){
		return deviceGroupsResponse;
	}

	@Override
 	public String toString(){
		return 
			"DeviceGroupsResponse{" + 
			"deviceGroupsResponse = '" + deviceGroupsResponse + '\'' + 
			"}";
		}
}