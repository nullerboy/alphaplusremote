package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

public class Icon{

	@SerializedName("path")
	private String path;

	@SerializedName("by_status")
	private int byStatus;

	@SerializedName("width")
	private int width;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@SerializedName("order")
	private int order;

	@SerializedName("height")
	private int height;

	public void setPath(String path){
		this.path = path;
	}

	public String getPath(){
		return path;
	}

	public void setByStatus(int byStatus){
		this.byStatus = byStatus;
	}

	public int getByStatus(){
		return byStatus;
	}

	public void setWidth(int width){
		this.width = width;
	}

	public int getWidth(){
		return width;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setOrder(int order){
		this.order = order;
	}

	public int getOrder(){
		return order;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	@Override
 	public String toString(){
		return 
			"Icon{" + 
			"path = '" + path + '\'' + 
			",by_status = '" + byStatus + '\'' + 
			",width = '" + width + '\'' + 
			",id = '" + id + '\'' + 
			",type = '" + type + '\'' + 
			",order = '" + order + '\'' + 
			",height = '" + height + '\'' + 
			"}";
		}
}