package com.alfaplus.tracker.responses.events;

import com.google.gson.annotations.SerializedName;

public class DataItem {

    @SerializedName("altitude")
    private int altitude;

    @SerializedName("device_id")
    private int deviceId;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("type")
    private String type;

    @SerializedName("message")
    private String message;

    @SerializedName("speed")
    private int speed;

    @SerializedName("device_name")
    private String deviceName;

    @SerializedName("deleted")
    private int deleted;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("user_id")
    private int userId;

    @SerializedName("alert_id")
    private int alertId;

    @SerializedName("name")
    private String name;

    @SerializedName("course")
    private double course;

    @SerializedName("id")
    private int id;

    @SerializedName("time")
    private String time;

    @SerializedName("detail")
    private String detail;

    @SerializedName("position_id")
    private int positionId;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("additional")
    private Additional additional;

    public Additional getAdditional() {
        return additional;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setAlertId(int alertId) {
        this.alertId = alertId;
    }

    public int getAlertId() {
        return alertId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    public double getCourse() {
        return course;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "altitude = '" + altitude + '\'' +
                        ",device_id = '" + deviceId + '\'' +
                        ",latitude = '" + latitude + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",type = '" + type + '\'' +
                        ",message = '" + message + '\'' +
                        ",speed = '" + speed + '\'' +
                        ",device_name = '" + deviceName + '\'' +
                        ",deleted = '" + deleted + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",user_id = '" + userId + '\'' +
                        ",alert_id = '" + alertId + '\'' +
                        ",name = '" + name + '\'' +
                        ",course = '" + course + '\'' +
                        ",id = '" + id + '\'' +
                        ",time = '" + time + '\'' +
                        ",detail = '" + detail + '\'' +
                        ",position_id = '" + positionId + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        "}";
    }
}