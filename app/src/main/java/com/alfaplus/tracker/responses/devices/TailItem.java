package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

public class TailItem{

	@SerializedName("lng")
	private Double lng;

	@SerializedName("lat")
	private Double lat;

	public void setLng(Double lng){
		this.lng = lng;
	}

	public Double getLng(){
		return lng;
	}

	public void setLat(Double lat){
		this.lat = lat;
	}

	public Double getLat(){
		return lat;
	}

	@Override
 	public String toString(){
		return 
			"TailItem{" + 
			"lng = '" + lng + '\'' + 
			",lat = '" + lat + '\'' + 
			"}";
		}
}