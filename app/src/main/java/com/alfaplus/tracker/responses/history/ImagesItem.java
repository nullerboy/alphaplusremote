package com.alfaplus.tracker.responses.history;

import com.google.gson.annotations.SerializedName;

public class ImagesItem{

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	@SerializedName("value")
	private String value;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"ImagesItem{" + 
			"id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}