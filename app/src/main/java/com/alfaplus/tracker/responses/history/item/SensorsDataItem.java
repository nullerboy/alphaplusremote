package com.alfaplus.tracker.responses.history.item;

import com.google.gson.annotations.SerializedName;

public class SensorsDataItem{

	@SerializedName("id")
	private String id;

	@SerializedName("value")
	private double value;

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setValue(double value){
		this.value = value;
	}

	public double getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"SensorsDataItem{" + 
			"id = '" + id + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}