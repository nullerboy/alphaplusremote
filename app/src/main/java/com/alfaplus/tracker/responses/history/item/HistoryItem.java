package com.alfaplus.tracker.responses.history.item;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HistoryItem{

	@SerializedName("raw_time")
	private String rawTime;

	@SerializedName("time_seconds")
	private int timeSeconds;

	@SerializedName("distance")
	private double distance;

	@SerializedName("show")
	private String show;

	@SerializedName("engine_work")
	private int engineWork;

	@SerializedName("engine_hours")
	private int engineHours;

	@SerializedName("engine_idle")
	private int engineIdle;

	@SerializedName("driver")
	private Object driver;

	@SerializedName("left")
	private String left;

	@SerializedName("fuel_consumption")
	private Object fuelConsumption;

	@SerializedName("top_speed")
	private int topSpeed;

	@SerializedName("time")
	private String time;

	@SerializedName("average_speed")
	private int averageSpeed;

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("status")
	private int status;

	public void setRawTime(String rawTime){
		this.rawTime = rawTime;
	}

	public String getRawTime(){
		return rawTime;
	}

	public void setTimeSeconds(int timeSeconds){
		this.timeSeconds = timeSeconds;
	}

	public int getTimeSeconds(){
		return timeSeconds;
	}

	public void setDistance(double distance){
		this.distance = distance;
	}

	public double getDistance(){
		return distance;
	}

	public void setShow(String show){
		this.show = show;
	}

	public String getShow(){
		return show;
	}

	public void setEngineWork(int engineWork){
		this.engineWork = engineWork;
	}

	public int getEngineWork(){
		return engineWork;
	}

	public void setEngineHours(int engineHours){
		this.engineHours = engineHours;
	}

	public int getEngineHours(){
		return engineHours;
	}

	public void setEngineIdle(int engineIdle){
		this.engineIdle = engineIdle;
	}

	public int getEngineIdle(){
		return engineIdle;
	}

	public void setDriver(Object driver){
		this.driver = driver;
	}

	public Object getDriver(){
		return driver;
	}

	public void setLeft(String left){
		this.left = left;
	}

	public String getLeft(){
		return left;
	}

	public void setFuelConsumption(Object fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}

	public Object getFuelConsumption(){
		return fuelConsumption;
	}

	public void setTopSpeed(int topSpeed){
		this.topSpeed = topSpeed;
	}

	public int getTopSpeed(){
		return topSpeed;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setAverageSpeed(int averageSpeed){
		this.averageSpeed = averageSpeed;
	}

	public int getAverageSpeed(){
		return averageSpeed;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"HistoryItem{" + 
			"raw_time = '" + rawTime + '\'' + 
			",time_seconds = '" + timeSeconds + '\'' + 
			",distance = '" + distance + '\'' + 
			",show = '" + show + '\'' + 
			",engine_work = '" + engineWork + '\'' + 
			",engine_hours = '" + engineHours + '\'' + 
			",engine_idle = '" + engineIdle + '\'' + 
			",driver = '" + driver + '\'' + 
			",left = '" + left + '\'' + 
			",fuel_consumption = '" + fuelConsumption + '\'' + 
			",top_speed = '" + topSpeed + '\'' + 
			",time = '" + time + '\'' + 
			",average_speed = '" + averageSpeed + '\'' + 
			",items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}