package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

public class IconColors{

	@SerializedName("offline")
	private String offline;

	@SerializedName("stopped")
	private String stopped;

	@SerializedName("engine")
	private String engine;

	@SerializedName("moving")
	private String moving;

	public void setOffline(String offline){
		this.offline = offline;
	}

	public String getOffline(){
		return offline;
	}

	public void setStopped(String stopped){
		this.stopped = stopped;
	}

	public String getStopped(){
		return stopped;
	}

	public void setEngine(String engine){
		this.engine = engine;
	}

	public String getEngine(){
		return engine;
	}

	public void setMoving(String moving){
		this.moving = moving;
	}

	public String getMoving(){
		return moving;
	}

	@Override
 	public String toString(){
		return 
			"IconColors{" + 
			"offline = '" + offline + '\'' + 
			",stopped = '" + stopped + '\'' + 
			",engine = '" + engine + '\'' + 
			",moving = '" + moving + '\'' + 
			"}";
		}
}