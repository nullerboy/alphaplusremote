package com.alfaplus.tracker.responses.devices;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Sensors{

	@SerializedName("Sensors")
	private List<SensorsItem> sensors;

	public void setSensors(List<SensorsItem> sensors){
		this.sensors = sensors;
	}

	public List<SensorsItem> getSensors(){
		return sensors;
	}

	@Override
 	public String toString(){
		return 
			"Sensors{" + 
			"sensors = '" + sensors + '\'' + 
			"}";
		}
}