package com.alfaplus.tracker.responses;

import com.google.gson.annotations.SerializedName;

public class ResponseDelays{

	@SerializedName("event_refresh_delay")
	private int eventRefreshDelay;

	@SerializedName("map_refresh_delay")
	private int mapRefreshDelay;

	public int getEventRefreshDelay(){
		return eventRefreshDelay;
	}

	public int getMapRefreshDelay(){
		return mapRefreshDelay;
	}
}