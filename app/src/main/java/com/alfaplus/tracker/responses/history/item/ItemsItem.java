package com.alfaplus.tracker.responses.history.item;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemsItem{

	@SerializedName("raw_time")
	private String rawTime;

	@SerializedName("altitude")
	private double altitude;

	@SerializedName("server_time")
	private String serverTime;

	@SerializedName("other")
	private String other;

	@SerializedName("device_id")
	private int deviceId;

	@SerializedName("lng")
	private double lng;

	@SerializedName("distance")
	private double distance;

	@SerializedName("color")
	private String color;

	@SerializedName("item_id")
	private String itemId;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("speed")
	private int speed;

	@SerializedName("valid")
	private int valid;

	@SerializedName("sensors_data")
	private List<SensorsDataItem> sensorsData;

	@SerializedName("course")
	private double course;

	@SerializedName("id")
	private int id;

	@SerializedName("time")
	private String time;

	@SerializedName("other_arr")
	private List<String> otherArr;

	@SerializedName("lat")
	private double lat;

	@SerializedName("longitude")
	private double longitude;

	@SerializedName("device_time")
	private String deviceTime;

	public void setRawTime(String rawTime){
		this.rawTime = rawTime;
	}

	public String getRawTime(){
		return rawTime;
	}

	public void setAltitude(int altitude){
		this.altitude = altitude;
	}

	public double getAltitude(){
		return altitude;
	}

	public void setServerTime(String serverTime){
		this.serverTime = serverTime;
	}

	public String getServerTime(){
		return serverTime;
	}

	public void setOther(String other){
		this.other = other;
	}

	public String getOther(){
		return other;
	}

	public void setDeviceId(int deviceId){
		this.deviceId = deviceId;
	}

	public int getDeviceId(){
		return deviceId;
	}

	public void setLng(double lng){
		this.lng = lng;
	}

	public double getLng(){
		return lng;
	}

	public void setDistance(double distance){
		this.distance = distance;
	}

	public double getDistance(){
		return distance;
	}

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setItemId(String itemId){
		this.itemId = itemId;
	}

	public String getItemId(){
		return itemId;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setSpeed(int speed){
		this.speed = speed;
	}

	public int getSpeed(){
		return speed;
	}

	public void setValid(int valid){
		this.valid = valid;
	}

	public int getValid(){
		return valid;
	}

	public void setSensorsData(List<SensorsDataItem> sensorsData){
		this.sensorsData = sensorsData;
	}

	public List<SensorsDataItem> getSensorsData(){
		return sensorsData;
	}

	public void setCourse(double course){
		this.course = course;
	}

	public double getCourse(){
		return course;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTime(String time){
		this.time = time;
	}

	public String getTime(){
		return time;
	}

	public void setOtherArr(List<String> otherArr){
		this.otherArr = otherArr;
	}

	public List<String> getOtherArr(){
		return otherArr;
	}

	public void setLat(double lat){
		this.lat = lat;
	}

	public double getLat(){
		return lat;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public void setDeviceTime(String deviceTime){
		this.deviceTime = deviceTime;
	}

	public String getDeviceTime(){
		return deviceTime;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"raw_time = '" + rawTime + '\'' + 
			",altitude = '" + altitude + '\'' + 
			",server_time = '" + serverTime + '\'' + 
			",other = '" + other + '\'' + 
			",device_id = '" + deviceId + '\'' + 
			",lng = '" + lng + '\'' + 
			",distance = '" + distance + '\'' + 
			",color = '" + color + '\'' + 
			",item_id = '" + itemId + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",speed = '" + speed + '\'' + 
			",valid = '" + valid + '\'' + 
			",sensors_data = '" + sensorsData + '\'' + 
			",course = '" + course + '\'' + 
			",id = '" + id + '\'' + 
			",time = '" + time + '\'' + 
			",other_arr = '" + otherArr + '\'' + 
			",lat = '" + lat + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",device_time = '" + deviceTime + '\'' + 
			"}";
		}
}