package com.alfaplus.tracker.responses.authentication;

import com.google.gson.annotations.SerializedName;

public class Authentication{

	@SerializedName("user_api_hash")
	private String userApiHash;

	@SerializedName("status")
	private int status;

	public String getUserApiHash(){
		return userApiHash;
	}

	public int getStatus(){
		return status;
	}
}