package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

public class SensorsItem{


	@SerializedName("name")
	private String name;

	@SerializedName("show_in_popup")
	private int showInPopup;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@SerializedName("value")
	private String value;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setShowInPopup(int showInPopup){
		this.showInPopup = showInPopup;
	}

	public int getShowInPopup(){
		return showInPopup;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"SensorsItem{" + 
			",name = '" + name + '\'' +
			",show_in_popup = '" + showInPopup + '\'' + 
			",id = '" + id + '\'' + 
			",type = '" + type + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}