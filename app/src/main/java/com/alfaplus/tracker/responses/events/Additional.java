package com.alfaplus.tracker.responses.events;

import com.google.gson.annotations.SerializedName;

public class Additional{

	@SerializedName("overspeed_speed")
	private int overspeedSpeed;

	public void setOverspeedSpeed(int overspeedSpeed){
		this.overspeedSpeed = overspeedSpeed;
	}

	public int getOverspeedSpeed(){
		return overspeedSpeed;
	}

	@Override
 	public String toString(){
		return 
			"Additional{" + 
			"overspeed_speed = '" + overspeedSpeed + '\'' + 
			"}";
		}
}