package com.alfaplus.tracker.responses.history;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HistoryResponse{

	@SerializedName("images")
	private List<ImagesItem> images;

	@SerializedName("top_speed")
	private String topSpeed;

	@SerializedName("distance_sum")
	private String distanceSum;

	@SerializedName("items")
	private List<ItemsItem> items;

	@SerializedName("stop_duration")
	private String stopDuration;

	@SerializedName("move_duration")
	private String moveDuration;

	@SerializedName("status")
	private int status;

	public void setImages(List<ImagesItem> images){
		this.images = images;
	}

	public List<ImagesItem> getImages(){
		return images;
	}

	public void setTopSpeed(String topSpeed){
		this.topSpeed = topSpeed;
	}

	public String getTopSpeed(){
		return topSpeed;
	}

	public void setDistanceSum(String distanceSum){
		this.distanceSum = distanceSum;
	}

	public String getDistanceSum(){
		return distanceSum;
	}

	public void setItems(List<ItemsItem> items){
		this.items = items;
	}

	public List<ItemsItem> getItems(){
		return items;
	}

	public void setStopDuration(String stopDuration){
		this.stopDuration = stopDuration;
	}

	public String getStopDuration(){
		return stopDuration;
	}

	public void setMoveDuration(String moveDuration){
		this.moveDuration = moveDuration;
	}

	public String getMoveDuration(){
		return moveDuration;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"HistoryResponse{" + 
			"images = '" + images + '\'' + 
			",top_speed = '" + topSpeed + '\'' + 
			",distance_sum = '" + distanceSum + '\'' + 
			",items = '" + items + '\'' + 
			",stop_duration = '" + stopDuration + '\'' + 
			",move_duration = '" + moveDuration + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}