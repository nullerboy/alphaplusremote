package com.alfaplus.tracker.responses.history;

import com.google.gson.annotations.SerializedName;

public class ItemsItem{

	@SerializedName("status")
	private int status;

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ItemsItem{" + 
			"status = '" + status + '\'' + 
			"}";
		}
}