package com.alfaplus.tracker.responses.alerts;

import com.google.gson.annotations.SerializedName;

public class AlertsResponse{

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AlertsResponse{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}