package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DeviceGroupsResponseItem {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;
    @SerializedName("items")
    private List<ItemsItem> items;

    public void setItems(List<ItemsItem> items) {
        this.items = items;
    }

    public List<ItemsItem> getItems() {
        return items;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return
                "DeviceGroupsResponseItem{" +
                        "id = '" + id + '\'' +
                        ",title = '" + title + '\'' +
                        "}";
    }
}