package com.alfaplus.tracker.responses.devices;

import com.google.gson.annotations.SerializedName;

public class DeviceData {

    @SerializedName("imei")
    private String imei;

    @SerializedName("sim_number")
    private String simNumber;

    @SerializedName("id")
    private int id;

    @SerializedName("device_model")
    private String deviceModel;

    @SerializedName("additional_notes")
    private String additionalNotes;

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImei() {
        return imei;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return
                "DeviceData{" +
                        "imei = '" + imei + '\'' +
                        ",sim_number = '" + simNumber + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }
}