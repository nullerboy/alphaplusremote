package com.alfaplus.tracker.responses.alerts;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Items{

	@SerializedName("alerts")
	private List<AlertsItem> alerts;

	public void setAlerts(List<AlertsItem> alerts){
		this.alerts = alerts;
	}

	public List<AlertsItem> getAlerts(){
		return alerts;
	}

	@Override
 	public String toString(){
		return 
			"Items{" + 
			"alerts = '" + alerts + '\'' + 
			"}";
		}
}