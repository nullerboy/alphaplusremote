package com.alfaplus.tracker.responses.events;

import com.google.gson.annotations.SerializedName;

public class EventsResponse{

	@SerializedName("items")
	private Items items;

	@SerializedName("status")
	private int status;

	public void setItems(Items items){
		this.items = items;
	}

	public Items getItems(){
		return items;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"EventsResponse{" + 
			"items = '" + items + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}