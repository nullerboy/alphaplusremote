package com.alfaplus.tracker.responses.devices;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ItemsItem {

    @SerializedName("altitude")
    private int altitude;

    @SerializedName("distance_unit_hour")
    private String distanceUnitHour;

    @SerializedName("icon")
    private Icon icon;

    @SerializedName("engine_hours")
    private String engineHours;

    @SerializedName("speed")
    private int speed;

    @SerializedName("acktimestamp")
    private int acktimestamp;

    @SerializedName("protocol")
    private String protocol;

    @SerializedName("alarm")
    private int alarm;

    @SerializedName("total_distance")
    private double totalDistance;

    @SerializedName("course")
    private int course;

    @SerializedName("stop_duration_sec")
    private int stopDurationSec;

    @SerializedName("unit_of_distance")
    private String unitOfDistance;

    @SerializedName("id")
    private int id;

    @SerializedName("power")
    private String power;

    @SerializedName("unit_of_capacity")
    private String unitOfCapacity;

    @SerializedName("lat")
    private double lat;

    @SerializedName("timestamp")
    private int timestamp;

    @SerializedName("icon_type")
    private String iconType;

    @SerializedName("address")
    private String address;

    @SerializedName("lng")
    private double lng;

    @SerializedName("unit_of_altitude")
    private String unitOfAltitude;

    @SerializedName("icon_colors")
    private IconColors iconColors;

    @SerializedName("tail")
    private List<TailItem> tail;

    @SerializedName("engine_status")
    private String engineStatus;

    @SerializedName("services")
    private List<Object> services;

    @SerializedName("device_data")
    private DeviceData deviceData;

    @SerializedName("icon_color")
    private String iconColor;

    @SerializedName("driver")
    private String driver;

    @SerializedName("name")
    private String name;

    @SerializedName("detect_engine")
    private String detectEngine;

    @SerializedName("online")
    private String online;

    @SerializedName("time")
    private String time;

    @SerializedName("moved_timestamp")
    private int movedTimestamp;

    @SerializedName("stop_duration")
    private String stopDuration;

    @SerializedName("sensors")
    private List<SensorsItem> sensorsItems;

    public List<SensorsItem> getSensorsItems() {
        return sensorsItems;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setDistanceUnitHour(String distanceUnitHour) {
        this.distanceUnitHour = distanceUnitHour;
    }

    public String getDistanceUnitHour() {
        return distanceUnitHour;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setEngineHours(String engineHours) {
        this.engineHours = engineHours;
    }

    public String getEngineHours() {
        return engineHours;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    public void setAcktimestamp(int acktimestamp) {
        this.acktimestamp = acktimestamp;
    }

    public int getAcktimestamp() {
        return acktimestamp;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setAlarm(int alarm) {
        this.alarm = alarm;
    }

    public int getAlarm() {
        return alarm;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public int getCourse() {
        return course;
    }

    public void setStopDurationSec(int stopDurationSec) {
        this.stopDurationSec = stopDurationSec;
    }

    public int getStopDurationSec() {
        return stopDurationSec;
    }

    public void setUnitOfDistance(String unitOfDistance) {
        this.unitOfDistance = unitOfDistance;
    }

    public String getUnitOfDistance() {
        return unitOfDistance;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getPower() {
        return power;
    }

    public void setUnitOfCapacity(String unitOfCapacity) {
        this.unitOfCapacity = unitOfCapacity;
    }

    public String getUnitOfCapacity() {
        return unitOfCapacity;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLat() {
        return lat;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setIconType(String iconType) {
        this.iconType = iconType;
    }

    public String getIconType() {
        return iconType;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLng() {
        return lng;
    }

    public void setUnitOfAltitude(String unitOfAltitude) {
        this.unitOfAltitude = unitOfAltitude;
    }

    public String getUnitOfAltitude() {
        return unitOfAltitude;
    }

    public void setIconColors(IconColors iconColors) {
        this.iconColors = iconColors;
    }

    public IconColors getIconColors() {
        return iconColors;
    }

    public void setTail(List<TailItem> tail) {
        this.tail = tail;
    }

    public List<TailItem> getTail() {
        return tail;
    }

    public void setEngineStatus(String engineStatus) {
        this.engineStatus = engineStatus;
    }

    public String getEngineStatus() {
        return engineStatus;
    }

    public void setServices(List<Object> services) {
        this.services = services;
    }

    public List<Object> getServices() {
        return services;
    }

    public void setDeviceData(DeviceData deviceData) {
        this.deviceData = deviceData;
    }

    public DeviceData getDeviceData() {
        return deviceData;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDriver() {
        return driver;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDetectEngine(String detectEngine) {
        this.detectEngine = detectEngine;
    }

    public String getDetectEngine() {
        return detectEngine;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getOnline() {
        return online;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setMovedTimestamp(int movedTimestamp) {
        this.movedTimestamp = movedTimestamp;
    }

    public int getMovedTimestamp() {
        return movedTimestamp;
    }

    public void setStopDuration(String stopDuration) {
        this.stopDuration = stopDuration;
    }

    public String getStopDuration() {
        return stopDuration;
    }

    @Override
    public String toString() {
        return
                "ItemsItem{" +
                        "altitude = '" + altitude + '\'' +
                        ",distance_unit_hour = '" + distanceUnitHour + '\'' +
                        ",icon = '" + icon + '\'' +
                        ",engine_hours = '" + engineHours + '\'' +
                        ",speed = '" + speed + '\'' +
                        ",acktimestamp = '" + acktimestamp + '\'' +
                        ",protocol = '" + protocol + '\'' +
                        ",alarm = '" + alarm + '\'' +
                        ",total_distance = '" + totalDistance + '\'' +
                        ",course = '" + course + '\'' +
                        ",stop_duration_sec = '" + stopDurationSec + '\'' +
                        ",unit_of_distance = '" + unitOfDistance + '\'' +
                        ",id = '" + id + '\'' +
                        ",power = '" + power + '\'' +
                        ",unit_of_capacity = '" + unitOfCapacity + '\'' +
                        ",lat = '" + lat + '\'' +
                        ",timestamp = '" + timestamp + '\'' +
                        ",icon_type = '" + iconType + '\'' +
                        ",address = '" + address + '\'' +
                        ",lng = '" + lng + '\'' +
                        ",unit_of_altitude = '" + unitOfAltitude + '\'' +
                        ",icon_colors = '" + iconColors + '\'' +
                        ",tail = '" + tail + '\'' +
                        ",engine_status = '" + engineStatus + '\'' +
                        ",services = '" + services + '\'' +
                        ",device_data = '" + deviceData + '\'' +
                        ",icon_color = '" + iconColor + '\'' +
                        ",driver = '" + driver + '\'' +
                        ",name = '" + name + '\'' +
                        ",detect_engine = '" + detectEngine + '\'' +
                        ",online = '" + online + '\'' +
                        ",time = '" + time + '\'' +
                        ",moved_timestamp = '" + movedTimestamp + '\'' +
                        ",stop_duration = '" + stopDuration + '\'' +
                        "}";
    }
}