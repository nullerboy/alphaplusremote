package com.alfaplus.tracker;

import com.alfaplus.tracker.responses.alerts.AlertsResponse;
import com.alfaplus.tracker.responses.authentication.Authentication;
import com.alfaplus.tracker.responses.events.EventsResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    @POST("login")
    @FormUrlEncoded
    Call<Authentication> Authentication(@Field("email") String email,
                                        @Field("password") String password);

    @GET("get_devices")
    Call<ResponseBody> getDevices(@Query("user_api_hash") String userHash);

    @GET("get_history")
    Call<ResponseBody> getHistory(@Query("user_api_hash") String userHash,
                                  @Query("device_id") int deviceId,
                                  @Query("from_date") String startDate,
                                  @Query("from_time") String startTime,
                                  @Query("to_date") String toDate,
                                  @Query("to_time") String toTime,
                                  @Query("stops") int time);

    @POST("send_gprs_command")
    @FormUrlEncoded
    Call<ResponseBody> sendCommand(@Query("user_api_hash") String userHash,
                                   @Field("device_id") int deviceId,
                                   @Field("message") String message,
                                   @Field("type") String type);

    @GET("get_events")
    Call<EventsResponse> getEvents(@Query("user_api_hash") String userHash);

    @GET("get_events")
    Call<EventsResponse> getEvents(@Query("user_api_hash") String userHash,
                                   @Query("device_id") int deviceId);

    @GET("get_events")
    Call<EventsResponse> getEvents(@Query("user_api_hash") String userHash,
                                   @Query("type") String type);

    @GET("get_events")
    Call<EventsResponse> getEvents(@Query("user_api_hash") String userHash,
                                   @Query("device_id") int deviceId,
                                   @Query("date_from") String dateFrom,
                                   @Query("date_to") String dateTo);

    @GET("get_events")
    Call<EventsResponse> getEvents(@Query("user_api_hash") String userHash,
                                   @Query("date_from") String dateFrom,
                                   @Query("date_to") String dateTo);

    @POST("add_alert")
    @FormUrlEncoded
    Call<ResponseBody> addAlert(@Query("user_api_hash") String userHash,
                                @Field("name") String name,
                                @Field("type") String type,
                                @Field("overspeed") int speed,
                                @Field("devices[]") Integer[] deviceId);

    @POST("edit_alert")
    @FormUrlEncoded
    Call<ResponseBody> editAlert(@Query("user_api_hash") String userHash,
                                 @Field("name") String name,
                                 @Field("type") String type,
                                 @Field("overspeed") int speed,
                                 @Field("devices[]") Integer[] deviceId);

    @GET("get_alerts")
    Call<AlertsResponse> getAlerts(@Query("user_api_hash") String userHash);

    @GET("destroy_alert")
    Call<ResponseBody> removeAlert(@Query("user_api_hash") String userHash,
                                   @Query("alert_id") int alertId);

    @POST("edit_device")
    @FormUrlEncoded
    Call<ResponseBody> editDevice(@Query("user_api_hash") String userHash,
                                  @Query("device_id") int deviceId,
                                  @Field("fuel_measurement_id") int fuelMeasurementId,
                                  @Field("name") String name,
                                  @Field("sim_number") String simNumber);

    @POST("edit_device")
    @FormUrlEncoded
    Call<ResponseBody> editDeviceNote(@Query("user_api_hash") String userHash,
                                      @Query("device_id") int deviceId,
                                      @Field("fuel_measurement_id") int fuelMeasurementId,
                                      @Field("name") String name,
                                      @Field("additional_notes") String additionalNotes);
}
