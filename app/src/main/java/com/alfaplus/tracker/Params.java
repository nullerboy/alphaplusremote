package com.alfaplus.tracker;

public class Params {


    public static String TAG_REMOTE_FRAGMENT = "remoteFragment";
    public static String TAG_SETTINGS_FRAGMENT = "settingsFragment";
    public static String TAG_LOCATION_FRAGMENT = "locationFragment";
    public static String TAG_HISTORY_FRAGMENT = "historyFragment";
    public static String TAG_ABOUT_FRAGMENT = "aboutFragment";
    public static String TAG = "Alpha Logger --> ";

    public static String DEMO_USERNAME = "demo@demo.com";
    public static String DEMO_PASSWORD = "123456";
    public static String DEMO_MOBILE = "09123456789";
    public static String BASE_SERVER_1 = "http://alfaamarket.com/";
    public static String BASE_SERVER_2 = "http://188.121.109.22/";
//    public static int REFRESH_INTERVAL = 3000;
    public static int HISTORY_STOPS_SECONDS = 180;

}
