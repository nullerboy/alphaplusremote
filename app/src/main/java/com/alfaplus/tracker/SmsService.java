package com.alfaplus.tracker;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.alfaplus.tracker.activities.NoticeActivity;

public class SmsService extends Service {

    private Pref pref;
    private SMSListener smsListener;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("SmsService", "Service onCreate.");
        smsListener = new SMSListener();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.e("SmsService", "Service onStartCommand.");
        smsListener.setSmsInterface(new SMSInterface() {
            @Override
            public void onReceive(String number, String message) {
                pref = new Pref(getBaseContext());
                Log.e("SmsService", "Number: " + number + " ,Message: " + message);
                if (number.equals(pref.getDeviceMobile())) {

                    if (pref.getAlertState()) {
                        Intent start = new Intent(getBaseContext(), NoticeActivity.class);
                        start.putExtra("message", message);
                        start.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getBaseContext().startActivity(start);
                    }

                }
            }
        });
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getBaseContext(), "Service Stopped.", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}
