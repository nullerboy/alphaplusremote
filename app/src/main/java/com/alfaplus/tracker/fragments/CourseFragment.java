package com.alfaplus.tracker.fragments;

import static com.alfaplus.tracker.Params.HISTORY_STOPS_SECONDS;
import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.alfaplus.tracker.activities.BaseActivity;
import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.alfaplus.tracker.responses.history.item.HistoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint({"NonConstantResourceId", "UseCompatLoadingForDrawables"})
public class CourseFragment extends Fragment {

    GoogleMap map;

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.spinner_devices)
    MaterialSpinner spinnerDevices;

    @BindView(R.id.card_detail)
    CardView cardDetail;

    @BindView(R.id.text_speed)
    TextView textSpeed;
    @BindView(R.id.text_time)
    TextView textTime;
    @BindView(R.id.text_distance)
    TextView textDistance;

    int selectedDevice = 0;
    int selectedDate = 0;

    public static int selectedHistory = 0;
    public static int selectedDeviceId = 0;
    public static String startDate = "";
    public static String endDate = "";
    List<HistoryItem> historyItems = new ArrayList<>();
    Bundle savedInstanceState;
    List<ItemsItem> devices = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_course, container, false);
        this.savedInstanceState = savedInstanceState;
        ButterKnife.bind(this, view);
        spinnerDevices.setOnItemSelectedListener((view1, position, id, item) -> {
//                selectedHistory = 0;
//                selectedDeviceId = 0;

        });


        Log.e(Params.TAG, "onCreateView: " + devices.size());

        return view;
    }

    @SuppressLint("SimpleDateFormat")
    public void getDevices() {
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                devices = devicesResponse.getItems();
                                TabbedFragment.devicesName = new ArrayList<>();
                                ItemsItem item = devices.get(selectedDeviceId);
                                for (ItemsItem itemsItem : devices) {
                                    TabbedFragment.devicesName.add(itemsItem.getName());
                                }
                                spinnerDevices.setItems(TabbedFragment.devicesName);
                                spinnerDevices.setSelectedIndex(selectedDeviceId);

                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);
                                String end = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
                                String start = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
//                                getHistory(selectedHistory,
//                                        item.getId(),
//                                        startDate.isEmpty() ? start : startDate,
//                                        endDate.isEmpty() ? end : endDate);
//                                getHistory(selectedHistory,
//                                        item.getId(),
//                                        startDate.isEmpty() ? start : startDate,
//                                        endDate.isEmpty() ? end : endDate);

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.e(Params.TAG, "onResponse Code:" + response.code());
//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();

                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("onFailure", "onFailure: " + t.getLocalizedMessage());
                    }
                });
    }

    public void getHistory(int historyId, int deviceId, String startDate, String startTime, String toDate, String toTime) {
        Log.e(TAG, "getUserVisibleHint2: selectedHistory=" + historyId + " , selectedDeviceId=" + deviceId + " " + startDate + " -> " + toDate);
        BaseActivity.api.getHistory(BaseActivity.pref.getUserApiHash(),
                deviceId,
                startDate,
                startTime,
                toDate,
                toTime,
                HISTORY_STOPS_SECONDS)
                .enqueue(new Callback<ResponseBody>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            try {
                                JSONObject resObject = new JSONObject(response.body().string());
                                JSONArray itemsArray = resObject.getJSONArray("items");

                                for (int i = 0; i < itemsArray.length(); i++) {
                                    JSONObject item = itemsArray.getJSONObject(i);
                                    int status = item.getInt("status");
                                    if (status == 1 || status == 2) {
                                        HistoryItem historyItem = new Gson().fromJson(item.toString(), HistoryItem.class);
                                        historyItems.add(historyItem);
                                    }
                                }

                                if (historyItems.size() > 0) {
                                    HistoryItem historyItem = historyItems.get(historyId);
                                    List<com.alfaplus.tracker.responses.history.item.ItemsItem> items = historyItem.getItems();

                                    PolylineOptions polylineOptions = new PolylineOptions().width(10).color(Color.parseColor("#2ea8d1"))
                                            .geodesic(true);
                                    for (com.alfaplus.tracker.responses.history.item.ItemsItem itemsItem : items) {
                                        polylineOptions.add(new LatLng(
                                                itemsItem.getLat(),
                                                itemsItem.getLng()
                                        ));
                                    }

                                    Bitmap bitmapA = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_a)).getBitmap();
                                    Bitmap bitmapB = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_b)).getBitmap();

                                    LatLng latLngA = new LatLng(items.get(0).getLat(), items.get(0).getLng());
                                    LatLng latLngB = new LatLng(items.get(items.size() - 1).getLat(), items.get(items.size() - 1).getLng());

                                    map.addMarker(new MarkerOptions()
                                            .icon(BitmapDescriptorFactory
                                                    .fromBitmap(Bitmap.createScaledBitmap(bitmapA, bitmapA.getWidth(), bitmapA.getHeight(), false)))
                                            .position(latLngA)
                                            .flat(false));

                                    map.addMarker(new MarkerOptions()
                                            .icon(BitmapDescriptorFactory
                                                    .fromBitmap(Bitmap.createScaledBitmap(bitmapB, bitmapB.getWidth(), bitmapB.getHeight(), false)))
                                            .position(latLngB)
                                            .flat(false));

                                    map.addPolyline(polylineOptions);
                                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(polylineOptions.getPoints().get(0), 17));

                                    textSpeed.setText(getString(R.string.text_speed) + historyItem.getAverageSpeed() + getString(R.string.text_kph));
                                    textSpeed.setVisibility(View.GONE);
                                    textDistance.setText(getString(R.string.text_distance) + historyItem.getDistance() + getString(R.string.text_kilometer));
                                    textTime.setText(getString(R.string.text_duration) + historyItem.getTime());
                                    cardDetail.setVisibility(View.VISIBLE);
                                }


                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Log.e(Params.TAG, "onResponse Code:" + response.code());
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

    }


    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }

        //INSERT CUSTOM CODE HERE
        new Handler().postDelayed(() -> {
            mapView.onCreate(savedInstanceState);
            mapView.onResume();
            mapView.getMapAsync(googleMap -> {
                map = googleMap;
                map.getUiSettings().setCompassEnabled(false);
                map.getUiSettings().setRotateGesturesEnabled(false);
                map.getUiSettings().setZoomControlsEnabled(false);
                map.getUiSettings().setMapToolbarEnabled(false);
                map.getUiSettings().setAllGesturesEnabled(true);

                getDevices();

            });

        }, 1000);


    }
}