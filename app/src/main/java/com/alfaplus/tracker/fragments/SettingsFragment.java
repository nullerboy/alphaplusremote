package com.alfaplus.tracker.fragments;

import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.activities.AdminsActivity;
import com.alfaplus.tracker.activities.AlertsActivity;
import com.alfaplus.tracker.activities.BaseActivity;
import com.alfaplus.tracker.activities.DevicesActivity;
import com.alfaplus.tracker.activities.EventsActivity;
import com.alfaplus.tracker.activities.LoginActivity;
import com.alfaplus.tracker.activities.MainActivity;
import com.alfaplus.tracker.activities.NoticeActivity;
import com.alfaplus.tracker.activities.SplashActivity;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.adapters.LangDialog;
import com.alfaplus.tracker.adapters.PwdDialog;
import com.alfaplus.tracker.adapters.SensDialog;
import com.alfaplus.tracker.adapters.ServerDialog;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint({"NewApi", "NonConstantResourceId", "UseSwitchCompatOrMaterialCode"})
public class SettingsFragment extends Fragment
        implements MaterialSpinner.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {

    Pref pref;
    SweetAlertDialog loading;

    @BindView(R.id.spinner_names)
    MaterialSpinner spinnerNames;

    public static boolean isActive = false;
    public static FloatingActionButton fabCall;
    public static Switch aSwitchSpy;
    public static Switch aSwitchAlert;
    public static Switch aSwitchBiometric;

    public boolean onPause = true;

    private NoticeActivity noticeActivity;

    List<DevicesItem> devicesItems = new ArrayList<>();
    List<String> deviceNames = new ArrayList<>();
    int selectedDevice = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);


        ButterKnife.bind(this, view);
        MainActivity.navigation.getMenu().getItem(1).setChecked(true);

        pref = new Pref(container.getContext());
        if (pref.getLanguage().equals("fa")) {
            view.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            view.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        spinnerNames.setOnItemSelectedListener(this);
        fabCall = view.findViewById(R.id.fab_call);
        aSwitchSpy = view.findViewById(R.id.switch_spy);
        aSwitchAlert = view.findViewById(R.id.switch_alert);
        aSwitchBiometric = view.findViewById(R.id.switch_fingerprint);
        aSwitchBiometric.setOnCheckedChangeListener(this);
        aSwitchBiometric.setChecked(pref.getUsesBiometric());

        aSwitchSpy.setOnCheckedChangeListener(this);
        aSwitchAlert.setOnCheckedChangeListener(this);

        fabCall.setOnClickListener(v -> {
            DevicesItem device = devicesItems.get(spinnerNames.getSelectedIndex());
            Intent callIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:" + device.getSimNumber()));
            startActivity(callIntent);
        });

        isActive = true;


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.err.println("SettingsFragment => super.onResume()");
        setOfflineDevices();
    }

    public void setOfflineDevices() {
        devicesItems = pref.getDevicesConfigs();
        if (devicesItems == null) {
            SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
            dialog.setCancelable(false);
            dialog.setTitleText(getString(R.string.oops));
            dialog.setContentText(getString(R.string.first_sync_desc));
            dialog.setConfirmButton(getString(R.string.ok), new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialog.cancel();
                    sync();
                }
            });
            dialog.show();

        } else {
            if (deviceNames.isEmpty())
                for (DevicesItem device : devicesItems)
                    deviceNames.add(device.getName());

            spinnerNames.setItems(deviceNames);
            spinnerNames.setSelectedIndex(selectedDevice);
            applyViews(devicesItems.get(selectedDevice));
        }

    }


    @OnClick({
            R.id.layout_status,
            R.id.layout_sens,
            R.id.layout_help_me,
            R.id.layout_pwd,
            R.id.layout_define_admin,
            R.id.layout_logout,
            R.id.layout_devices,
            R.id.layout_sync,
            R.id.layout_events,
            R.id.layout_alert,
            R.id.layout_language,
            R.id.layout_server
    })
    void click(View view) {

        if (devicesItems.size() == 0) {
            return;
        }

        if (pref.isDemoMode()) {
            BaseActivity.showDemoDialog(getActivity(), () -> {
                getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            });
            return;
        }

        int id = view.getId();
        DevicesItem device = devicesItems.get(spinnerNames.getSelectedIndex());
        String mobileNumber = device.getSimNumber();

        if (R.id.layout_status == id) {
            BaseActivity.sendSMS(getActivity(), mobileNumber, getString(R.string.cmd_3) + device.getPassword());
            Toast.makeText(getActivity(), getString(R.string.cmd_sent_succ), Toast.LENGTH_SHORT).show();
            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_help_me == id) {
            BaseActivity.sendSMS(getActivity(), mobileNumber, getString(R.string.cmd_8));
            Toast.makeText(getActivity(), getString(R.string.cmd_sent_succ), Toast.LENGTH_SHORT).show();
            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_sens == id) {
            SensDialog sensDialog = new SensDialog(getActivity());
            sensDialog.setListener(new SensDialog.SensDialogListener() {
                @Override
                public void listener(int sensitivity) {
                    sensitivity++;
                    if (sensitivity == 3) {
                        BaseActivity.sendSMS(getActivity(), mobileNumber, getString(R.string.cmd_4) + device.getPassword() + " " + (1));

                    } else if (sensitivity == 2) {
                        BaseActivity.sendSMS(getActivity(), mobileNumber, getString(R.string.cmd_4) + device.getPassword() + " " + (2));

                    } else if (sensitivity == 1) {
                        BaseActivity.sendSMS(getActivity(), mobileNumber, getString(R.string.cmd_4) + device.getPassword() + " " + (3));

                    }
                    Toast.makeText(getActivity(), getString(R.string.cmd_sent_succ), Toast.LENGTH_SHORT).show();
                }
            });
            sensDialog.show();
            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_pwd == id) {
            PwdDialog dialog = new PwdDialog(getActivity());
            dialog.setListener(new PwdDialog.PwdListener() {
                @Override
                public void listener(String newPassword) {
                    BaseActivity.sendSMS(getActivity(), mobileNumber, getString(R.string.cmd_9) + device.getPassword() + " " + (newPassword));
                    device.setNewPassword(newPassword);
                    pref.setDevicesConfig(devicesItems);
                    Toast.makeText(getActivity(), getString(R.string.cmd_sent_succ), Toast.LENGTH_SHORT).show();
                }
            });
            dialog.show();
            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_logout == id) {
            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.text_logout_warn)
                    .setPositiveButton(getString(R.string.yes), (dialog1, which) -> {
                        pref.clearPrefs();
                        startActivity(new Intent(getActivity(), SplashActivity.class));
                        getActivity().finish();
                        dialog1.cancel();
                    }).setNegativeButton(getString(R.string.no), (dialog12, which) -> dialog12.cancel()).create();
            dialog.show();
            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_define_admin == id) {

            if (device.getAdditionalNotes() == null) {
                SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
                dialog.setCancelable(false);
                dialog.setTitleText(getString(R.string.alert));
                dialog.setContentText(getString(R.string.text_define_admin_synce));
                dialog.setConfirmButton(getString(R.string.ok), new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        dialog.cancel();
                        sync();
                    }
                });
                dialog.show();
            } else {
                Intent intent = new Intent(getActivity(), AdminsActivity.class);
                intent.putExtra("deviceId", device.getId());
                intent.putExtra("deviceSim", device.getSimNumber());
                intent.putExtra("numbers", device.getAdditionalNotes());
                startActivity(intent);
            }


            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_devices == id) {

            getActivity().startActivity(new Intent(getActivity(), DevicesActivity.class));
            MediaPlayer.create(getActivity(), R.raw.pop).start();
        } else if (R.id.layout_sync == id) {
            sync();

        } else if (R.id.layout_events == id) {
            startActivity(new Intent(getActivity(), EventsActivity.class));

        } else if (R.id.layout_alert == id) {
            startActivity(new Intent(getActivity(), AlertsActivity.class));

        } else if (R.id.layout_language == id) {
            LangDialog langDialog = new LangDialog(getActivity());
            langDialog.setOnLanguageSelect(language -> {
                langDialog.cancel();
                if (!language.equals(pref.getLanguage())) {
                    pref.setLanguage(language);
                    getActivity().startActivity(new Intent(getActivity(), SplashActivity.class));
                    getActivity().finish();
                }
            });
            langDialog.show();
        } else if (R.id.layout_server == id) {
            ServerDialog serverDialog = new ServerDialog(getActivity());
            serverDialog.show();
        }

    }

    @Override
    public void onDestroy() {
        isActive = false;
        super.onDestroy();
    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

        selectedDevice = position;
        DevicesItem device = devicesItems.get(spinnerNames.getSelectedIndex());
        System.err.println(device.toString());
        if (device.getSpyState()) {
            fabCall.show();
        } else {
            fabCall.hide();
        }
        applyViews(device);

    }

    public void applyViews(DevicesItem device) {
        aSwitchAlert.setOnCheckedChangeListener(null);
        aSwitchAlert.setChecked(device.getAlertState());
        aSwitchAlert.setOnCheckedChangeListener(this);

        aSwitchSpy.setOnCheckedChangeListener(null);
        aSwitchSpy.setChecked(device.getSpyState());
        aSwitchSpy.setOnCheckedChangeListener(this);

        if (device.getSpyState())
            fabCall.show();
        else fabCall.hide();


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (devicesItems.size() == 0) {
            return;
        }

        int id = buttonView.getId();
        DevicesItem device = devicesItems.get(spinnerNames.getSelectedIndex());

        if (id == R.id.switch_alert) {
            MediaPlayer.create(getActivity(), R.raw.pop).start();
            device.setAlertState(!device.getAlertState());
            pref.setDevicesConfig(devicesItems);
        } else if (id == R.id.switch_spy) {
            MediaPlayer.create(getActivity(), R.raw.pop).start();
            Toast.makeText(getActivity(), device.getName(), Toast.LENGTH_SHORT).show();

            if (isChecked) {
                fabCall.show();
            } else {
                fabCall.hide();
            }

            if (pref.isDemoMode()) {
                BaseActivity.showDemoDialog(getActivity(), new Runnable() {
                    @Override
                    public void run() {
                        getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }
                });
                return;
            }

            BaseActivity.sendSMS(getActivity(), device.getSimNumber(),
                    isChecked ? getString(R.string.cmd_7) + device.getPassword() :
                            getString(R.string.cmd_10) + device.getPassword());
            Toast.makeText(getActivity(), getString(R.string.cmd_sent_succ), Toast.LENGTH_SHORT).show();
        } else if (id == R.id.switch_fingerprint) {

            checkBiometric(getActivity());

        }

    }

    public void checkBiometric(Context context) {
        BiometricManager biometricManager = BiometricManager.from(context);
        if (biometricManager.canAuthenticate() == BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE) {
            Toast.makeText(context, R.string.no_biometric_sensor, Toast.LENGTH_SHORT).show();
        } else if (biometricManager.canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS) {
            Executor executor = ContextCompat.getMainExecutor(getActivity());
            BiometricPrompt prompt = new BiometricPrompt(getActivity(), executor, new BiometricPrompt.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                    super.onAuthenticationError(errorCode, errString);
                    Toast.makeText(context, R.string.biometric_read_error, Toast.LENGTH_SHORT).show();
                    aSwitchBiometric.setOnCheckedChangeListener(null);
                    aSwitchBiometric.setChecked(pref.getUsesBiometric());
                    aSwitchBiometric.setOnCheckedChangeListener(SettingsFragment.this);
                }

                @Override
                public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                    super.onAuthenticationSucceeded(result);
                    Toast.makeText(context, getString(R.string.operation_succ), Toast.LENGTH_SHORT).show();
                    pref.setUsesBiometric(!pref.getUsesBiometric());
                }

                @Override
                public void onAuthenticationFailed() {
                    super.onAuthenticationFailed();
                    Toast.makeText(context, R.string.biometric_read_error, Toast.LENGTH_SHORT).show();
                    aSwitchBiometric.setOnCheckedChangeListener(null);
                    aSwitchBiometric.setChecked(pref.getUsesBiometric());
                    aSwitchBiometric.setOnCheckedChangeListener(SettingsFragment.this);
                }
            });
            BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle(getString(R.string.app_name))
                    .setSubtitle("Touch Sensor")
                    .setNegativeButtonText("Cancel")
                    .build();
            prompt.authenticate(promptInfo);
        }
    }

    public void sync() {
        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText(getString(R.string.text_syncing));
        loading.show();
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                List<ItemsItem> devices = devicesResponse.getItems();

                                deviceNames.clear();
                                devicesItems.clear();

                                for (ItemsItem device : devices) {
                                    deviceNames.add(device.getName());
                                    DevicesItem devicesItem = new DevicesItem();
                                    Log.e(TAG, "devicesItem: " + device.getName());

                                    devicesItem.setId(device.getId());
                                    devicesItem.setName(device.getName());
                                    devicesItem.setSimNumber(device.getDeviceData().getSimNumber());
                                    devicesItem.setAlertState(true);
                                    devicesItem.setPowerState(true);
                                    devicesItem.setSpyState(false);
                                    devicesItem.setPassword("123456");
                                    devicesItem.setNewPassword("");
                                    devicesItem.setAdditionalNotes(device.getDeviceData().getAdditionalNotes());
                                    devicesItems.add(devicesItem);
                                }
//                                setDevices(devices);

                                pref.setDevicesConfig(devicesItems);
                                loading.cancel();
                                loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                loading.setTitleText(getString(R.string.nice));
                                loading.setContentText(getString(R.string.text_sync_succ));
                                loading.setConfirmButton(getString(R.string.ok), new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        loading.cancel();
                                        startActivity(new Intent(getActivity(), MainActivity.class));
                                        getActivity().finish();
                                    }
                                });
                                loading.show();

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.e(TAG, "onResponse Code:" + response.code());

//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("onFailure", "onFailure: " + t.getLocalizedMessage());
                        loading.cancel();
                        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                        loading.setTitleText(getString(R.string.oops));
                        loading.setCancelable(false);
                        loading.setContentText(getString(R.string.no_internet));
                        loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> sync());
                        loading.show();
                    }
                });
    }

}