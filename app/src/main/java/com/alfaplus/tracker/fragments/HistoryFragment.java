package com.alfaplus.tracker.fragments;

import static com.alfaplus.tracker.Params.HISTORY_STOPS_SECONDS;
import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.activities.BaseActivity;
import com.alfaplus.tracker.activities.CourseActivity;
import com.alfaplus.tracker.adapters.HistoryAdapter;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.alfaplus.tracker.responses.history.item.HistoryItem;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint({"NonConstantResourceId", "SetTextI18n"})
public class HistoryFragment extends Fragment {

    @BindView(R.id.recycler_history)
    RecyclerView recycler;

    @BindView(R.id.spinner_devices)
    MaterialSpinner spinnerDevices;

    @BindView(R.id.spinner_dates)
    MaterialSpinner spinnerDates;

    @BindView(R.id.button_select_start_date)
    Button buttonSelectStartDate;

    @BindView(R.id.button_select_start_time)
    Button buttonSelectStartTime;

    @BindView(R.id.button_select_end_date)
    Button buttonSelectEndDate;

    @BindView(R.id.button_select_end_time)
    Button buttonSelectEndTime;

    @BindView(R.id.progress_loading)
    ProgressBar loading;

    Pref pref;

    //    List<ItemsItem> items;
//    List<String> devicesName;
    int selectedDevice = 0;
    //    int selectedDate = 0;
    List<String> devicesName = new ArrayList<>();
    List<ItemsItem> devices = new ArrayList<>();

    long timestampStart = 0;
    long timestampEnd = 0;
    String dateStart = "";
    String dateEnd = "";
    String timeStart = "00:00";
    String timeEnd = "23:59";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        view.setLayoutDirection(new Pref(getActivity()).getLanguage().equals("fa") ?
                View.LAYOUT_DIRECTION_RTL :
                View.LAYOUT_DIRECTION_LTR);

        ButterKnife.bind(this, view);
        pref = new Pref(container.getContext());

        recycler.setLayoutManager(new LinearLayoutManager(container.getContext()));


        if (pref.getLanguage().equals("fa")) {
            PersianCalendar persianCalendarStart = new PersianCalendar();
            persianCalendarStart.set(PersianCalendar.HOUR_OF_DAY, 0);
            persianCalendarStart.set(PersianCalendar.MINUTE, 0);

            PersianCalendar persianCalendarEnd = new PersianCalendar();
            persianCalendarEnd.set(PersianCalendar.HOUR_OF_DAY, 23);
            persianCalendarEnd.set(PersianCalendar.MINUTE, 59);

            dateStart = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(persianCalendarStart.getTime());
            dateEnd = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(persianCalendarStart.getTime());

            buttonSelectStartDate.setText("🗓" + persianCalendarStart.getPersianYear() + "-" +
                    (persianCalendarStart.getPersianMonth() + 1) + "-" +
                    persianCalendarStart.getPersianDay());
            buttonSelectEndDate.setText("🗓" + persianCalendarEnd.getPersianYear() + "-" +
                    (persianCalendarEnd.getPersianMonth() + 1) + "-" +
                    persianCalendarEnd.getPersianDay());

            timestampStart = persianCalendarStart.getTimeInMillis();
            timestampEnd = persianCalendarEnd.getTimeInMillis();

        } else {
            Calendar calendar = Calendar.getInstance();
            buttonSelectStartDate.setText("🗓" + calendar.get(Calendar.YEAR) + "-" +
                    (calendar.get(Calendar.MONTH) + 1) + "-" +
                    calendar.get(Calendar.DAY_OF_MONTH));
            buttonSelectEndDate.setText("🗓" + calendar.get(Calendar.YEAR) + "-" +
                    (calendar.get(Calendar.MONTH) + 1) + "-" +
                    calendar.get(Calendar.DAY_OF_MONTH));

            dateStart = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
            dateEnd = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());

            timestampStart = calendar.getTimeInMillis();
            timestampEnd = calendar.getTimeInMillis();
        }


        buttonSelectStartTime.setText("🕒" + timeStart);
        buttonSelectEndTime.setText("🕒" + timeEnd);


//        items = new ArrayList<>();
//        devicesName = new ArrayList<>();
//        spinnerDates.setItems("امروز", "دیروز", "۷ روز قبل");
        spinnerDates.setItems(
                getString(R.string.text_select_time),
                getString(R.string.text_last_hour),
                getString(R.string.text_today),
                getString(R.string.text_yesterday),
                getString(R.string.text_last_two_day),
                getString(R.string.text_current_week)
        );
        spinnerDates.setOnItemSelectedListener((view12, position, id, item) -> {
//            selectedDate = position;
            Calendar calendar = Calendar.getInstance();

            if (position == 1) {
                String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String endTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
                calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
                String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String startTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());

                showCourse(devices.get(selectedDevice).getId(),
                        startDate, endDate,
                        startTime, endTime,
                        devicesName.get(selectedDevice));

            } else if (position == 2) {
                String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String endTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String startTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());

                showCourse(devices.get(selectedDevice).getId(),
                        startDate, endDate,
                        "00:00", "23:59",
                        devicesName.get(selectedDevice));

            } else if (position == 3) {
                calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String endTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String startTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());

                showCourse(devices.get(selectedDevice).getId(),
                        startDate, endDate,
                        "00:00", "23:59",
                        devicesName.get(selectedDevice));

            } else if (position == 4) {
                calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 2);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String startTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
                calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                String endTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());

                showCourse(devices.get(selectedDevice).getId(),
                        startDate, endDate,
                        "00:00", "23:59",
                        devicesName.get(selectedDevice));

            } else if (position == 5) {
                String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 6);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                showCourse(devices.get(selectedDevice).getId(),
                        startDate, endDate,
                        "00:00", "23:59",
                        devicesName.get(selectedDevice));
//                calendar.set(Calendar.HOUR_OF_DAY, 23);
//                calendar.set(Calendar.MINUTE, 59);
//                String endDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
//                String endTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
//                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
//                calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
//                calendar.set(Calendar.HOUR_OF_DAY, 0);
//                calendar.set(Calendar.MINUTE, 0);
//                String startDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
//                String startTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(calendar.getTime());
//
//                showCourse(devices.get(selectedDevice).getId(),
//                        startDate, endDate,
//                        "00:00", "23:59",
//                        devicesName.get(selectedDevice));

            }

        });
        spinnerDevices.setOnItemSelectedListener((view1, position, id, item) -> {
                    selectedDevice = position;
                }
        );

        Log.e(TAG, "onDateSet: Start -> " + dateStart);
        Log.e(TAG, "onDateSet: End -> " + dateEnd);
        getDevices();

        return view;
    }

    public void getHistory(int deviceId, String startDate, String toDate) {
        Log.e(Params.TAG, "getHistory: Date=" + spinnerDates.getSelectedIndex() + " , Device=" + selectedDevice);
        loading.setVisibility(View.VISIBLE);
        recycler.setVisibility(View.GONE);
        BaseActivity.api.getHistory(BaseActivity.pref.getUserApiHash(),
                deviceId,
                startDate, "00:00",
                toDate, "23:59",
                HISTORY_STOPS_SECONDS)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.e(Params.TAG, "onResponse Code:" + call.request().url());
                        if (response.code() == 200) {
                            try {
                                List<HistoryItem> historyItems = new ArrayList<>();
                                JSONObject resObject = new JSONObject(response.body().string());
                                JSONArray itemsArray = resObject.getJSONArray("items");

                                for (int i = 0; i < itemsArray.length(); i++) {
                                    JSONObject item = itemsArray.getJSONObject(i);
                                    int status = item.getInt("status");
                                    if (status == 1 || status == 2) {
                                        HistoryItem historyItem = new Gson().fromJson(item.toString(), HistoryItem.class);
                                        if (historyItem.getDistance() > 0.09)
                                            historyItems.add(historyItem);
                                    }
                                }
                                HistoryAdapter adapter = new HistoryAdapter(historyItems,pref.getLanguage());
                                recycler.setAdapter(adapter);
                                adapter.setOnClicked(i -> {

                                    Log.e(TAG, "getUserVisibleHint1: selectedHistory=" + i + " , selectedDeviceId=" + devices.get(selectedDevice).getId() + " " + startDate + " -> " + toDate);

//                                        CourseFragment.selectedDeviceId = selectedDevice;
//                                        CourseFragment.selectedHistory = i;
//                                        CourseFragment.startDate = startDate;
//                                        CourseFragment.endDate = toDate;
//                                        TabbedFragment.viewPager.setCurrentItem(2);

                                    Intent intent = new Intent(getActivity(), CourseActivity.class);
                                    intent.putExtra("history", i);
                                    intent.putExtra("device", deviceId);
                                    intent.putExtra("start", startDate);
                                    intent.putExtra("end", toDate);
                                    intent.putExtra("name", devicesName.get(selectedDevice));
                                    intent.putExtra("show", historyItems.get(i).getShow());
                                    intent.putExtra("left", historyItems.get(i).getLeft());
                                    getActivity().startActivity(intent);


                                });
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                Toast.makeText(getActivity(), "IOException", Toast.LENGTH_LONG).show();
                            }
                            loading.setVisibility(View.GONE);
                            recycler.setVisibility(View.VISIBLE);
                        } else {
                            Log.e(Params.TAG, "onResponse Code:" + response.code());

                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getActivity(), "onFailure", Toast.LENGTH_LONG).show();
                    }
                });

    }

    @SuppressLint("SimpleDateFormat")
    public void getDevices() {
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            devicesName.clear();
                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                devices = devicesResponse.getItems();
                                for (ItemsItem device : devices) {
                                    devicesName.add(device.getName());
                                }
                                spinnerDevices.setItems(devicesName);

                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.HOUR_OF_DAY, 0);
                                calendar.set(Calendar.MINUTE, 0);
                                calendar.set(Calendar.SECOND, 0);
                                String end = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());
                                String start = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(calendar.getTime());

//                                getHistory(TabbedFragment.items.get(selectedDevice).getId(), start, start);

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        getDevices();
                    }
                });
    }

    @OnClick({R.id.button_select_start_date,
            R.id.button_select_end_date,
            R.id.button_select_start_time,
            R.id.button_select_end_time,
            R.id.button_submit})
    void clicked(View view) {
        int id = view.getId();

        if (R.id.button_select_start_date == id) {

            if (pref.getLanguage().equals("fa")) {
                PersianCalendar persianCalendar = new PersianCalendar();
                DatePickerDialog dialog = DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
                    persianCalendar.setPersianDate(year, (monthOfYear), dayOfMonth);
                    timestampStart = persianCalendar.getTimeInMillis();
                    dateStart = persianCalendar.get(PersianCalendar.YEAR) + "-" +
                            (persianCalendar.get(PersianCalendar.MONTH) + 1) + "-" +
                            persianCalendar.get(PersianCalendar.DAY_OF_MONTH);
                    buttonSelectStartDate.setText(persianCalendar.getPersianYear() + "-" +
                            (persianCalendar.getPersianMonth() + 1) + "-" +
                            persianCalendar.getPersianDay());
                    Log.e(TAG, "onDateSet: End -> " + dateEnd);
                }, persianCalendar.getPersianYear(), persianCalendar.getPersianMonth(), persianCalendar.getPersianDay());

                dialog.show(getActivity().getFragmentManager(), "DatePickerStart");

            } else {
                MaterialDatePicker datePicker = MaterialDatePicker.Builder
                        .datePicker()
                        .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                        .build();
                datePicker.addOnPositiveButtonClickListener(selection -> {
                    Calendar calendar = Calendar.getInstance();
                    long timestamp = Long.parseLong(selection.toString());
                    calendar.setTimeInMillis(timestamp);
                    timestampStart = timestamp;
                    dateStart = calendar.get(Calendar.YEAR) + "-" +
                            ((calendar.get(Calendar.MONTH) + 1) > 9 ? (calendar.get(Calendar.MONTH) + 1) : "0" + (calendar.get(Calendar.MONTH) + 1)) + "-" +
                            (calendar.get(Calendar.DAY_OF_MONTH) > 9 ? calendar.get(Calendar.DAY_OF_MONTH) : "0" + calendar.get(Calendar.DAY_OF_MONTH));
                    Log.e(TAG, "onDateSet: Start -> " + dateStart);
                    buttonSelectStartDate.setText(dateStart);

                });
                datePicker.show(getActivity().getSupportFragmentManager(), "DatePickerStart");

            }


        } else if (R.id.button_select_end_date == id) {

            if (pref.getLanguage().equals("fa")) {
                PersianCalendar persianCalendar = new PersianCalendar();
                DatePickerDialog dialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        persianCalendar.setPersianDate(year, (monthOfYear), dayOfMonth);
                        timestampEnd = persianCalendar.getTimeInMillis();
                        dateEnd = persianCalendar.get(PersianCalendar.YEAR) + "-" +
                                (persianCalendar.get(PersianCalendar.MONTH) + 1) + "-" +
                                persianCalendar.get(PersianCalendar.DAY_OF_MONTH);

                        buttonSelectEndDate.setText(persianCalendar.getPersianYear() + "-" +
                                (persianCalendar.getPersianMonth() + 1) + "-" +
                                persianCalendar.getPersianDay());

                        Log.e(TAG, "onDateSet: End -> " + dateEnd);
                    }
                }, persianCalendar.getPersianYear(), persianCalendar.getPersianMonth(), persianCalendar.getPersianDay());
                dialog.show(getActivity().getFragmentManager(), "DatePickerEnd");

            } else {
                MaterialDatePicker datePicker = MaterialDatePicker.Builder
                        .datePicker()
                        .setInputMode(MaterialDatePicker.INPUT_MODE_CALENDAR)
                        .build();
                datePicker.addOnPositiveButtonClickListener(selection -> {
                    Calendar calendar = Calendar.getInstance();
                    long timestamp = Long.parseLong(selection.toString());
                    calendar.setTimeInMillis(timestamp);
                    timestampEnd = timestamp;
                    dateEnd = calendar.get(Calendar.YEAR) + "-" +
                            ((calendar.get(Calendar.MONTH) + 1) > 9 ? (calendar.get(Calendar.MONTH) + 1) : "0" + (calendar.get(Calendar.MONTH) + 1)) + "-" +
                            (calendar.get(Calendar.DAY_OF_MONTH) > 9 ? calendar.get(Calendar.DAY_OF_MONTH) : "0" + calendar.get(Calendar.DAY_OF_MONTH));
                    Log.e(TAG, "onDateSet: End -> " + dateEnd);
                    buttonSelectEndDate.setText(dateEnd);

                });
                datePicker.show(getActivity().getSupportFragmentManager(), "DatePickerStart");

            }


        } else if (R.id.button_submit == id) {
            if (timestampStart > timestampEnd) {
//                Toast.makeText(getActivity(), "بازه زمانی به درستی انتخاب نشده است", Toast.LENGTH_LONG).show();
            } else if (devices.size() == 0) {
//                Snackbar.make(view, "درحال دریافت داده! شکیبا باشید.", Snackbar.LENGTH_SHORT).show();
            } else {
//                Intent intent = new Intent(getActivity(), CourseActivity.class);
//                intent.putExtra("device", devices.get(selectedDevice).getId());
//                intent.putExtra("startDate", dateStart);
//                intent.putExtra("endDate", dateEnd);
//                intent.putExtra("startTime", timeStart);
//                intent.putExtra("endTime", timeEnd);
//                intent.putExtra("name", devicesName.get(selectedDevice));
//                getActivity().startActivity(intent);

                showCourse(devices.get(selectedDevice).getId(),
                        dateStart,
                        dateEnd,
                        timeStart,
                        timeEnd,
                        devicesName.get(selectedDevice));

//                Log.e(TAG, "time: "+time.getHours()+":"+time.getMinutes() );
//                Log.e(TAG, "start: " + new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US).format(time));
//                Log.e(TAG, "end: " + new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.ENGLISH).format(timestampEnd));

            }

        } else if (R.id.button_select_start_time == id) {
            if (pref.getLanguage().equals("fa")) {
                PersianCalendar persianCalendar = new PersianCalendar();
                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance((view1, hourOfDay, minute) -> {
                    timeStart = (hourOfDay > 9 ? "" + hourOfDay : "0" + hourOfDay) + ":" + (minute > 9 ? "" + minute : "0" + minute);
                    timestampStart = persianCalendar.getTimeInMillis();
                    buttonSelectStartTime.setText("🕒" + timeStart);
                }, persianCalendar.get(PersianCalendar.HOUR_OF_DAY), persianCalendar.get(PersianCalendar.MINUTE), true);
                timePickerDialog.show(getActivity().getFragmentManager(), "TimePickerDialog");

            } else {
                MaterialTimePicker materialTimePicker = new MaterialTimePicker.Builder()
                        .setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                        .setTimeFormat(TimeFormat.CLOCK_24H)
                        .build();
                materialTimePicker.addOnPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timeStart = (materialTimePicker.getHour() > 9 ? materialTimePicker.getHour() + "" : "0" + materialTimePicker.getHour())+":"+
                                (materialTimePicker.getMinute() > 9 ? materialTimePicker.getMinute() + "" : "0" + materialTimePicker.getMinute());
                        buttonSelectStartTime.setText("🕒" + timeStart);
                    }
                });
                materialTimePicker.show(getActivity().getSupportFragmentManager(), "MaterialTimePicker");

            }


        } else if (R.id.button_select_end_time == id) {
            if (pref.getLanguage().equals("fa")){
                PersianCalendar persianCalendar = new PersianCalendar();
                TimePickerDialog timePickerDialog = TimePickerDialog.newInstance((view1, hourOfDay, minute) -> {
                    timeEnd = (hourOfDay > 9 ? "" + hourOfDay : "0" + hourOfDay) + ":" + (minute > 9 ? "" + minute : "0" + minute);
                    timestampEnd = persianCalendar.getTimeInMillis();
                    buttonSelectEndTime.setText("🕒" + timeEnd);
                }, persianCalendar.get(PersianCalendar.HOUR_OF_DAY), persianCalendar.get(PersianCalendar.MINUTE), true);
                timePickerDialog.show(getActivity().getFragmentManager(), "TimePickerDialog");

            }else {
                MaterialTimePicker materialTimePicker = new MaterialTimePicker.Builder()
                        .setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                        .setTimeFormat(TimeFormat.CLOCK_24H)
                        .build();
                materialTimePicker.addOnPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        timeEnd = (materialTimePicker.getHour() > 9 ? materialTimePicker.getHour() + "" : "0" + materialTimePicker.getHour())+":"+
                                (materialTimePicker.getMinute() > 9 ? materialTimePicker.getMinute() + "" : "0" + materialTimePicker.getMinute());
                        buttonSelectEndTime.setText("🕒" + timeEnd);
                    }
                });
                materialTimePicker.show(getActivity().getSupportFragmentManager(), "MaterialTimePicker");

            }


        }

    }

    public void showCourse(int deviceId, String dateStart, String dateEnd, String timeStart, String timeEnd, String devicesName) {
        Intent intent = new Intent(getActivity(), CourseActivity.class);
        intent.putExtra("device", deviceId);
        intent.putExtra("startDate", dateStart);
        intent.putExtra("endDate", dateEnd);
        intent.putExtra("startTime", timeStart);
        intent.putExtra("endTime", timeEnd);
        intent.putExtra("name", devicesName);
        getActivity().startActivity(intent);
        spinnerDates.setSelectedIndex(0);
    }

}