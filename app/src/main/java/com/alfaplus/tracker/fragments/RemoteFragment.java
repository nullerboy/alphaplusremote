package com.alfaplus.tracker.fragments;

import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.SMSListener;
import com.alfaplus.tracker.activities.BaseActivity;
import com.alfaplus.tracker.activities.LoginActivity;
import com.alfaplus.tracker.activities.MainActivity;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint({"NonConstantResourceId", "UseSwitchCompatOrMaterialCode"})
public class RemoteFragment extends Fragment implements MaterialSpinner.OnItemSelectedListener {

    @BindView(R.id.image_car)
    ImageView imageCar;

    @BindView(R.id.switch_gprd)
    Switch aSwitchGPRS;

    public static ImageView imageRemote;

    @BindView(R.id.spinner_names)
    MaterialSpinner spinnerNames;

    Pref pref;
    public static boolean isActive = false;

    public SMSListener smsListener;

    List<DevicesItem> devicesItems = new ArrayList<>();
    List<String> deviceNames = new ArrayList<>();
    SweetAlertDialog loading;

    int selectedDevice = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_remote, container, false);

        ButterKnife.bind(this, view);
        MainActivity.navigation.getMenu().getItem(0).setChecked(true);
        pref = new Pref(container.getContext());
        imageRemote = view.findViewById(R.id.image_remote);

        aSwitchGPRS.setChecked(pref.getGPRSState());
        aSwitchGPRS.setOnCheckedChangeListener((buttonView, isChecked) -> {
            pref.setGPRSState(isChecked);
        });

        isActive = true;
        spinnerNames.setOnItemSelectedListener(this);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setOfflineDevices();
    }

    @OnClick({R.id.button_unlock,
            R.id.button_lock,
            R.id.button_power})
    void OnClick(View view) {

        int id = view.getId();

        if (R.id.button_unlock == id) {
            remoteButton(1);
        } else if (R.id.button_lock == id) {
            remoteButton(2);
        } else if (R.id.button_power == id) {
            remoteButton(0);
        }

    }

    @SuppressLint("NewApi")
    public void remoteButton(int btnId) {

        if (devicesItems.size() == 0) {
            return;
        }

        if (pref.isDemoMode()) {
            BaseActivity.showDemoDialog(getActivity(), new Runnable() {
                @Override
                public void run() {
                    getActivity().startActivity(new Intent(getActivity(), LoginActivity.class));
                    getActivity().finish();
                }
            });
            return;
        }

        DevicesItem device = devicesItems.get(spinnerNames.getSelectedIndex());
        String mobile = device.getSimNumber();
        boolean gprs = aSwitchGPRS.isChecked();


        if (btnId == 1) {
            MediaPlayer.create(getActivity(), R.raw.remote_unlock).start();
            if (gprs) {
                sendCommand(device.getId(), "alarmDisarm", device.getPowerState());
            } else {
                BaseActivity.sendSMS(getActivity(), mobile, getString(R.string.cmd_0) + device.getPassword());
            }
            Toast.makeText(getActivity(), getString(R.string.alarms_deactivate), Toast.LENGTH_SHORT).show();
        } else if (btnId == 2) {
            MediaPlayer.create(getActivity(), R.raw.remote_lock).start();
            if (gprs) {
                sendCommand(device.getId(), "alarmArm", device.getPowerState());
            } else {
                BaseActivity.sendSMS(getActivity(), mobile, getString(R.string.cmd_lock) + device.getPassword());
            }
            Toast.makeText(getActivity(), getString(R.string.alarms_activate), Toast.LENGTH_SHORT).show();
        } else {
            if (device.getPowerState()) {
                AlertDialog dialog = new AlertDialog.Builder(getActivity())
                        .setMessage(getString(R.string.text_car_acc_alert))
                        .setPositiveButton(getString(R.string.yes), (dialog12, which) -> {
                            if (gprs) {
                                sendCommand(device.getId(), "engineStop", false);
                            } else {
                                BaseActivity.sendSMS(getActivity(), mobile,
                                        device.getPowerState() ?
                                                getString(R.string.cmd_1) + device.getPassword() :
                                                getString(R.string.cmd_2) + device.getPassword());
                            }
                            MediaPlayer.create(getActivity(), R.raw.car_off).start();
                        }).setNegativeButton(getString(R.string.no), (dialog1, which) -> dialog1.cancel()).create();
                dialog.show();

            } else {
                MediaPlayer.create(getActivity(), R.raw.pop).start();
                if (gprs) {
                    sendCommand(device.getId(), "engineResume", true);
                } else {
                    BaseActivity.sendSMS(getActivity(), mobile,
                            device.getPowerState() ? getString(R.string.cmd_1) + device.getPassword() :
                                    getString(R.string.cmd_2) + device.getPassword());
                }
            }
        }
        final int[] i = {0};
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageCar.setImageResource(i[0] % 2 == 0 ? R.drawable.car_off : R.drawable.car_on);
                        i[0]++;

                        if (i[0] == 5) timer.cancel();
                    }
                });


            }
        }, 0, 200);
    }

    @Override
    public void onDestroy() {
        isActive = false;
        super.onDestroy();
    }

    public void sync() {
        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText(getString(R.string.text_syncing));
        loading.show();
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                List<ItemsItem> devices = devicesResponse.getItems();

                                for (ItemsItem device : devices) {
                                    deviceNames.add(device.getName());
                                    DevicesItem devicesItem = new DevicesItem();
                                    Log.e(TAG, "devicesItem: " + device.getName());

                                    devicesItem.setId(device.getId());
                                    devicesItem.setName(device.getName());
                                    devicesItem.setSimNumber(device.getDeviceData().getSimNumber());
                                    devicesItem.setAlertState(true);
                                    devicesItem.setPowerState(true);
                                    devicesItem.setSpyState(false);
                                    devicesItem.setPassword("123456");
                                    devicesItem.setNewPassword("");
                                    devicesItem.setAdditionalNotes(device.getDeviceData().getAdditionalNotes());
                                    devicesItems.add(devicesItem);
                                }

//                                setDevices(devices);
                                pref.setDevicesConfig(devicesItems);
                                loading.cancel();
                                loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                loading.setTitleText(getString(R.string.nice));
                                loading.setContentText(getString(R.string.text_sync_succ));
                                loading.setConfirmButton("تایید", new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        loading.cancel();
                                        startActivity(new Intent(getActivity(), MainActivity.class));
                                        getActivity().finish();
                                    }
                                });
                                loading.show();

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.e(TAG, "onResponse Code:" + response.code());

//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("onFailure", "onFailure: " + t.getLocalizedMessage());
                        loading.cancel();
                        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                        loading.setTitleText(getString(R.string.oops));
                        loading.setCancelable(false);
                        loading.setContentText(getString(R.string.no_internet));
                        loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> sync());
                        loading.show();
                    }
                });
    }


    public void setOfflineDevices() {
        devicesItems = pref.getDevicesConfigs();
        if (devicesItems == null) {
            SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
            dialog.setCancelable(false);
            dialog.setTitleText(getString(R.string.warning));
            dialog.setContentText(getString(R.string.first_sync_desc));
            dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                dialog.cancel();
                sync();
            });
            dialog.show();
        } else {
            if (deviceNames.isEmpty())
                for (DevicesItem device : devicesItems)
                    deviceNames.add(device.getName());
            spinnerNames.setItems(deviceNames);
            spinnerNames.setSelectedIndex(selectedDevice);
            imageRemote.setImageResource(devicesItems.get(selectedDevice).getPowerState() ? R.drawable.remote_on : R.drawable.remote_off);

        }

    }

    @Override
    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
        selectedDevice = position;
        imageRemote.setImageResource(devicesItems.get(position).getPowerState() ? R.drawable.remote_on : R.drawable.remote_off);
    }

    public void sendCommand(int deviceId, String command, boolean powerState) {
        SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        dialog.setCancelable(false);
        dialog.setContentText(getString(R.string.sending_cmd));
        dialog.show();
        BaseActivity.api.sendCommand(pref.getUserApiHash(),
                deviceId, command, command).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    dialog.cancel();
                    SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                    dialog.setContentText(getString(R.string.cmd_sent_succ));
                    dialog.setConfirmText(getString(R.string.ok));
                    dialog.show();
                    DevicesItem device = devicesItems.get(spinnerNames.getSelectedIndex());
                    device.setPowerState(powerState);
                    pref.setDevicesConfig(devicesItems);
                    imageRemote.setImageResource(powerState ? R.drawable.remote_on : R.drawable.remote_off);
                } else {
                    dialog.cancel();
                    SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
                    dialog.setContentText(getString(R.string.cmd_send_failed));
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.cancel();
                SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                dialog.setContentText(getActivity().getString(R.string.no_internet));
                dialog.setConfirmButton(getActivity().getString(R.string.close), sweetAlertDialog -> dialog.cancel());
                dialog.show();
            }
        });
    }
}