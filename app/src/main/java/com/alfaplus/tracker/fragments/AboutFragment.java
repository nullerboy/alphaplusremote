package com.alfaplus.tracker.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alfaplus.tracker.BuildConfig;
import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.activities.MainActivity;
import com.alfaplus.tracker.R;

import butterknife.ButterKnife;

public class AboutFragment extends Fragment implements View.OnClickListener {

    TextView textSite, textWhatsapp, textPhone, textTelegram, textTelegramSupport, textVersion;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        ButterKnife.bind(container, view);
        view.setLayoutDirection(new Pref(getActivity()).getLanguage().equals("fa")?
                View.LAYOUT_DIRECTION_RTL:
                View.LAYOUT_DIRECTION_LTR);
        MainActivity.navigation.getMenu().getItem(3).setChecked(true);

        textSite = view.findViewById(R.id.text_site);
        textWhatsapp = view.findViewById(R.id.text_whatsapp);
        textPhone = view.findViewById(R.id.text_phone);
        textTelegram = view.findViewById(R.id.text_telegram);
        textTelegramSupport = view.findViewById(R.id.text_support_telegram);
        textVersion = view.findViewById(R.id.text_version);
        textVersion.setText(getString(R.string.version)+" " + BuildConfig.VERSION_NAME);


        textSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://alfamarket.ir")));
            }
        });

        textWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/989921650225")));

            }
        });

        textPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:02128428738")));

            }
        });

        textTelegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/gpsalfa")));
            }
        });

        textTelegramSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/alfagps")));
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (R.id.text_site == id) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("www.alfamarket.ir")));
        } else if (R.id.text_phone == id) {
            startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:09921650225")));
        }
    }
}