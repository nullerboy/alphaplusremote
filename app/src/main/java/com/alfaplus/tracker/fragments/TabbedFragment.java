package com.alfaplus.tracker.fragments;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.activities.MainActivity;
import com.google.android.material.tabs.TabLayout;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.MyTabAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabbedFragment extends Fragment {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static List<String> devicesName = new ArrayList<>();
    Pref pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tabbed, container, false);
        MainActivity.navigation.getMenu().getItem(2).setChecked(true);
        pref = new Pref(getActivity());
        view.setLayoutDirection(pref.getLanguage().equals("fa")?
                View.LAYOUT_DIRECTION_RTL:
                View.LAYOUT_DIRECTION_LTR);


        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.view_pager);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.nav_location)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.text_courses)));
//        tabLayout.addTab(tabLayout.newTab().setText("لیست مسیرها"));
//        tabLayout.addTab(tabLayout.newTab().setText("مسیر"));

        viewPager.setAdapter(new MyTabAdapter(getChildFragmentManager(), getActivity(), 2));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        changeTabsFont();
        return view;
    }

    private void changeTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/iransans.ttf"));
                }
            }
        }
    }

}