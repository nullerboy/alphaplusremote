package com.alfaplus.tracker.fragments;

import static com.alfaplus.tracker.Params.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.activities.BaseActivity;
import com.alfaplus.tracker.responses.devices.DeviceGroupsResponseItem;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.alfaplus.tracker.responses.devices.SensorsItem;
import com.alfaplus.tracker.responses.devices.TailItem;
import com.github.anastr.speedviewlib.PointerSpeedometer;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class LocationFragment extends Fragment implements
        OnMapReadyCallback, LocationListener {
    private Context context;
    Pref pref;

    GoogleMap map;
    private boolean isSatView = false;
    private float zoomLevel = 0.0F;

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.spinner_devices)
    MaterialSpinner spinnerDevices;

    @BindView(R.id.spinner_devices_groups)
    MaterialSpinner spinnerDevicesGroups;

    @BindView(R.id.text_sensors)
    TextView textSensorsData;

    @BindView(R.id.speedView)
    PointerSpeedometer speedometer;

    List<TailItem> tails;
    LatLng iran = new LatLng(35.6977114, 51.4034201);

    Marker marker;
    LatLng lastPosition;
    float lastAngle = 0;
    int selectedGroup = 0;
    int selectedDevice = 0;

    Location location;
    LocationManager locationManager;
    LocationListener locationListener;
    List<ItemsItem> devices = new ArrayList<>();
    List<DeviceGroupsResponseItem> deviceGroups = new ArrayList<>();
    List<String> deviceGroupsNames = new ArrayList<>();
    List<String> devicesName = new ArrayList<>();
    Timer timer;
    SweetAlertDialog loading;
    Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        view.setLayoutDirection(new Pref(getActivity()).getLanguage().equals("fa") ?
                View.LAYOUT_DIRECTION_RTL :
                View.LAYOUT_DIRECTION_LTR);
        ButterKnife.bind(this, view);
        pref = new Pref(container.getContext());
        tails = new ArrayList<>();
        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);

        lastPosition = new LatLng(0.0, 0.0);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mapView.onCreate(savedInstanceState);
                mapView.onResume();
                mapView.getMapAsync(LocationFragment.this);
            }
        }, 0);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
            dialog.setTitleText(getString(R.string.alert));
            dialog.setContentText(getString(R.string.text_gps_off));
            dialog.setConfirmButton(getString(R.string.ok), new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    dialog.cancel();
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
//            dialog.show();
        } else {

            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
                    dialog.setTitleText("پیام");
                    dialog.setContentText("جی‌پی‌اس موبایل خاموش است. برای استفاده بهتر از برنامه لطفا آن را روشن کنید.");
                    dialog.setConfirmButton("باشه", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });
                    dialog.show();
                }
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 100, this);
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 100, this);

            }
        }

        spinnerDevicesGroups.setOnItemSelectedListener((view12, position, id, item) -> {
            selectedGroup = position;
            selectedDevice = 0;
            devicesName.clear();
            devices = deviceGroups.get(position).getItems();
            for (ItemsItem device : devices) {
                devicesName.add(device.getName());
            }
            spinnerDevices.setItems(devicesName);
            spinnerDevices.setSelectedIndex(selectedDevice);

            if (map != null) {
                ItemsItem device = devices.get(selectedDevice);
                speedometer.speedTo(device.getSpeed());
                LatLng pos = new LatLng(device.getLat(), device.getLng());

                long timestamp = (Long.parseLong(device.getTimestamp() + "000"));
                StringBuilder markerDetail = new StringBuilder();

                markerDetail.append(" 🚗 ")
                        .append(getString(R.string.text_speed))
                        .append(" : ")
                        .append(device.getSpeed()).append(getString(R.string.text_kph))
                        .append("\n");

                for (SensorsItem sensor : device.getSensorsItems()) {
                    markerDetail.append(" 🔵 ")
                            .append(sensor.getName())
                            .append(" : ")
                            .append(sensor.getValue())
                            .append("\n");
                }

                if (pref.getLanguage().equals("fa")) {
                    PersianCalendar persianCalendar = new PersianCalendar(timestamp);
                    markerDetail.append(" 🕗 ")
                            .append(getString(R.string.text_time_))
                            .append(persianCalendar.getPersianYear())
                            .append("/").append(persianCalendar.getPersianMonth() + 1)
                            .append("/").append(persianCalendar.getPersianDay())
                            .append(" ").append(persianCalendar.get(PersianCalendar.HOUR_OF_DAY))
                            .append(":").append(persianCalendar.get(PersianCalendar.MINUTE));

                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(timestamp);
                    markerDetail.append(" 🕗 ")
                            .append(getString(R.string.text_time_))
                            .append(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date(timestamp)));


                }


                textSensorsData.setText(markerDetail.toString());

                addCarOnMap(pos,
                        17,
                        device.getIcon().getPath(),
                        markerDetail.toString(),
                        device.getOnline()
                );

            }


        });

        spinnerDevices.setOnItemSelectedListener((view1, position, id, item) -> {
            if (map != null) {
                selectedDevice = position;
                ItemsItem selectedDevice = devices.get(position);
                speedometer.speedTo(selectedDevice.getSpeed());
                LatLng pos = new LatLng(selectedDevice.getLat(), selectedDevice.getLng());
                long timestamp = (Long.parseLong(selectedDevice.getTimestamp() + "000"));
                StringBuilder markerDetail = new StringBuilder();

                markerDetail.append(" 🚗 ")
                        .append(getString(R.string.text_speed))
                        .append(" : ")
                        .append(selectedDevice.getSpeed()).append(getString(R.string.text_kph))
                        .append("\n");

                for (SensorsItem sensor : selectedDevice.getSensorsItems()) {
                    markerDetail.append(" 🔵 ")
                            .append(sensor.getName())
                            .append(" : ")
                            .append(sensor.getValue())
                            .append("\n");
                }

                if (pref.getLanguage().equals("fa")) {
                    PersianCalendar persianCalendar = new PersianCalendar(timestamp);
                    markerDetail.append(" 🕗 ")
                            .append(getString(R.string.text_time_))
                            .append(persianCalendar.getPersianYear())
                            .append("/").append(persianCalendar.getPersianMonth() + 1)
                            .append("/").append(persianCalendar.getPersianDay())
                            .append(" ").append(persianCalendar.get(PersianCalendar.HOUR_OF_DAY))
                            .append(":").append(persianCalendar.get(PersianCalendar.MINUTE));

                } else {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(timestamp);
                    markerDetail.append(" 🕗 ")
                            .append(getString(R.string.text_time_))
                            .append(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date(timestamp)));


                }


                textSensorsData.setText(markerDetail.toString());

                addCarOnMap(pos,
                        17,
                        selectedDevice.getIcon().getPath(),
                        markerDetail.toString(),
                        selectedDevice.getOnline()
                );

            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setCompassEnabled(false);
        map.getUiSettings().setRotateGesturesEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setTrafficEnabled(true);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        map.setMyLocationEnabled(true);

        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                zoomLevel = map.getCameraPosition().zoom;
            }
        });

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getDevices();
            }
        }, 100, pref.getMapDelay());
//        timer.cancel();

    }

    @OnClick({R.id.button_map_style,
            R.id.button_gps,
            R.id.button_direction,
            R.id.button_map_zoom_out,
            R.id.button_map_zoom_in})
    void OnClick(View view) {
        int id = view.getId();
        if (R.id.button_gps == id) {

//            locationManager.removeUpdates(LocationFragment.this);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                SweetAlertDialog dialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE);
                dialog.setTitleText(getString(R.string.warning));
                dialog.setContentText(getString(R.string.text_gps_off));
                dialog.setConfirmButton(getString(R.string.ok), sweetAlertDialog -> {
                    dialog.cancel();
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                });
                dialog.show();
                return;
            }

            if (location != null) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), zoomLevel));
            } else {
//                Toast.makeText(getActivity(),"درحال دریافت سیگنال از GPS",Toast.LENGTH_SHORT).show();
            }


        } else if (R.id.button_map_style == id) {
            if (map != null) {
                map.setMapType(isSatView ? GoogleMap.MAP_TYPE_NORMAL : GoogleMap.MAP_TYPE_SATELLITE);
                isSatView = !isSatView;
            }
        } else if (R.id.button_direction == id) {
            System.err.println(lastPosition.latitude + "," + lastPosition.longitude);
            startDirectionActivity(lastPosition.latitude, lastPosition.longitude);

        } else if (R.id.button_map_zoom_in == id) {
            if (zoomLevel < map.getMaxZoomLevel()) zoomLevel += 2;
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(map.getCameraPosition().target, zoomLevel));

        } else if (R.id.button_map_zoom_out == id) {
            if (zoomLevel > map.getMinZoomLevel()) zoomLevel -= 2;
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(map.getCameraPosition().target, zoomLevel));

        }

    }


    public void getDevices() {
        Log.e(TAG, "getDevices: Run");
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            deviceGroups.clear();
                            deviceGroupsNames.clear();
                            devicesName.clear();
                            devices.clear();
                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);

                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    DeviceGroupsResponseItem deviceGroup = new Gson().fromJson(jsonArray.getString(i), DeviceGroupsResponseItem.class);
                                    deviceGroups.add(deviceGroup);
                                }

                                for (DeviceGroupsResponseItem group : deviceGroups) {
                                    deviceGroupsNames.add(group.getTitle());
//                                    for (ItemsItem device : group.getItems()) {
//                                        devicesName.add(group.getTitle() + "\t\t|\t\t" + device.getName());
//                                        devicesName.add(device.getName());
//                                        devices.add(device);
//                                    }
                                }

                                spinnerDevicesGroups.setItems(deviceGroupsNames);
                                spinnerDevicesGroups.setSelectedIndex(selectedGroup);

                                devices = devicesResponse.getItems();
                                devicesName = new ArrayList<>();
                                devices = deviceGroups.get(selectedGroup).getItems();
                                ItemsItem item = devices.get(selectedDevice);
                                for (ItemsItem itemsItem : devices) {
                                    Log.e(TAG, "devicesResponse: " + itemsItem.getDeviceData().getSimNumber());
                                    devicesName.add(itemsItem.getName());
                                }
                                spinnerDevices.setItems(devicesName);
                                spinnerDevices.setSelectedIndex(selectedDevice);
                                speedometer.speedTo(item.getSpeed());

                                long timestamp = (Long.parseLong(item.getTimestamp() + "000"));

                                StringBuilder markerDetail = new StringBuilder();

                                if (context != null) {
                                    markerDetail.append(" 🚗 ")
                                            .append(getString(R.string.text_speed))
                                            .append(" : ")
                                            .append(item.getSpeed()).append(getString(R.string.text_kph))
                                            .append("\n");

                                    for (SensorsItem sensor : item.getSensorsItems()) {
                                        markerDetail.append(" 🔵 ")
                                                .append(sensor.getName())
                                                .append(" : ")
                                                .append(sensor.getValue())
                                                .append("\n");
                                    }
                                    if (pref.getLanguage().equals("fa")) {
                                        PersianCalendar persianCalendar = new PersianCalendar(timestamp);
                                        markerDetail.append(" 🕗 ")
                                                .append(getString(R.string.text_time_))
                                                .append(persianCalendar.getPersianYear())
                                                .append("/").append(persianCalendar.getPersianMonth() + 1)
                                                .append("/").append(persianCalendar.getPersianDay())
                                                .append(" ").append(persianCalendar.get(PersianCalendar.HOUR_OF_DAY))
                                                .append(":").append(persianCalendar.get(PersianCalendar.MINUTE));

                                    } else {
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTimeInMillis(timestamp);
                                        markerDetail.append(" 🕗 ")
                                                .append(getString(R.string.text_time_))
                                                .append(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date(timestamp)));


                                    }
                                }


                                textSensorsData.setText(markerDetail.toString());

                                LatLng pos = new LatLng(item.getLat(), item.getLng());
                                if (context != null) {
                                    List<TailItem> tailList = item.getTail();
                                    TailItem tail = tailList.get(tailList.size() - 1);
                                    lastPosition = new LatLng(tail.getLat(), tail.getLng());
                                    addCarOnMap(pos,
                                            item.getIcon().getPath(),
                                            getString(R.string.text_speed) + item.getSpeed() + " km/h",
                                            item.getOnline(),
                                            !item.getIcon().getType().equals("icon"));
                                }


                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.e(TAG, "getDevices onResponse Code:" + response.code());
                            getDevices();
//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e(TAG, "getDevices: Stop");
                        getDevices();
//                        timer.cancel();

//                        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
//                        loading.setTitleText(getString(R.string.oops));
//                        loading.setContentText(getString(R.string.no_internet));
//                        loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> {
//                            getDevices();
//                            loading.cancel();
//                            timer = new Timer();
//                            timer.scheduleAtFixedRate(new TimerTask() {
//                                @Override
//                                public void run() {
//                                    getDevices();
//                                }
//                            }, 100, REFRESH_INTERVAL);
//                        });
//                        loading.show();
                    }
                });
    }

    public void addCarOnMap(LatLng position, String iconUrl, String deviceName, String online, boolean rotate) {
        iconUrl = pref.getServer() + iconUrl;
        System.err.println(iconUrl);
        if (map != null && context != null) {
            if (marker != null) {
                marker.remove();
            }
            Picasso.get().load(iconUrl)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, false);
                            float angle = getAngle(lastPosition, position);
                            Log.e(TAG, "getAngle: " + angle);
                            marker = map.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromBitmap(newBitmap))
                                    .position(position)
                                    .flat(true)
                                    .title(deviceName));

                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, zoomLevel == map.getMinZoomLevel() ? 15 : zoomLevel));
                            if (rotate) marker.setRotation(angle == 0.0 ? lastAngle : angle);
                            if (online.equals("ack") || online.equals("offline")) {
                                lastPosition = position;
                                if (rotate) marker.setRotation(angle);
                            } else {
                                if (rotate) marker.setRotation(angle == 0.0 ? lastAngle : angle);
                                if (angle != 0.0) lastAngle = angle;
                            }
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

        }
    }

    public void addCarOnMap(LatLng position, float zoomLevel, String iconUrl, String deviceName, String online) {
        iconUrl = pref.getAPiServer() + iconUrl;
        System.err.println("spinnerDevices: " + position.latitude + "," + position.longitude);
        System.err.println(iconUrl);

        if (map != null) {
            if (marker != null) {
                marker.remove();
            }
            Picasso.get().load(iconUrl)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * 2, bitmap.getHeight() * 2, false);
                            float angle = getAngle(lastPosition, position);
                            marker = map.addMarker(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromBitmap(newBitmap))
                                    .position(position)
                                    .flat(true)
                                    .title(deviceName));

                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, zoomLevel));
                            if (online.equals("ack") || online.equals("offline")) {
                                lastPosition = position;
                                marker.setRotation(0);
                            } else {
                                marker.setRotation(angle == 0.0 ? lastAngle : angle);
                                if (angle != 0.0) lastAngle = angle;
                            }

                            lastPosition = position;

//                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(lastPosition, zoomLevel));

                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

        }
    }

    public float getAngle(LatLng p1, LatLng p2) {
        float angle = (float) Math.toDegrees(Math.atan2(p2.longitude - p1.longitude, p2.latitude - p1.latitude));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }

        //INSERT CUSTOM CODE HERE


    }

    @Override
    public void onPause() {
//        Toast.makeText(getActivity(), "onPause", Toast.LENGTH_SHORT).show();
        locationManager.removeUpdates(this);
        timer.cancel();
        super.onPause();
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        this.location = location;
        Log.e(TAG, "onLocationChanged: " + location.getLatitude() + "," + location.getLongitude());
    }


    public void startDirectionActivity(double lat, double lng) {
        String uri = "http://maps.google.com/maps?f=d&hl=en&daddr=" + lat + "," + lng;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
        getActivity().startActivity(Intent.createChooser(intent, getString(R.string.select_direction_app)));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }
}