package com.alfaplus.tracker.adapters.DevicesConfig;

import com.google.gson.annotations.SerializedName;

public class DevicesItem{

	@SerializedName("password")
	private String password;

	@SerializedName("powerState")
	private boolean powerState;

	@SerializedName("spyState")
	private boolean spyState;

	@SerializedName("newPassword")
	private String newPassword;

	@SerializedName("id")
	private int id;

	@SerializedName("alertState")
	private boolean alertState;

	@SerializedName("simNumber")
	private String simNumber;

	@SerializedName("name")
	private String name;

	@SerializedName("additional_notes")
	private String additionalNotes;

	@SerializedName("temp_admin")
	private String tempAdmin;

	public String getTempAdmin() {
		return tempAdmin;
	}

	public void setTempAdmin(String tempAdmin) {
		this.tempAdmin = tempAdmin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setPowerState(boolean powerState){
		this.powerState = powerState;
	}

	public boolean getPowerState(){
		return powerState;
	}

	public void setSpyState(boolean spyState){
		this.spyState = spyState;
	}

	public boolean getSpyState(){
		return spyState;
	}

	public void setNewPassword(String newPassword){
		this.newPassword = newPassword;
	}

	public String getNewPassword(){
		return newPassword;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setAlertState(boolean alertState){
		this.alertState = alertState;
	}

	public boolean getAlertState(){
		return alertState;
	}

	public void setSimNumber(String simNumber){
		this.simNumber = simNumber;
	}

	public String getSimNumber(){
		return simNumber;
	}

	public String getAdditionalNotes() {
		return additionalNotes;
	}

	public void setAdditionalNotes(String additionalNotes) {
		this.additionalNotes = additionalNotes;
	}
}