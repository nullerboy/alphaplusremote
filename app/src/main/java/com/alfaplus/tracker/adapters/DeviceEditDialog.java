package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("NonConstantResourceId")
public class DeviceEditDialog extends Dialog {

    private Context context;
    private int deviceId;
    private String deviceName;
    private String simNumber;

    OnEditDeviceSubmit onSubmit;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.input_name)
    EditText inputName;

    @BindView(R.id.input_sim_number)
    EditText inputSimNumber;

    public DeviceEditDialog(@NonNull Context context, int deviceId, String deviceName, String simNumber) {
        super(context);
        this.context = context;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.simNumber = simNumber;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_edit_device);
        ButterKnife.bind(this);

        inputName.setText(deviceName);
        inputSimNumber.setText(simNumber);

    }

    public void setOnSubmit(OnEditDeviceSubmit onSubmit) {
        this.onSubmit = onSubmit;
    }

    @OnClick({R.id.button_submit})
    void OnClick(View view) {
        int id = view.getId();

        if (R.id.button_submit == id) {
            String name = inputName.getText().toString();
            String simNumber = inputSimNumber.getText().toString();
            if (name.isEmpty() || simNumber.isEmpty()) {
                Toast.makeText(context, getContext().getString(R.string.requeird_fields), Toast.LENGTH_SHORT).show();
            } else {
                onSubmit.onSubmit(deviceId, name, simNumber);
            }
            cancel();
        }
    }
}
