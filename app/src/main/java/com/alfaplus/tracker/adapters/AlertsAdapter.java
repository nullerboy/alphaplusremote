package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.responses.alerts.AlertsItem;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint({"NonConstantResourceId", "SetTextI18n"})
public class AlertsAdapter extends RecyclerView.Adapter<AlertsAdapter.Holder> {

    Context context;
    public static int BUTTON_EDIT = 0;
    public static int BUTTON_REMOVE = 1;
    List<AlertsItem> alerts;
    Pref pref;
    OnAlertButtonClick onAlertButtonClick;

    public AlertsAdapter(List<AlertsItem> alerts) {
        this.alerts = alerts;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_alert, parent, false);
        pref = new Pref(parent.getContext());
        context=parent.getContext();
        view.setLayoutDirection(pref.getLanguage().equals("fa")?
                View.LAYOUT_DIRECTION_RTL:
                View.LAYOUT_DIRECTION_LTR);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        AlertsItem alert = alerts.get(position);

        if (pref.getLanguage().equals("fa")){
            PersianCalendar calendar = convertToTimestamp(alert.getCreatedAt());
            holder.textCreated.setText(calendar.getPersianYear() + "-" + (calendar.getPersianMonth() + 1) + "-" + calendar.getPersianDay());
        }else {
            holder.textCreated.setText(alert.getCreatedAt());
        }

        holder.textName.setText(" 🔔 " + alert.getName());
        holder.textDevices.setText(getDevicesNames(alert.getDevices()));

    }

    @Override
    public int getItemCount() {
        return alerts.size();
    }

    public void setOnAlertButtonClick(OnAlertButtonClick onAlertButtonClick) {
        this.onAlertButtonClick = onAlertButtonClick;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_alert)
        CardView cardAlert;
        @BindView(R.id.text_name)
        TextView textName;
        @BindView(R.id.text_devices)
        TextView textDevices;
        @BindView(R.id.text_created)
        TextView textCreated;
        @BindView(R.id.button_edit)
        Button btnEdit;
        @BindView(R.id.button_remove)
        Button btnRemove;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btnEdit.setOnClickListener(v -> onAlertButtonClick.onButtonClick(getAdapterPosition(), BUTTON_EDIT));
            btnRemove.setOnClickListener(v -> onAlertButtonClick.onButtonClick(getAdapterPosition(), BUTTON_REMOVE));

        }
    }

    @SuppressLint("SimpleDateFormat")
    public PersianCalendar convertToTimestamp(String text) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return new PersianCalendar(dateFormat.parse(text).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getDevicesNames(List<Integer> devicesId) {
        StringBuilder devicesNames = new StringBuilder();
        List<DevicesItem> devices = pref.getDevicesConfigs();
        if (devicesId.size() == 0) return context.getString(R.string.no_cars_found);
        for (DevicesItem device : devices) {
            for (Integer id : devicesId) {
                if (id == device.getId())
                    devicesNames.append(device.getName()).append("✔ ");

            }

        }
        return devicesNames.toString();
    }

}
