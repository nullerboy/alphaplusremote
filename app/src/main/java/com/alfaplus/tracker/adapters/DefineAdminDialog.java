package com.alfaplus.tracker.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DefineAdminDialog extends Dialog {

    @BindView(R.id.input_mobile)
    EditText inputMobile;

    public AdminListener adminListener;

    public DefineAdminDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_define_admin);

        ButterKnife.bind(this);

    }

    @OnClick({R.id.button_ok, R.id.button_cancel})
    void OnClick(View view) {
        int id = view.getId();
        if (R.id.button_ok == id) {
            String mobile = inputMobile.getText().toString();
            if (mobile.length() != 11) {
                Toast.makeText(getContext(), getContext().getString(R.string.incorrect_mobile_number), Toast.LENGTH_SHORT).show();
            } else {
                adminListener.OnAdminDefined(mobile, true);
            }
            cancel();
        } else if (R.id.button_cancel == id) {
            String mobile = inputMobile.getText().toString();
            adminListener.OnAdminDefined(mobile, false);
            cancel();
        }
    }

    public void setAdminListener(AdminListener adminListener) {
        this.adminListener = adminListener;
    }

    public interface AdminListener {
        void OnAdminDefined(String newAdminMobile, boolean isNew);
    }

}
