package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.activities.BaseActivity;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("NonConstantResourceId")
public class ServerDialog extends Dialog {

    private Context context;
    private Pref pref;

    @BindView(R.id.spinner_servers)
    MaterialSpinner spinnerServers;

    @BindView(R.id.input_server_address)
    EditText inputServerAddress;

    public ServerDialog(@NonNull Context context) {
        super(context);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_server);
        this.context = context;
        ButterKnife.bind(this);

        pref = new Pref(context);
        spinnerServers.setItems(context.getString(R.string.text_server_1),
                context.getString(R.string.text_server_2),
                context.getString(R.string.text_custom_server));

        if (pref.getServer().equals(Params.BASE_SERVER_1)) {
            spinnerServers.setSelectedIndex(0);
        } else if (pref.getServer().equals(Params.BASE_SERVER_2)) {
            spinnerServers.setSelectedIndex(1);
        } else {
            spinnerServers.setSelectedIndex(2);
            inputServerAddress.setVisibility(View.VISIBLE);
            inputServerAddress.setText(pref.getServer());
        }

        spinnerServers.setOnItemSelectedListener((view, position, id, item) -> {
            if (position == 2) {
                inputServerAddress.setVisibility(View.VISIBLE);
            } else {
                inputServerAddress.setVisibility(View.GONE);
            }
        });
    }

    @OnClick({R.id.button_ok})
    void OnClick(View view) {
        if (spinnerServers.getSelectedIndex() == 0) {
            pref.setServer(Params.BASE_SERVER_1);
        } else if (spinnerServers.getSelectedIndex() == 1) {
            pref.setServer(Params.BASE_SERVER_2);
        } else if (spinnerServers.getSelectedIndex() == 2) {
            String serverAddress = inputServerAddress.getText().toString();
            if (serverAddress.contains("https://") || serverAddress.contains("http://")) {
                if (!serverAddress.endsWith("/")) {
                    serverAddress += "/";
                    pref.setServer(serverAddress);
                }
            } else {
                Toast.makeText(context, R.string.text_invalid_server_address, Toast.LENGTH_LONG).show();
                return;
            }
        }
        System.err.println("New Server :" + pref.getServer());
        BaseActivity.setServerAddress();
        cancel();
    }
}
