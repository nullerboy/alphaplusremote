package com.alfaplus.tracker.adapters;

import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;

public interface OnDeviceCheckBoxClick {
    void onCheckChanged(DevicesItem device, boolean isChecked);
}
