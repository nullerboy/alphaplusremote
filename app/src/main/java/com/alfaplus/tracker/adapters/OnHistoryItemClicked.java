package com.alfaplus.tracker.adapters;

import com.alfaplus.tracker.responses.history.item.ItemsItem;

import java.util.List;

public interface OnHistoryItemClicked {
    void onHistoryClick(int index,List<ItemsItem> items);
}
