package com.alfaplus.tracker.adapters;

import static com.alfaplus.tracker.activities.BaseActivity.api;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.responses.events.DataItem;
import com.alfaplus.tracker.responses.events.EventsResponse;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint({"NonConstantResourceId", "SetTextI18n"})
public class AddAlertDialog extends Dialog {

    Context context;
    Pref pref;
    DevicesCheckBoxAdapter adapter;
    OnDevicesSubmit onDevicesSubmit;

    @BindView(R.id.layout_dialog)
    LinearLayout layoutDialog;

    @BindView(R.id.progress_loading)
    ProgressBar loading;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.recycler_devices)
    RecyclerView recyclerDevices;

    @BindView(R.id.spinner_alert_type)
    MaterialSpinner spinnerType;

    @BindView(R.id.input_value)
    EditText inputValue;

    @BindView(R.id.input_name)
    EditText inputName;

    @BindView(R.id.button_submit)
    Button btnSubmit;

    List<Integer> devicesID = new ArrayList<>();
    List<String> spinnerItems = new ArrayList<>();

    String name = "";
    String type = "";
    Integer[] devices;
    int alertId = -1;
    int value;

    public AddAlertDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public AddAlertDialog(@NonNull Context context, int AlertId, String name, String type, Integer[] devices, int value) {
        super(context);
        this.context = context;
        this.name = name;
        this.type = type;
        this.devices = devices;
        this.value = value;
        this.alertId = AlertId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_add_alert);
        ButterKnife.bind(this);

        pref = new Pref(context);
        adapter = new DevicesCheckBoxAdapter(devices, pref.getDevicesConfigs());
        recyclerDevices.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerDevices.setAdapter(adapter);

        spinnerItems.add(context.getString(R.string.overspeed));
        spinnerType.setItems(spinnerItems);

        adapter.setOnDeviceCheckBoxClick((device, isChecked) -> {
            if (devicesID.contains(device.getId())) {
                if (!isChecked) {
                    devicesID.remove((Integer) device.getId());
                    return;
                }
            }
            devicesID.add(device.getId());
        });

        if (alertId != -1) {
            inputName.setText(name);
            textTitle.setText(R.string.edit_alert);

            getEventsByType(type);
        }


    }

    public void setOnDevicesSubmit(OnDevicesSubmit onDevicesSubmit) {
        this.onDevicesSubmit = onDevicesSubmit;
    }

    @OnClick({R.id.button_submit})
    void OnClick(View view) {

        int id = view.getId();

        if (R.id.button_submit == id) {
            Integer[] ids = devicesID.toArray(new Integer[0]);

            if (inputValue.getText().toString().isEmpty()) {
                Toast.makeText(context,context.getString(R.string.enter_value), Toast.LENGTH_SHORT).show();
            } else if (inputName.getText().toString().isEmpty()) {
                Toast.makeText(context, context.getString(R.string.no_name_entered), Toast.LENGTH_SHORT).show();
            } else if (ids.length == 0) {
                Toast.makeText(context, context.getString(R.string.no_car_selected), Toast.LENGTH_SHORT).show();
            } else {
                int value = Integer.parseInt(inputValue.getText().toString());
                String name = inputName.getText().toString();
                onDevicesSubmit.onSubmit(ids, value, name);
            }

        }
    }

    public void getEventsByType(String alertType) {
        loading.setVisibility(View.VISIBLE);
        layoutDialog.setVisibility(View.GONE);
        api.getEvents(pref.getUserApiHash(), alertType)
                .enqueue(new Callback<EventsResponse>() {
                    @Override
                    public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                        loading.setVisibility(View.GONE);
                        layoutDialog.setVisibility(View.VISIBLE);
                        List<DataItem> dataItems = response.body().getItems().getData();
                        for (DataItem data : dataItems) {
                            if (alertId == data.getAlertId())
                                inputValue.setText(data.getAdditional().getOverspeedSpeed()+"");
                            return;
                        }
                    }

                    @Override
                    public void onFailure(Call<EventsResponse> call, Throwable t) {
                        System.err.println("onFailure");
                    }
                });
    }

}
