package com.alfaplus.tracker.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.responses.history.item.HistoryItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CoursesDialog extends Dialog {

    private Context context;
    private List<HistoryItem> historyItems;
    @BindView(R.id.recycler_history)
    RecyclerView recycler;
    private OnHistoryItemClicked onHistoryItemClicked;
    String language = "fa";


    public CoursesDialog(@NonNull Context context, List<HistoryItem> historyItems, String language) {
        super(context);
        this.context = context;
        this.historyItems = historyItems;
        this.language = language;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_courses);
        ButterKnife.bind(this);

        HistoryAdapter adapter = new HistoryAdapter(historyItems, language);
        adapter.setOnClicked(new OnClicked() {
            @Override
            public void OnClick(int i) {
                HistoryItem history = historyItems.get(i);
                onHistoryItemClicked.onHistoryClick(i, history.getItems());
            }
        });
        recycler.setLayoutManager(new LinearLayoutManager(context));
        recycler.setAdapter(adapter);

    }

    public void setOnHistoryItemClicked(OnHistoryItemClicked onHistoryItemClicked) {
        this.onHistoryItemClicked = onHistoryItemClicked;
    }
}
