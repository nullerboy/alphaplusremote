package com.alfaplus.tracker.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LangDialog extends Dialog {

    @BindView(R.id.radio_language)
    RadioGroup radioLang;

    @BindView(R.id.button_ok)
    Button btnSubmit;

    private Context context;
    private Pref pref;
    private OnLanguageSelect onLanguageSelect;
    private String lang = "";

    public LangDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_language);
        ButterKnife.bind(this);

        pref = new Pref(context);
        lang = pref.getLanguage();

        if (pref.getLanguage().equals("en")) {
            radioLang.check(R.id.radio_en);
        } else if (pref.getLanguage().equals("fr")) {
            radioLang.check(R.id.radio_fr);
        } else if (pref.getLanguage().equals("fa")) {
            radioLang.check(R.id.radio_fa);
        }

        radioLang.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radio_en) {
                lang = "en";
            } else if (checkedId == R.id.radio_fa) {
                lang = "fa";
            } else if (checkedId == R.id.radio_fr) {
                lang = "fr";
            }
        });

        btnSubmit.setOnClickListener(v -> {
            onLanguageSelect.onSelect(lang);
        });
    }

    public void setOnLanguageSelect(OnLanguageSelect onLanguageSelect) {
        this.onLanguageSelect = onLanguageSelect;
    }
}
