package com.alfaplus.tracker.adapters;

public interface OnDevicesSubmit {
    void onSubmit(Integer[] devices_id,int value,String name);
}
