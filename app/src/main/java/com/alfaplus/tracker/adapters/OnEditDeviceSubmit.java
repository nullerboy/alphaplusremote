package com.alfaplus.tracker.adapters;

public interface OnEditDeviceSubmit {
    void onSubmit(int deviceId,String name,String simNumber);
}
