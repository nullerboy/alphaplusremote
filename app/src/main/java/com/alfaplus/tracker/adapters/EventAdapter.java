package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.responses.events.DataItem;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("NonConstantResourceId")
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.Holder> {

    OnClicked onClicked;
    List<DataItem> dataItems;
    String language = "fa";

    public EventAdapter(List<DataItem> dataItems, String language) {
        this.dataItems = dataItems;
        this.language = language;
    }

    public EventAdapter(List<DataItem> dataItems) {
        this.dataItems = dataItems;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_event, parent, false);
        return new Holder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        DataItem data = dataItems.get(position);

        holder.textDeviceName.setText(data.getDeviceName());
        holder.textMessage.setText(data.getMessage());

        if (data.getType().equals("overspeed")) {
            holder.textSpeed.setText(data.getSpeed() + " Km/h");
            holder.textOverSpeed.setText(data.getAdditional().getOverspeedSpeed() + " Km/h");
            holder.layoutOverSpeed.setVisibility(View.VISIBLE);
        } else {
            holder.layoutOverSpeed.setVisibility(View.GONE);
        }

        if (language.equals("fa")){
            PersianCalendar calendar = convertToTimestamp(data.getTime());
            holder.textTime.setText(calendar.getPersianYear() + "-" + (calendar.getPersianMonth() + 1) + "-" + calendar.getPersianDay()
                    + " " + calendar.get(PersianCalendar.HOUR_OF_DAY) + ":" + calendar.get(PersianCalendar.MINUTE));
            holder.layoutEvent.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }else {
            holder.textTime.setText(data.getTime());
            holder.layoutEvent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_event)
        CardView cardEvent;
        @BindView(R.id.text_device_name)
        TextView textDeviceName;
        @BindView(R.id.text_message)
        TextView textMessage;
        @BindView(R.id.text_time)
        TextView textTime;
        @BindView(R.id.text_speed)
        TextView textSpeed;
        @BindView(R.id.text_over_speed)
        TextView textOverSpeed;
        @BindView(R.id.layout_over_speed)
        LinearLayout layoutOverSpeed;
        @BindView(R.id.layout_event)
        LinearLayout layoutEvent;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            cardEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicked.OnClick(getAdapterPosition());
                }
            });
        }
    }

    public void setOnClicked(OnClicked onClicked) {
        this.onClicked = onClicked;
    }

    @SuppressLint("SimpleDateFormat")
    public PersianCalendar convertToTimestamp(String text) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return new PersianCalendar(dateFormat.parse(text).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

}
