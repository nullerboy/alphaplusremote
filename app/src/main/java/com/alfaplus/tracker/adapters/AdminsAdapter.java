package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.helper.widget.Layer;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("NonConstantResourceId")
public class AdminsAdapter extends RecyclerView.Adapter<AdminsAdapter.Holder> {

    List<String> numbers;
    Context context;
    OnClicked onClicked;
    String language = "fa";

    public AdminsAdapter(List<String> numbers, String language) {
        this.numbers = numbers;
        this.language = language;
    }

    public AdminsAdapter(List<String> numbers) {
        this.numbers = numbers;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_admin, parent, false);
        this.context = parent.getContext();

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.textNumber.setText(numbers.get(position));
        if (language.equals("fa")){
            holder.layoutAdmin.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }else {
            holder.layoutAdmin.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public void setOnClicked(OnClicked onClicked) {
        this.onClicked = onClicked;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.layout_admin)
        LinearLayout layoutAdmin;

        @BindView(R.id.text_number)
        TextView textNumber;

        @BindView(R.id.button_remove)
        Button btnRemove;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicked.OnClick(getAdapterPosition());
                }
            });
        }
    }
}
