package com.alfaplus.tracker.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SensDialog extends Dialog {


    public SensDialogListener listener;
    @BindView(R.id.seekbar)
    SeekBar seekBar;
    Pref pref;
    Context context;

    public SensDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_sens);

        ButterKnife.bind(this);
        pref = new Pref(context);

        seekBar.setProgress(pref.getSensLevel());


    }

    @OnClick({R.id.button_ok, R.id.button_cancel})
    void click(View view) {
        if (R.id.button_ok == view.getId()) {
            listener.listener(seekBar.getProgress());
            pref.setSensLevel(seekBar.getProgress());
            cancel();
        } else if (R.id.button_cancel == view.getId()) {
            cancel();
        }


    }


    public interface SensDialogListener {
        void listener(int sensitivity);
    }

    public void setListener(SensDialogListener listener) {
        this.listener = listener;
    }
}
