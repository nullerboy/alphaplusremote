package com.alfaplus.tracker.adapters;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PwdDialog extends Dialog {

    public PwdListener listener;

    @BindView(R.id.input_current_pass)
    EditText inputCurrentPass;
    @BindView(R.id.input_new_pass_1)
    EditText inputPass1;
    @BindView(R.id.input_new_pass_2)
    EditText inputPass2;

    public PwdDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.dialog_pwd);

        ButterKnife.bind(this);


    }

    @OnClick({R.id.button_ok, R.id.button_cancel})
    void click(View view) {
        if (R.id.button_ok == view.getId()) {
            String pass1 = inputPass1.getText().toString();
            String pass2 = inputPass2.getText().toString();

            if (pass1.equals(pass2)) {
                listener.listener(pass1);
                cancel();
            } else {
                inputCurrentPass.setText("");
                inputPass1.setText("");
                inputPass2.setText("");
                Toast.makeText(getContext(), getContext().getString(R.string.password_not_confirmed), Toast.LENGTH_SHORT).show();
            }
        }else if (R.id.button_cancel==view.getId()){
            cancel();
        }

    }


    public interface PwdListener {
        void listener(String newPassword);
    }

    public void setListener(PwdListener listener) {
        this.listener = listener;
    }
}
