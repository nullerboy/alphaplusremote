package com.alfaplus.tracker.adapters;

public interface OnLanguageSelect {
    void onSelect(String language);
}
