package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("NonConstantResourceId")
public class DevicesCheckBoxAdapter extends RecyclerView.Adapter<DevicesCheckBoxAdapter.Holder> {

    Integer[] devicesId;
    List<DevicesItem> devices;
    OnDeviceCheckBoxClick onDeviceCheckBoxClick;

    public DevicesCheckBoxAdapter(List<DevicesItem> devices) {
        this.devices = devices;
    }

    public DevicesCheckBoxAdapter(Integer[] devicesId, List<DevicesItem> devices) {
        this.devicesId = devicesId;
        this.devices = devices;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_device_checkbox, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        DevicesItem device = devices.get(position);
        holder.deviceCheckBox.setText(device.getName());

        if (devicesId != null)
            for (Integer id : devicesId) {
                holder.deviceCheckBox.setChecked(id == device.getId());
            }

    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public void setOnDeviceCheckBoxClick(OnDeviceCheckBoxClick onDeviceCheckBoxClick) {
        this.onDeviceCheckBoxClick = onDeviceCheckBoxClick;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.checkbox_device)
        CheckBox deviceCheckBox;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            deviceCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                onDeviceCheckBoxClick.onCheckChanged(devices.get(getAdapterPosition()), isChecked);
            });
        }
    }
}
