package com.alfaplus.tracker.adapters;

public interface OnSms {
    void onReceive(String text, String number);
}
