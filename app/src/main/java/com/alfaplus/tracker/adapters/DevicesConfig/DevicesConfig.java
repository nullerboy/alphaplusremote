package com.alfaplus.tracker.adapters.DevicesConfig;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DevicesConfig{

	@SerializedName("devices")
	private List<DevicesItem> devices;

	public void setDevices(List<DevicesItem> devices){
		this.devices = devices;
	}

	public List<DevicesItem> getDevices(){
		return devices;
	}

	@Override
 	public String toString(){
		return 
			"DevicesConfig{" + 
			"devices = '" + devices + '\'' + 
			"}";
		}
}