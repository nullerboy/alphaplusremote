package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.responses.history.item.HistoryItem;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.Holder> {

    Context context;
    private OnClicked onClicked;
    private List<HistoryItem> historyItems;
    String language = "fa";

    public HistoryAdapter(List<HistoryItem> historyItems, String language) {
        this.historyItems = historyItems;
        this.language = language;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history, parent, false);
        context = parent.getContext();
        return new Holder(view);
    }

    @SuppressLint({"SetTextI18n", "SimpleDateFormat"})
    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        HistoryItem historyItem = historyItems.get(position);
        holder.textDistance.setText(context.getString(R.string.text_distance) + historyItem.getDistance() + " km");
//        holder.textTotalTime.setText(context.getString(R.string.text_duration) + historyItem.getTime());
        holder.textTotalTime.setText(context.getString(R.string.text_duration) + secondsToString(historyItem.getTimeSeconds()));

        long startTimestamp = convertStringToTimestamp(historyItem.getShow());
        long endTimestamp = convertStringToTimestamp(historyItem.getLeft());


        if (language.equals("fa")) {
            PersianCalendar calendarShow = new PersianCalendar(startTimestamp);
            PersianCalendar calendarLeft = new PersianCalendar(endTimestamp);

            holder.textStartDate.setText(calendarShow.getPersianDay() + " " + calendarShow.getPersianMonthName());
            holder.textEndDate.setText(calendarLeft.getPersianDay() + " " + calendarLeft.getPersianMonthName());
            holder.textStartTime.setText(context.getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(startTimestamp)));
            holder.textEndTime.setText(context.getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(endTimestamp)));
        } else {
            Calendar calendarShow = Calendar.getInstance();
            calendarShow.setTimeInMillis(startTimestamp);

            Calendar calendarLeft = Calendar.getInstance();
            calendarLeft.setTimeInMillis(endTimestamp);

            holder.textStartDate.setText(new SimpleDateFormat("dd MMM").format(new Date(startTimestamp)));
            holder.textEndDate.setText(new SimpleDateFormat("dd MMM").format(new Date(endTimestamp)));
            holder.textStartTime.setText(context.getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(startTimestamp)));
            holder.textEndTime.setText(context.getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(endTimestamp)));

        }

        if (historyItem.getStatus() == 2) {
            holder.textD.setVisibility(View.GONE);
            holder.textP.setVisibility(View.VISIBLE);
            holder.textDistance.setText(context.getString(R.string.text_parked_car));
            holder.textA.setVisibility(View.GONE);
            holder.textB.setVisibility(View.GONE);
        } else {
            holder.textD.setVisibility(View.VISIBLE);
            holder.textP.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return historyItems.size();
    }

    public void setOnClicked(OnClicked onClicked) {
        this.onClicked = onClicked;
    }

    @SuppressLint("NonConstantResourceId")
    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_history)
        CardView card;
        @BindView(R.id.text_start_date)
        TextView textStartDate;
        @BindView(R.id.text_start_time)
        TextView textStartTime;
        @BindView(R.id.text_end_date)
        TextView textEndDate;
        @BindView(R.id.text_end_time)
        TextView textEndTime;
        @BindView(R.id.text_distance)
        TextView textDistance;
        @BindView(R.id.text_total_time)
        TextView textTotalTime;
        @BindView(R.id.text_d)
        TextView textD;
        @BindView(R.id.text_p)
        TextView textP;
        @BindView(R.id.text_a)
        TextView textA;
        @BindView(R.id.text_b)
        TextView textB;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClicked.OnClick(getAdapterPosition());
                }
            });
        }
    }

    @SuppressLint("SimpleDateFormat")
    private static long convertStringToTimestamp(String something) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Timestamp timestamp = null;
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(something);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return timestamp.getTime();
    }

    public String secondsToString(int totalSecs) {
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        return hours + "H " + minutes + "m " + seconds + "s";
    }
}
