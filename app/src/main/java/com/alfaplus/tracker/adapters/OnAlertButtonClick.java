package com.alfaplus.tracker.adapters;

public interface OnAlertButtonClick {
    void onButtonClick(int position,int buttonId);
}
