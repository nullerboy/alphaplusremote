package com.alfaplus.tracker.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.responses.devices.ItemsItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("NonConstantResourceId")
public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.Holder> {

    Context context;
    OnClicked onClicked;
    List<ItemsItem> devices;
    String language="fa";

    public DevicesAdapter(List<ItemsItem> devices, String language) {
        this.devices = devices;
        this.language = language;
    }

    public DevicesAdapter(List<ItemsItem> devices) {
        this.devices = devices;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_device, parent, false);
        context = parent.getContext();
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        ItemsItem device = devices.get(position);
        holder.textName.setText(device.getName());
        holder.textIMEI.setText(device.getDeviceData().getImei());
        holder.textModel.setText(device.getDeviceData().getDeviceModel().isEmpty() ? context.getString(R.string.unknown) : device.getDeviceData().getDeviceModel());

        if (device.getDeviceData().getSimNumber().isEmpty()) {
            holder.textSimNumber.setTextColor(Color.RED);
            holder.textSimNumber.setText(R.string.not_submited);
        } else {
            holder.textSimNumber.setTextColor(Color.BLACK);
            holder.textSimNumber.setText(device.getDeviceData().getSimNumber());
        }

        if (language.equals("fa")){
            holder.layoutDevices.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }else {
            holder.layoutDevices.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void setOnClicked(OnClicked onClicked) {
        this.onClicked = onClicked;
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.layout_devices)
        LinearLayout layoutDevices;
        @BindView(R.id.text_name)
        TextView textName;
        @BindView(R.id.text_device_model)
        TextView textModel;
        @BindView(R.id.text_sim_number)
        TextView textSimNumber;
        @BindView(R.id.text_imei)
        TextView textIMEI;
        @BindView(R.id.button_edit)
        Button btnEdit;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btnEdit.setOnClickListener(clicked -> {
                if (onClicked != null) {
                    onClicked.OnClick(getAdapterPosition());
                }
            });

        }
    }
}
