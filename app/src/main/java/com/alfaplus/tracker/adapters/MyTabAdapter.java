package com.alfaplus.tracker.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.alfaplus.tracker.fragments.CourseFragment;
import com.alfaplus.tracker.fragments.HistoryFragment;
import com.alfaplus.tracker.fragments.LocationFragment;

public class MyTabAdapter extends FragmentPagerAdapter {


    Context context;
    int tabsCount;

    public MyTabAdapter(@NonNull FragmentManager fm, Context context, int tabsCount) {
        super(fm);
        this.context = context;
        this.tabsCount = tabsCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new LocationFragment();
        } else if (position == 1) {
            return new HistoryFragment();
        } else if (position == 2) {
            return new CourseFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return tabsCount;
    }
}
