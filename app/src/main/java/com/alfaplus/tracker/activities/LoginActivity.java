package com.alfaplus.tracker.activities;

import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.BuildConfig;
import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.adapters.LangDialog;
import com.alfaplus.tracker.adapters.ServerDialog;
import com.alfaplus.tracker.responses.authentication.Authentication;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class LoginActivity extends BaseActivity {

    @BindView(R.id.text_version)
    TextView textVersion;

    @BindView(R.id.input_mobile)
    EditText inputMobile;

    @BindView(R.id.input_email)
    EditText inputEmail;

    @BindView(R.id.input_password)
    EditText inputPassword;

    private SweetAlertDialog dialog;
    private SweetAlertDialog loading;
    private List<DevicesItem> devicesItems = new ArrayList<>();

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        textVersion.setText(getString(R.string.version) + BuildConfig.VERSION_NAME);

        checkPermissions();

        System.err.println(pref.isFirstTime());


    }

    @OnClick({R.id.button_submit,
            R.id.button_site,
            R.id.button_whatsapp,
            R.id.button_demo,
            R.id.button_learn,
            R.id.button_language,
            R.id.button_server
    })
    void onClick(View view) {
        if (view.getId() == R.id.button_submit) {

//            String mobile = inputMobile.getText().toString();
            String email = inputEmail.getText().toString();
            String password = inputPassword.getText().toString();

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                Toast.makeText(context, getString(R.string.incorrect_email), Toast.LENGTH_LONG).show();
            } else if (email.equals(Params.DEMO_USERNAME)) {
                authentication(email, password);
                pref.setDemoMode(true);
            } else {
                authentication(email, password);
            }

        } else if (view.getId() == R.id.button_site) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://alfaagps.com/")));
        } else if (view.getId() == R.id.button_whatsapp) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/989921650225")));
        } else if (view.getId() == R.id.button_learn) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://alfaagps.com/category/learning/")));
        } else if (view.getId() == R.id.button_demo) {
            inputEmail.setText(Params.DEMO_USERNAME);
            inputPassword.setText(Params.DEMO_PASSWORD);
            inputMobile.setText(Params.DEMO_MOBILE);
            pref.setDemoMode(true);
            authentication(Params.DEMO_USERNAME,
                    Params.DEMO_PASSWORD);
        } else if (view.getId() == R.id.button_language) {
            showLanguageDialog();
        } else if (view.getId() == R.id.button_server) {
            ServerDialog serverDialog = new ServerDialog(context);
            serverDialog.show();
        }

    }

    public void showLanguageDialog() {
        LangDialog langDialog = new LangDialog(LoginActivity.this);
        langDialog.setOnLanguageSelect(language -> {
            if (!language.equals(pref.getLanguage())) {
                pref.setLanguage(language);
                langDialog.cancel();
                startActivity(new Intent(LoginActivity.this, SplashActivity.class));
                finish();
            }
            langDialog.cancel();
        });
        langDialog.show();
        pref.setFirstTime(false);
    }

    public void authentication(String email, String password) {
        dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.login_title));
        dialog.show();

        api.Authentication(email, password).enqueue(new Callback<Authentication>() {
            @Override
            public void onResponse(Call<Authentication> call, Response<Authentication> response) {
                System.err.println("Authentication Server :" + call.request().url());
                System.err.println("Authentication :" + response.code());
                if (response.code() == 200) {
                    dialog.cancel();
                    Authentication authentication = response.body();
                    pref.setUserApiHash(authentication.getUserApiHash());
                    System.err.println(authentication.getUserApiHash());
                    System.err.println(pref.getUserApiHash());

                    sync();

//                    startActivity(new Intent(context, MainActivity.class));
//                    finish();

                } else {
                    dialog.cancel();
                    dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    dialog.setTitle(getString(R.string.oops));
                    dialog.setContentText(getString(R.string.incorrect_login));
                    dialog.setContentTextSize(14);
                    dialog.show();

                }

            }

            @Override
            public void onFailure(Call<Authentication> call, Throwable t) {
                Toast.makeText(context, getString(R.string.no_internet), Toast.LENGTH_LONG)
                        .show();
                System.err.println("Authentication :" + call.request().url());
                dialog.cancel();
            }
        });

    }

    public void sync() {
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText(getString(R.string.text_syncing));
        loading.show();
        api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        System.err.println("sync :" + call.request().url());
                        System.err.println("sync :" + response.code());
                        if (response.code() == 200) {

                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                List<ItemsItem> devices = devicesResponse.getItems();

                                for (ItemsItem device : devices) {
                                    DevicesItem devicesItem = new DevicesItem();
                                    Log.e(TAG, "devicesItem: " + device.getName());

                                    devicesItem.setId(device.getId());
                                    devicesItem.setName(device.getName());
                                    devicesItem.setSimNumber(device.getDeviceData().getSimNumber());
                                    devicesItem.setAlertState(true);
                                    devicesItem.setPowerState(true);
                                    devicesItem.setSpyState(false);
                                    devicesItem.setPassword("123456");
                                    devicesItem.setNewPassword("");
                                    devicesItem.setAdditionalNotes(device.getDeviceData().getAdditionalNotes());
                                    devicesItems.add(devicesItem);
                                }
                                pref.setDevicesConfig(devicesItems);
                                loading.cancel();

                                startActivity(new Intent(context, MainActivity.class));
                                finish();

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else if (response.code() == 400) {
                            Log.e(TAG, "onResponse Code:" + response.code());
                            sync();
//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Log.e("onFailure", "onFailure: " + t.getLocalizedMessage());
                        loading.cancel();
                        loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        loading.setTitleText(getString(R.string.oops));
                        loading.setCancelable(false);
                        loading.setContentText(getString(R.string.no_internet));
                        loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> sync());
                        loading.show();
                    }
                });
    }

    public void checkCarsHasSimNumber() {
        List<DevicesItem> devicesItems = pref.getDevicesConfigs();
        for (DevicesItem device : devicesItems) {
            if (device.getSimNumber().isEmpty()) {
                SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
                dialog.setCancelable(false);
                dialog.setTitleText(getString(R.string.oops));
                dialog.setContentText(getString(R.string.none_sim_number_desc));
                dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                    startActivity(new Intent(context, DevicesActivity.class));
                });
                return;
            }
        }
        startActivity(new Intent(context, MainActivity.class));
    }

}