package com.alfaplus.tracker.activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.AddAlertDialog;
import com.alfaplus.tracker.adapters.AlertsAdapter;
import com.alfaplus.tracker.adapters.OnDevicesSubmit;
import com.alfaplus.tracker.responses.alerts.AlertsItem;
import com.alfaplus.tracker.responses.alerts.AlertsResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class AlertsActivity extends BaseActivity {

    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    @BindView(R.id.recycler_alerts)
    RecyclerView recyclerAlerts;

    List<AlertsItem> alerts = new ArrayList<>();
    AlertsAdapter adapter;
    AddAlertDialog addAlertDialog;
    SweetAlertDialog loading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_alerts);
        ButterKnife.bind(this);

        adapter = new AlertsAdapter(alerts);
        recyclerAlerts.setLayoutManager(new LinearLayoutManager(context));
        recyclerAlerts.setAdapter(adapter);

        adapter.setOnAlertButtonClick((position, buttonId) -> {
            if (buttonId == AlertsAdapter.BUTTON_EDIT) {

                AlertsItem alert = alerts.get(position);
                AddAlertDialog addAlertDialog = new AddAlertDialog(context,
                        alert.getId(),
                        alert.getName(),
                        alert.getType(),
                        alert.getDevices().toArray(new Integer[0]),
                        110);
                addAlertDialog.show();
                addAlertDialog.setOnDevicesSubmit(new OnDevicesSubmit() {
                    @Override
                    public void onSubmit(Integer[] devices_id, int value, String name) {
                        System.err.println(Arrays.toString(devices_id));
                    }
                });


            } else if (buttonId == AlertsAdapter.BUTTON_REMOVE) {
                AlertsItem alert = alerts.get(position);
                SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                dialog.setTitleText(getString(R.string.alert));
                dialog.setContentText(getString(R.string.alert)+" " + alert.getName() + getString(R.string.text_delete));
                dialog.setConfirmButton(getString(R.string.yes), sweetAlertDialog -> {
                    dialog.cancel();
                    removeAlert(alert.getId(), position);
                });
                dialog.show();
            }
        });

        recyclerAlerts.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0)
                    fabAdd.hide();
                else
                    fabAdd.show();
            }
        });

        fabAdd.setOnClickListener(v -> {
            addAlertDialog = new AddAlertDialog(context);
            addAlertDialog.show();
            addAlertDialog.setOnDevicesSubmit(new OnDevicesSubmit() {
                @Override
                public void onSubmit(Integer[] devices_id, int value, String name) {
                    addAlert(name,"overspeed",value,devices_id);
                }
            });
        });
        getAlerts();

    }

    public void getAlerts() {
        SweetAlertDialog loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setContentText(getString(R.string.text_loading));
        loading.show();
        api.getAlerts(pref.getUserApiHash())
                .enqueue(new Callback<AlertsResponse>() {
                    @Override
                    public void onResponse(Call<AlertsResponse> call, Response<AlertsResponse> response) {
                        for (AlertsItem alert : response.body().getItems().getAlerts()) {
                            if (alert.getType().equals("overspeed")) {
                                alerts.add(alert);
                            }
                        }

                        adapter.notifyItemInserted(alerts.size());
                        loading.cancel();
                    }

                    @Override
                    public void onFailure(Call<AlertsResponse> call, Throwable t) {
                        loading.cancel();
                        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        dialog.setCancelable(false);
                        dialog.setTitleText(getString(R.string.oops));
                        dialog.setContentText(getString(R.string.no_internet));
                        dialog.setConfirmButton("OK", sweetAlertDialog -> finish());
                        dialog.show();
                        Log.e(Params.TAG, "onFailure:" + t.getLocalizedMessage());
                    }
                });
    }

    public void addAlert(String name, String type, int value, Integer[] devices) {
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText(getString(R.string.text_loading));
        loading.show();
        BaseActivity.api.addAlert(pref.getUserApiHash(),
                name,
                type,
                value,
                devices).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.cancel();
                loading = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                loading.setTitleText(getString(R.string.nice));
                loading.setContentText(getString(R.string.alert_added_succ));
                loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                    loading.cancel();
                    addAlertDialog.cancel();
                    alerts.clear();
                    getAlerts();
                });
                loading.show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.cancel();
                loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                loading.setTitleText(getString(R.string.oops));
                loading.setContentText(getString(R.string.no_internet));
                loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> loading.cancel());
                loading.show();
            }
        });
    }

    public void removeAlert(int alertId, int position) {
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText(getString(R.string.text_loading));
        loading.show();
        api.removeAlert(pref.getUserApiHash(), alertId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        loading.cancel();
                        loading = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                        loading.setTitleText(getString(R.string.nice));
                        loading.setContentText(getString(R.string.alert_remove_succ));
                        loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                            loading.cancel();
                            alerts.remove(position);
                            adapter.notifyItemRemoved(position);
                        });
                        loading.show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        loading.cancel();
                        loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        loading.setTitleText(getString(R.string.oops));
                        loading.setContentText(getString(R.string.no_internet));
                        loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> loading.cancel());
                        loading.show();
                    }
                });
    }

}