package com.alfaplus.tracker.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.alfaplus.tracker.R;

import java.util.Locale;
import java.util.concurrent.Executor;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends BaseActivity {

    @SuppressLint("NonConstantResourceId")
    @BindView(R.id.image_logo)
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        System.err.println(pref.isFirstTime());

        configLanguage();

        logo.animate().setDuration(500).translationX(0).start();

//        Intent intent = new Intent(getBaseContext(), EventMapActivity.class);
//        intent.putExtra("id", 1);
//        intent.putExtra("deviceName", "device name");
//        intent.putExtra("message", "message");
//        intent.putExtra("speed", 34);
//        intent.putExtra("time", "2022-01-17 17:01:15");
//        intent.putExtra("lat", 0.0);
//        intent.putExtra("lng", 0.0);

//        startActivity(intent);

        System.err.println("Server Address:" + pref.getServer());

        new Handler().postDelayed(() -> {
            if (pref.getUserApiHash().isEmpty() ) {
                pref.clearPrefs();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else if (pref.getUsesBiometric()) {
                checkBiometric(context);
            } else if (!pref.getUserApiHash().isEmpty() ||
                    pref.getDevicesConfigs() != null ||
                    !pref.getAppVersionName().isEmpty()) {
                startActivity(new Intent(context, MainActivity.class));
                finish();
            } else {
                pref.clearPrefs();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

        }, 2000);


    }

    public void checkBiometric(Context context) {
        BiometricManager biometricManager = BiometricManager.from(context);
        if (biometricManager.canAuthenticate() == BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE) {
            Toast.makeText(context, R.string.no_biometric_sensor, Toast.LENGTH_SHORT).show();
        } else if (biometricManager.canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS) {
            Executor executor = ContextCompat.getMainExecutor(context);
            BiometricPrompt prompt = new BiometricPrompt(SplashActivity.this, executor, new BiometricPrompt.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                    super.onAuthenticationError(errorCode, errString);
                    Toast.makeText(context, R.string.biometric_read_error, Toast.LENGTH_SHORT).show();
                    checkBiometric(context);
                }

                @Override
                public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                    super.onAuthenticationSucceeded(result);
                    startActivity(new Intent(context, MainActivity.class));
                    finish();
                }


                @Override
                public void onAuthenticationFailed() {
                    super.onAuthenticationFailed();
                    Toast.makeText(context, R.string.biometric_read_error, Toast.LENGTH_SHORT).show();
                    checkBiometric(context);
                }
            });
            BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle(getString(R.string.app_name))
                    .setSubtitle("Touch Sensor")
                    .setNegativeButtonText("Cancel")
                    .build();
            prompt.authenticate(promptInfo);
        }
    }

    public void configLanguage() {
        Locale locale = new Locale(pref.getLanguage());
        Locale.setDefault(locale);

        Resources resources = getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

}