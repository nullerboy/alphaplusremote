package com.alfaplus.tracker.activities;

import static com.alfaplus.tracker.Params.TAG_ABOUT_FRAGMENT;
import static com.alfaplus.tracker.Params.TAG_LOCATION_FRAGMENT;
import static com.alfaplus.tracker.Params.TAG_REMOTE_FRAGMENT;
import static com.alfaplus.tracker.Params.TAG_SETTINGS_FRAGMENT;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.alfaplus.tracker.MyService;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.fragments.AboutFragment;
import com.alfaplus.tracker.fragments.RemoteFragment;
import com.alfaplus.tracker.fragments.SettingsFragment;
import com.alfaplus.tracker.fragments.TabbedFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("NonConstantResourceId")
public class MainActivity extends BaseActivity implements NavigationBarView.OnItemSelectedListener, NavigationBarView.OnItemReselectedListener {

    //    @BindView(R.id.navigation)
    public static BottomNavigationView navigation;

    @BindView(R.id.text_title)
    TextView textTitle;

    public static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());
        activity=this;
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        navigation = findViewById(R.id.navigation);
        navigation.setOnItemSelectedListener(this);
        navigation.setOnItemReselectedListener(this);
        persian_iran_font(context, navigation);
        pref.setAppVersionName();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_main, new RemoteFragment())
                .commit();

//        checkPermissions();

//        SMSListener.setSmsInterface(new SMSInterface() {
//            @Override
//            public void onReceive(String data) {
//                Toast.makeText(context,data,Toast.LENGTH_SHORT)
//                        .show();
//            }
//        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setContentText(getString(R.string.screen_overlay_desc))
                        .setConfirmButton(getString(R.string.close), sweetAlertDialog1 -> {
                            sweetAlertDialog1.cancel();
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, 0);

                        });
                sweetAlertDialog.show();

            }
        }

        startService(new Intent(context, MyService.class));



    }

    public void showNotification(Context context, String title, String message, Intent intent, int reqCode) {

        PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_ONE_SHOT);
        String CHANNEL_ID = "channel_name";// The id of the channel.
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.alpha)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Channel Name";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(reqCode, notificationBuilder.build()); // 0 is the request code, it should be unique id

        Log.d("showNotification", "showNotification: " + reqCode);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (R.id.navigation_remote == id) {
            changeFragment(new RemoteFragment(), TAG_REMOTE_FRAGMENT);
            textTitle.setText(R.string.nav_remote);

        } else if (R.id.navigation_settings == id) {
            changeFragment(new SettingsFragment(), TAG_SETTINGS_FRAGMENT);
            textTitle.setText(R.string.nav_settings);

        } else if (R.id.navigation_location == id) {
            changeFragment(new TabbedFragment(), TAG_LOCATION_FRAGMENT);
            textTitle.setText(R.string.nav_location);
            checkInternet();

        } else if (R.id.navigation_about == id) {
            changeFragment(new AboutFragment(), TAG_ABOUT_FRAGMENT);
            textTitle.setText(R.string.nav_about_us);

        }

        return true;
    }

    public void changeFragment(Fragment fragment, String TAG) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_main, fragment);
        transaction.addToBackStack(TAG);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void persian_iran_font(Context context, View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    persian_iran_font(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/vazirl.ttf"));
                ((TextView) v).setTextSize(16);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {

    }

    public void checkCarsHasSimNumber() {
        List<DevicesItem> devicesItems = pref.getDevicesConfigs();
        for (DevicesItem device : devicesItems) {
            if (device.getSimNumber().isEmpty()) {
                SweetAlertDialog dialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
                dialog.setCancelable(false);
                dialog.setTitleText(getString(R.string.oops));
                dialog.setContentText(getString(R.string.none_sim_number_desc));
                dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                    dialog.cancel();
                    startActivity(new Intent(context, DevicesActivity.class));
                });
                dialog.show();
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkCarsHasSimNumber();
    }

    public void checkInternet() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if (netInfo == null) {
            SweetAlertDialog loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);

            loading.setTitleText(getString(R.string.oops));
            loading.setContentText(getString(R.string.no_internet));
            loading.setConfirmButton(getString(R.string.ok), sweetAlertDialog -> {
                loading.cancel();
            });
            loading.show();
        }
    }


}