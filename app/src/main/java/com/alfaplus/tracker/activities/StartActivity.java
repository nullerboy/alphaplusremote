package com.alfaplus.tracker.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.alfaplus.tracker.BuildConfig;
import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        Log.e("StartActivity", "onCreate: " );

        Intent intent=new Intent(getApplicationContext(), LoginActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

        //here we call the onDestory instead of finish()
        finish();
//        startActivity(intent);

//        new Handler(Looper.getMainLooper()).postDelayed(() -> {
//            if (pref.getUserApiHash().isEmpty() ||
//                    pref.getDeviceMobile().isEmpty() ||
//                    pref.getDevicesConfigs() == null ||
//                    pref.getAppVersionName().isEmpty() ||
//                    !pref.getAppVersionName().equals(BuildConfig.VERSION_NAME)) {
//                pref.clearPrefs();
//                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
//                finish();
//            } else {
////                startActivity(new Intent(getApplicationContext(), MainActivity.class));
//            }
//            finish();
//        }, 2000);

    }


}