package com.alfaplus.tracker.activities;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.alfaplus.tracker.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint({"NonConstantResourceId", "SetTextI18n", "UseCompatLoadingForDrawables"})
public class EventMapActivity extends BaseActivity {

    Bundle bundle;

    GoogleMap map;
    private boolean isSatView = false;

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.card_detail)
    CardView cardDetail;

    @BindView(R.id.text_device_name)
    TextView textDeviceName;

    @BindView(R.id.text_message)
    TextView textMessage;

    @BindView(R.id.text_speed)
    TextView textSpeed;

    @BindView(R.id.text_time)
    TextView textTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_event_map);
        ButterKnife.bind(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            new Handler().postDelayed(() -> {
                mapView.onCreate(savedInstanceState);
                mapView.onResume();
                mapView.getMapAsync(googleMap -> {
                    map = googleMap;
                    map.getUiSettings().setCompassEnabled(false);
                    map.getUiSettings().setRotateGesturesEnabled(false);
                    map.getUiSettings().setZoomControlsEnabled(false);
                    map.getUiSettings().setMapToolbarEnabled(false);
                    map.getUiSettings().setAllGesturesEnabled(true);

                    bundle = getIntent().getExtras();

                    PersianCalendar calendar = convertToTimestamp(bundle.getString("time"));
                    textDeviceName.setText(bundle.getString("deviceName"));
                    textMessage.setText(bundle.getString("message"));
                    textSpeed.setText((bundle.getInt("speed") + getString(R.string.text_kph)));

                    if (pref.getLanguage().equals("fa")) {
                        textTime.setText(calendar.getPersianYear() + "-" + (calendar.getPersianMonth() + 1) + "-" + calendar.getPersianDay()
                                + " " + calendar.get(PersianCalendar.HOUR_OF_DAY) + ":" + calendar.get(PersianCalendar.MINUTE));
                    } else {
                        textTime.setText(bundle.getString("time"));
                    }

                    cardDetail.setVisibility(View.VISIBLE);

                    LatLng position = new LatLng(bundle.getDouble("lat"), bundle.getDouble("lng"));
                    Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.car_green)).getBitmap();
                    map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory
                                    .fromBitmap(Bitmap.createScaledBitmap(icon,
                                            icon.getWidth() * 2,
                                            icon.getHeight() * 2,
                                            false)))
                            .position(position)
                            .flat(false));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 17));
                });

            }, 1000);
        }


    }

    @OnClick({R.id.button_map_style})
    void onClick(View view) {
        int id = view.getId();

        if (R.id.button_map_style == id) {
            if (map != null) {
                map.setMapType(isSatView ? GoogleMap.MAP_TYPE_NORMAL : GoogleMap.MAP_TYPE_SATELLITE);
                isSatView = !isSatView;
            }

        }
    }

    @SuppressLint("SimpleDateFormat")
    public PersianCalendar convertToTimestamp(String text) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            //            persianCalendar.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
            return new PersianCalendar(dateFormat.parse(text).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}