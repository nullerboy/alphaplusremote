package com.alfaplus.tracker.activities;

import static android.app.PendingIntent.FLAG_MUTABLE;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;

import com.alfaplus.tracker.API;
import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.LangDialog;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseActivity extends AppCompatActivity {


    private static final int PERMISSION_REQUEST = 1001;
    public Context context;
    public SmsManager smsManager;
    public static Pref pref;
    public static Retrofit retrofit;
    public static API api;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        context = BaseActivity.this;
        smsManager = SmsManager.getDefault();
        pref = new Pref(context);

        setServerAddress();

//        if (hasPermission() && !pref.getDeviceMobile().isEmpty())
//            startService(new Intent(context, SmsService.class));


    }

    public static void setServerAddress() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(pref.getAPiServer())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
        api = retrofit.create(API.class);
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    public static void sendSMS(Context context, String phoneNumber, String message) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
                new Intent(SENT), FLAG_MUTABLE);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                new Intent(DELIVERED), FLAG_MUTABLE);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

    }

    public static void showDemoDialog(Context context, Runnable confirmAction) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        dialog.setTitleText(context.getString(R.string.warning));
        dialog.setContentText(context.getString(R.string.demo_desc));
        dialog.setCancelable(false);
        dialog.setCancelButton(context.getString(R.string.cancel), sweetAlertDialog -> dialog.cancel());
        dialog.setConfirmButton(context.getString(R.string.text_enter), sweetAlertDialog -> {
            pref.clearPrefs();
            confirmAction.run();
            dialog.cancel();
        });
        dialog.show();
    }


    public void sendSms(String number, String text) {
        smsManager.sendTextMessage(number, null, text, null, null);
        Log.i(Params.TAG, "sendSms:");
    }

    public void checkPermissions() {
        if (hasPermission()) {
            ActivityCompat.requestPermissions(BaseActivity.this,
                    new String[]{Manifest.permission.SEND_SMS,
                            Manifest.permission.READ_SMS,
                            Manifest.permission.RECEIVE_SMS,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST);
        }
    }

    public boolean hasPermission() {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS)
                == PackageManager.PERMISSION_DENIED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST
                && grantResults[0] == PackageManager.PERMISSION_DENIED) {

            SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
            dialog.setContentText(getString(R.string.permissions_desc));
            dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                checkPermissions();
                dialog.cancel();
            });
            dialog.setCancelable(false);
            dialog.show();
        } else {
            if (pref.isFirstTime()) {
                showSelectLanguageDialog();
            }

        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void initLayoutDirection(String locale) {
        if (locale.equals("fa")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

    public void showSelectLanguageDialog() {
        LangDialog langDialog = new LangDialog(this);
        langDialog.setOnLanguageSelect(language -> {
            if (!language.equals(pref.getLanguage())) {
                pref.setLanguage(language);
                langDialog.cancel();
                startActivity(new Intent(this, SplashActivity.class));
                finish();
            }
            langDialog.cancel();
        });
        langDialog.show();
        pref.setFirstTime(false);
    }

}
