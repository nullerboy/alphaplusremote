package com.alfaplus.tracker.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.AdminsAdapter;
import com.alfaplus.tracker.adapters.DefineAdminDialog;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

@SuppressLint("NonConstantResourceId")
public class AdminsActivity extends BaseActivity {

    @BindView(R.id.recycler_admins)
    RecyclerView recycler;

    @BindView(R.id.fab_add)
    FloatingActionButton fabAdd;

    Bundle bundle;
    AdminsAdapter adminsAdapter;
    List<String> numbers;
    SweetAlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_admins);
        ButterKnife.bind(this);
        dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);

        numbers = new ArrayList<>();
        recycler.setLayoutManager(new LinearLayoutManager(context));

        if (getIntent() != null && getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
            String str = bundle.getString("numbers");
            int deviceId = bundle.getInt("deviceId");
            DevicesItem devicesItem = getDeviceById(deviceId);

            if (!str.isEmpty()) {
                numbers.addAll(Arrays.asList(str.split(",")));
                adminsAdapter = new AdminsAdapter(numbers, pref.getLanguage());
                recycler.setAdapter(adminsAdapter);

                adminsAdapter.setOnClicked(i -> {
                    String number = numbers.get(i);
                    dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    dialog.setTitleText(getString(R.string.alert));
                    dialog.setContentText(getString(R.string.text_admin_ok));
                    dialog.setCancelable(false);
                    dialog.show();

                    setTempAdmin(deviceId, number);
                    BaseActivity.sendSMS(context, devicesItem.getSimNumber(),
                            getString(R.string.cmd_12) + devicesItem.getPassword() + " " + number.replaceFirst("09", "+989"));

                });
            }
            fabAdd.setOnClickListener(v -> {

                DefineAdminDialog defineAdminDialog = new DefineAdminDialog(context);
                defineAdminDialog.setAdminListener((newAdminMobile, isNew) -> {

                    System.err.println(devicesItem.getAdditionalNotes() + " new:" + newAdminMobile);
                    if (devicesItem.getAdditionalNotes().contains(newAdminMobile)) {
                        Toast.makeText(context, R.string.text_admin_exist, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (numbers.size()>=5){
                        Toast.makeText(context, R.string.text_admin_limit, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    newAdminMobile = newAdminMobile.replaceFirst("09", "+989") ;
                    if (isNew) {
                        BaseActivity.sendSMS(context, devicesItem.getSimNumber(),
                                getString(R.string.cmd_11) + devicesItem.getPassword() + " " + newAdminMobile);
                        defineAdminDialog.cancel();
                        setTempAdmin(deviceId, newAdminMobile.replace("+98", "0"));
                        dialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                        dialog.setTitleText(getString(R.string.alert));
                        dialog.setContentText(getString(R.string.text_admin_ok));
                        dialog.setCancelable(false);
                        dialog.show();
                    } else {
//                        BaseActivity.sendSMS(context, devicesItem.getSimNumber(),
//                                getString(R.string.cmd_12) + devicesItem.getPassword() + " " + newAdminMobile);
//                        dialog.cancel();
                    }
                });
                defineAdminDialog.show();

            });
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialog.isShowing()) dialog.cancel();
        finish();
    }

    public DevicesItem getDeviceById(int deviceId) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getId() == deviceId) {
                return device;
            }
        }
        return null;
    }

    public void setTempAdmin(int deviceId, String number) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getId() == deviceId) {
                device.setTempAdmin(number);
                pref.setDevicesConfig(devices);
                return;
            }
        }
    }
}