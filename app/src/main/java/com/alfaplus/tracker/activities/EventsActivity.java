package com.alfaplus.tracker.activities;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.EventAdapter;
import com.alfaplus.tracker.adapters.OnClicked;
import com.alfaplus.tracker.responses.events.DataItem;
import com.alfaplus.tracker.responses.events.EventsResponse;
import com.alfaplus.tracker.responses.events.Items;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class EventsActivity extends BaseActivity {

    @BindView(R.id.recycler_events)
    RecyclerView recyclerEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_events);
        ButterKnife.bind(this);

        recyclerEvents.setLayoutManager(new LinearLayoutManager(context));

        getEvents();
    }

    public void getEvents() {
        SweetAlertDialog loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setContentText(getString(R.string.text_loading));
        loading.show();
        api.getEvents(pref.getUserApiHash()).enqueue(new Callback<EventsResponse>() {
            @Override
            public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                loading.cancel();
                Items items = response.body().getItems();
                List<DataItem> dataItems = items.getData();
                EventAdapter adapter = new EventAdapter(dataItems,pref.getLanguage());
                recyclerEvents.setAdapter(adapter);
                adapter.setOnClicked(new OnClicked() {
                    @Override
                    public void OnClick(int i) {
                        DataItem data = dataItems.get(i);
                        Intent intent = new Intent(EventsActivity.this, EventMapActivity.class);
                        intent.putExtra("id", data.getAlertId());
                        intent.putExtra("deviceName", data.getDeviceName());
                        intent.putExtra("message", data.getMessage());
                        intent.putExtra("speed", data.getSpeed());
                        intent.putExtra("time", data.getTime());
                        intent.putExtra("lat", data.getLatitude());
                        intent.putExtra("lng", data.getLongitude());

                        startActivity(intent);
                        System.err.println("speed:"+ data.getSpeed());
                    }
                });
            }

            @Override
            public void onFailure(Call<EventsResponse> call, Throwable t) {
                loading.cancel();
                SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                dialog.setCancelable(false);
                dialog.setTitleText(getString(R.string.oops));
                dialog.setContentText(getString(R.string.no_internet));
                dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> finish());
                dialog.show();
                Log.e(Params.TAG, "onFailure:" + t.getLocalizedMessage());
            }
        });
    }

}