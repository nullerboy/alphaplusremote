package com.alfaplus.tracker.activities;

import static com.alfaplus.tracker.Params.HISTORY_STOPS_SECONDS;
import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.alfaplus.tracker.Params;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.CoursesDialog;
import com.alfaplus.tracker.adapters.OnHistoryItemClicked;
import com.alfaplus.tracker.responses.history.item.HistoryItem;
import com.alfaplus.tracker.responses.history.item.ItemsItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint({"NonConstantResourceId", "UseCompatLoadingForDrawables", "SetTextI18n", "SimpleDateFormat"})
public class CourseActivity extends BaseActivity {

    Bundle bundle;

    GoogleMap map;
    private boolean isSatView = false;

    @BindView(R.id.map)
    MapView mapView;

    @BindView(R.id.card_detail)
    CardView cardDetail;

    @BindView(R.id.text_time)
    TextView textTime;

    @BindView(R.id.text_distance)
    TextView textDistance;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.text_start)
    TextView textStart;

    @BindView(R.id.text_end)
    TextView textEnd;

    @BindView(R.id.text_speed_avg)
    TextView textSpeedAvg;

    @BindView(R.id.text_speed_top)
    TextView textSpeedTop;

    @BindView(R.id.button_courses)
    Button btnCourses;

    @BindView(R.id.button_direction)
    ImageButton btnDirection;

    List<Marker> markers = new ArrayList<>();
    List<Marker> arrows = new ArrayList<>();
    List<Polyline> polylines = new ArrayList<>();

    LatLng latLngParked;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_course);

        ButterKnife.bind(this);


        if (getIntent() != null && getIntent().getExtras() != null) {

            new Handler().postDelayed(() -> {
                mapView.onCreate(savedInstanceState);
                mapView.onResume();
                mapView.getMapAsync(googleMap -> {
                    map = googleMap;
                    map.getUiSettings().setCompassEnabled(false);
                    map.getUiSettings().setRotateGesturesEnabled(false);
                    map.getUiSettings().setZoomControlsEnabled(false);
                    map.getUiSettings().setMapToolbarEnabled(false);
                    map.getUiSettings().setAllGesturesEnabled(true);
                    map.setTrafficEnabled(false);

                    bundle = getIntent().getExtras();
//                    int history = bundle.getInt("history");
                    int device = bundle.getInt("device");
                    String startDate = bundle.getString("startDate");
                    String endDate = bundle.getString("endDate");
                    String startTime = bundle.getString("startTime");
                    String endTime = bundle.getString("endTime");

                    String deviceName = bundle.getString("name");

                    Log.i("bundleData",
                            startDate + " " + startTime  + " --> " + endDate + " " + endTime + "\n"
                    );

                    textTitle.setText(getString(R.string.car_course) + deviceName);

                    Log.i("CourseActivity", startDate + " " + startTime + " -> "
                            + endDate + " " + endTime);

                    getHistory(device,
                            startDate,
                            startTime,
                            endDate,
                            endTime);

                });

            }, 1000);

        }

    }

    public void getHistory(int deviceId, String startDate, String startTime, String toDate, String toTime) {
        SweetAlertDialog loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setContentText(getString(R.string.text_loading));
        loading.show();
        BaseActivity.api.getHistory(BaseActivity.pref.getUserApiHash(),
                deviceId,
                startDate,
                startTime,
                toDate,
                toTime,
                HISTORY_STOPS_SECONDS)
                .enqueue(new Callback<ResponseBody>() {
                    @SuppressLint({"SetTextI18n", "SimpleDateFormat"})
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        loading.cancel();
                        Log.e(TAG, "getHistory onResponse: " + call.request().url());
                        Polyline polyline = null;
                        if (response.code() == 200) {
                            try {
                                JSONObject resObject = new JSONObject(response.body().string());
                                JSONArray itemsArray = resObject.getJSONArray("items");
                                System.err.println("getHistory:" + resObject.toString());

                                List<HistoryItem> historyItems = new ArrayList<>();
                                for (int i = 0; i < itemsArray.length(); i++) {
                                    JSONObject item = itemsArray.getJSONObject(i);
                                    int status = item.getInt("status");
                                    if (status == 1 || status == 2) {
                                        HistoryItem historyItem = new Gson().fromJson(item.toString(), HistoryItem.class);
//                                        if (historyItem.getDistance() > 0.09 ||
//                                                historyItem.getTopSpeed() > 0 ||
//                                                historyItem.getAverageSpeed() > 0)
                                        historyItems.add(historyItem);
                                    }
                                }

                                if (historyItems.size() > 0) {
                                    drawCourse(historyItems);

                                } else {
                                    SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                                    dialog.setCancelable(false);
                                    dialog.setTitleText(getString(R.string.oops));
                                    dialog.setContentText(getString(R.string.no_courses_found));
                                    dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                                        dialog.cancel();
                                        finish();
                                    });
                                    dialog.show();
                                }


                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Log.e(Params.TAG, "onResponse Code:" + response.code());
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        loading.cancel();
                        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        dialog.setCancelable(false);
                        dialog.setTitleText(getString(R.string.oops));
                        dialog.setContentText(getString(R.string.no_internet));
                        dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> finish());
                        dialog.show();
                        Log.e(Params.TAG, "onFailure:" + t.getLocalizedMessage());
                    }
                });

    }

    Marker startMarker;

    public void drawHistoryItem(HistoryItem history) {
        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        List<ItemsItem> items = history.getItems();
        PolylineOptions polylineOptions = new PolylineOptions()
                .color(Color.parseColor("#101dff"))
                .width(10)
                .geodesic(true);

        for (int i = 0; i < items.size(); i++) {
            ItemsItem itemsItem = items.get(i);
            LatLng point = new LatLng(
                    itemsItem.getLat(),
                    itemsItem.getLng()
            );
            bounds.include(point);
            polylineOptions.add(point);

            if (i > 0 && i != items.size() - 1 && items.size() > 2) {
                ItemsItem itemsItem2 = items.get(i + 1);
                LatLng point2 = new LatLng(
                        itemsItem2.getLat(),
                        itemsItem2.getLng()
                );
                Bitmap arrowRed = ((BitmapDrawable) getResources().getDrawable(R.drawable.arrow_red)).getBitmap();
                if (history.getStatus() != 2)
                    arrows.add(map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory
                                    .fromBitmap(Bitmap.createScaledBitmap(arrowRed,
                                            arrowRed.getWidth() + 16,
                                            arrowRed.getHeight() + 16,
                                            false)))
                            .rotation(getAngle(point, point2))
                            .position(point)
                            .flat(false)));
            }


        }

        Bitmap bitmapA = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_a)).getBitmap();
        Bitmap bitmapB = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_b)).getBitmap();
        Bitmap bitmapP = ((BitmapDrawable) getResources().getDrawable(R.drawable.route_stop)).getBitmap();

        LatLng latLngA = new LatLng(items.get(0).getLat(), items.get(0).getLng());
        LatLng latLngB = new LatLng(items.get(items.size() - 1).getLat(), items.get(items.size() - 1).getLng());
        LatLng latLngP = new LatLng(items.get(0).getLat(), items.get(0).getLng());

        long startTimestamp = convertStringToTimestamp(history.getShow());
        long endTimestamp = convertStringToTimestamp(history.getLeft());


        if (history.getStatus() == 2) {
            markers.add(map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory
                            .fromBitmap(Bitmap.createScaledBitmap(bitmapP,
                                    bitmapP.getWidth() * 3,
                                    bitmapP.getHeight() * 3,
                                    false)))
                    .position(latLngP)
                    .flat(false)));

            if (pref.getLanguage().equals("fa")) {
                PersianCalendar calendarShow = new PersianCalendar(startTimestamp);
                PersianCalendar calendarLeft = new PersianCalendar(endTimestamp);
                textStart.setText(calendarShow.getPersianDay() + " " + calendarShow.getPersianMonthName() + getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(startTimestamp)));
                textEnd.setText(calendarLeft.getPersianDay() + " " + calendarLeft.getPersianMonthName() + getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(endTimestamp)));
            } else {
                Calendar calendarShow = Calendar.getInstance();
                calendarShow.setTimeInMillis(startTimestamp);

                Calendar calendarLeft = Calendar.getInstance();
                calendarLeft.setTimeInMillis(endTimestamp);

                textStart.setText(new SimpleDateFormat("dd MMM HH:mm").format(new Date(startTimestamp)));
                textEnd.setText(new SimpleDateFormat("dd MMM HH:mm").format(new Date(endTimestamp)));
            }

            textDistance.setText(getString(R.string.text_distance) + history.getDistance() + getString(R.string.text_kilometer));
            textTime.setText(getString(R.string.text_duration) + secondsToString(history.getTimeSeconds()));
            textSpeedAvg.setText(R.string.text_parked_car);
            textSpeedTop.setVisibility(View.GONE);
            textDistance.setVisibility(View.GONE);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngP, 18));
            latLngParked = latLngP;
            btnDirection.setVisibility(View.VISIBLE);
        } else {
            markers.add(map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory
                            .fromBitmap(Bitmap.createScaledBitmap(bitmapA,
                                    bitmapA.getWidth(),
                                    bitmapA.getHeight(),
                                    false)))
                    .position(latLngA)
                    .flat(false)));
            markers.add(map.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory
                            .fromBitmap(Bitmap.createScaledBitmap(bitmapB,
                                    bitmapB.getWidth(),
                                    bitmapB.getHeight(),
                                    false)))
                    .position(latLngB)
                    .flat(false)));
            if (pref.getLanguage().equals("fa")) {
                PersianCalendar calendarShow = new PersianCalendar(startTimestamp);
                PersianCalendar calendarLeft = new PersianCalendar(endTimestamp);
                textStart.setText(calendarShow.getPersianDay() + " " + calendarShow.getPersianMonthName() + getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(startTimestamp)));
                textEnd.setText(calendarLeft.getPersianDay() + " " + calendarLeft.getPersianMonthName() + getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(endTimestamp)));
            } else {
                Calendar calendarShow = Calendar.getInstance();
                calendarShow.setTimeInMillis(startTimestamp);

                Calendar calendarLeft = Calendar.getInstance();
                calendarLeft.setTimeInMillis(endTimestamp);

                textStart.setText(new SimpleDateFormat("dd MMM HH:mm").format(new Date(startTimestamp)));
                textEnd.setText(new SimpleDateFormat("dd MMM HH:mm").format(new Date(endTimestamp)));
            }
//            textStart.setText(calendarShow.getPersianDay() + " " + calendarShow.getPersianMonthName() + getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(startTimestamp)));
//            textEnd.setText(calendarLeft.getPersianDay() + " " + calendarLeft.getPersianMonthName() + getString(R.string.text_hour) + new SimpleDateFormat("HH:mm").format(new Date(endTimestamp)));
            textDistance.setText(getString(R.string.text_distance) + history.getDistance() + getString(R.string.text_kilometer));
            textTime.setText(getString(R.string.text_duration) + secondsToString(history.getTimeSeconds()));
            textSpeedAvg.setText(getString(R.string.text_avg_speed) + history.getAverageSpeed() + getString(R.string.text_kph));
            textSpeedTop.setText(getString(R.string.text_max_speed) + history.getTopSpeed() + getString(R.string.text_kph));
            textSpeedTop.setVisibility(View.VISIBLE);
            textDistance.setVisibility(View.VISIBLE);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 48));
            polylines.add(map.addPolyline(polylineOptions));
            btnDirection.setVisibility(View.GONE);
        }


        cardDetail.setVisibility(View.VISIBLE);
    }

    public void drawCourse(List<HistoryItem> historyItems) {

        LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        PolylineOptions polylineOptions = new PolylineOptions()
                .color(Color.parseColor("#101dff"))
                .width(10)
                .geodesic(true);

        for (HistoryItem historyItem : historyItems) {
            List<ItemsItem> items = historyItem.getItems();

            for (int i = 0; i < items.size(); i++) {
                ItemsItem itemsItem = items.get(i);
                LatLng point = new LatLng(
                        itemsItem.getLat(),
                        itemsItem.getLng()
                );
                bounds.include(point);
                polylineOptions.add(point);

                if (i > 0 && i != items.size() - 1 && items.size() > 2) {
                    ItemsItem itemsItem2 = items.get(i + 1);
                    LatLng point2 = new LatLng(
                            itemsItem2.getLat(),
                            itemsItem2.getLng()
                    );
                    Bitmap arrowRed = ((BitmapDrawable) getResources().getDrawable(R.drawable.arrow_red)).getBitmap();
                    arrows.add(map.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory
                                    .fromBitmap(Bitmap.createScaledBitmap(arrowRed,
                                            arrowRed.getWidth() + 16,
                                            arrowRed.getHeight() + 16,
                                            false)))
                            .rotation(getAngle(point, point2))
                            .position(point)
                            .flat(false)));
                }


            }

            Bitmap bitmapA = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_a)).getBitmap();
            Bitmap bitmapB = ((BitmapDrawable) getResources().getDrawable(R.drawable.pin_b)).getBitmap();
            Bitmap bitmapP = ((BitmapDrawable) getResources().getDrawable(R.drawable.route_stop)).getBitmap();

            LatLng latLngA = new LatLng(items.get(0).getLat(), items.get(0).getLng());
            LatLng latLngB = new LatLng(items.get(items.size() - 1).getLat(), items.get(items.size() - 1).getLng());
            LatLng latLngP = new LatLng(items.get(0).getLat(), items.get(0).getLng());


            if (historyItem.getStatus() == 2)
                markers.add(map.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory
                                .fromBitmap(Bitmap.createScaledBitmap(bitmapP,
                                        bitmapP.getWidth() * 3,
                                        bitmapP.getHeight() * 3,
                                        false)))
                        .position(latLngP)
                        .flat(false)));


            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), 48));

        }
        polylines.add(map.addPolyline(polylineOptions));

//        map.setOnMarkerClickListener(marker -> {
//            Log.e(TAG, "onMarkerClick: " + marker.getId());
//            for (int i = 0; i < markers.size(); i++) {
//                Marker m = markers.get(i);
//
//                if (m.getId().equals(marker.getId())) {
//                    HistoryItem history = historyItems.get(i);
////                                                    Polyline polyline = polylines.get(i);
//
////                                                    for (Polyline p : polylines) {
////                                                        p.setColor(Color.parseColor("#2ea8d1"));
////                                                        if (p == polyline)
////                                                            polyline.setColor(Color.MAGENTA);
////                                                    }
//
//                    Bitmap bitmapGreen = ((BitmapDrawable) getResources().getDrawable(R.drawable.car_green)).getBitmap();
//                    LatLng latLng = new LatLng(history.getItems().get(history.getItems().size() - 1).getLat(), history.getItems().get(history.getItems().size() - 1).getLng());
//
//                    if (startMarker != null) {
//                        startMarker.remove();
//                    }
//                    startMarker = map.addMarker(new MarkerOptions()
//                            .icon(BitmapDescriptorFactory
//                                    .fromBitmap(Bitmap.createScaledBitmap(bitmapGreen,
//                                            bitmapGreen.getWidth() * 2,
//                                            bitmapGreen.getHeight() * 2,
//                                            false)))
//                            .position(latLng)
//                            .flat(false));
//
//                    Log.e(TAG, "marker.getId: " + m.getId());
//                    Log.e(TAG, "onMarkerClick: " + history.getDistance());
//
//                    long startTimestamp = convertStringToTimestamp(history.getShow());
//                    long endTimestamp = convertStringToTimestamp(history.getLeft());
//                    PersianCalendar calendarShow = new PersianCalendar(startTimestamp);
//                    PersianCalendar calendarLeft = new PersianCalendar(endTimestamp);
//                    textStart.setText(calendarShow.getPersianDay() + " " + calendarShow.getPersianMonthName() + " ساعت" + new SimpleDateFormat("HH:mm").format(new Date(startTimestamp)));
//                    textEnd.setText(calendarLeft.getPersianDay() + " " + calendarLeft.getPersianMonthName() + " ساعت" + new SimpleDateFormat("HH:mm").format(new Date(endTimestamp)));
//                    textDistance.setText("📏 مسافت: " + history.getDistance() + "کیلومتر");
//                    textTime.setText("🕒 مدت: " + history.getTime());
//                    textSpeedAvg.setText("متوسط سرعت:\n" + history.getAverageSpeed() + "کیلومتربرساعت");
//                    textSpeedTop.setText("بیشینه سرعت:\n" + history.getTopSpeed() + "کیلومتربرساعت");
//                    cardDetail.setVisibility(View.VISIBLE);
//
//                }
//            }
//            return false;
//        });

        btnCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CoursesDialog coursesDialog = new CoursesDialog(context, historyItems, pref.getLanguage());
                coursesDialog.show();
                coursesDialog.setOnHistoryItemClicked(new OnHistoryItemClicked() {
                    @Override
                    public void onHistoryClick(int index, List<ItemsItem> items) {
                        for (Polyline po : polylines)
                            po.remove();

                        for (Marker ma : markers)
                            ma.remove();

                        for (Marker arr : arrows)
                            arr.remove();


                        coursesDialog.cancel();
                        drawHistoryItem(historyItems.get(index));
                    }
                });
            }
        });
    }


    @SuppressLint("SimpleDateFormat")
    private static long convertStringToTimestamp(String something) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Timestamp timestamp = null;
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(something);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return timestamp.getTime();
    }

    public float getAngle(LatLng p1, LatLng p2) {
        float angle = (float) Math.toDegrees(Math.atan2(p2.longitude - p1.longitude, p2.latitude - p1.latitude));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    @OnClick({R.id.button_map_style,
            R.id.button_direction})
    void onClick(View view) {
        int id = view.getId();

        if (R.id.button_map_style == id) {
            if (map != null) {
                map.setMapType(isSatView ? GoogleMap.MAP_TYPE_NORMAL : GoogleMap.MAP_TYPE_SATELLITE);
                isSatView = !isSatView;
            }
        } else if (R.id.button_direction == id) {
            startDirectionActivity(latLngParked.latitude, latLngParked.longitude);
        }
    }

    public void startDirectionActivity(double lat, double lng) {
        String uri = "http://maps.google.com/maps?f=d&hl=en&daddr=" + lat + "," + lng;
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(Intent.createChooser(intent, getString(R.string.select_direction_app)));
    }

    public String secondsToString(int totalSecs) {
        int hours = totalSecs / 3600;
        int minutes = (totalSecs % 3600) / 60;
        int seconds = totalSecs % 60;
        return hours + "H " + minutes + "m " + seconds + "s";
    }
}