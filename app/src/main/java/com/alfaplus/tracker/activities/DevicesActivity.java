package com.alfaplus.tracker.activities;

import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.DeviceEditDialog;
import com.alfaplus.tracker.adapters.DevicesAdapter;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.responses.devices.DevicesResponse;
import com.alfaplus.tracker.responses.devices.ItemsItem;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class DevicesActivity extends BaseActivity {

    @BindView(R.id.recycler_devices)
    RecyclerView recyclerDevices;

    private DevicesAdapter adapter;
    private List<ItemsItem> devices = new ArrayList<>();
    List<DevicesItem> devicesItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayoutDirection(pref.getLanguage());

        setContentView(R.layout.activity_devices);
        ButterKnife.bind(this);

        adapter = new DevicesAdapter(devices, pref.getLanguage());
        recyclerDevices.setLayoutManager(new LinearLayoutManager(context));
        recyclerDevices.setAdapter(adapter);

        adapter.setOnClicked(i -> {
            ItemsItem device = devices.get(i);
            DeviceEditDialog dialog = new DeviceEditDialog(context,
                    device.getId(),
                    device.getName(),
                    device.getDeviceData().getSimNumber());
            dialog.show();
            dialog.setOnSubmit(this::editDevice);
        });

        getDevices();
    }

    public void getDevices() {
        SweetAlertDialog loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setContentText(getString(R.string.text_loading));
        loading.show();
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @SuppressLint("NotifyDataSetChanged")
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                devices.addAll(devicesResponse.getItems());
                                adapter.notifyItemInserted(devices.size());
                                loading.cancel();
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.e(TAG, "onResponse Code:" + response.code());
//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("onFailure", "onFailure: " + t.getLocalizedMessage());
                        loading.cancel();
                        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        dialog.setCancelable(false);
                        dialog.setTitleText(getString(R.string.oops));
                        dialog.setContentText(getString(R.string.no_internet));
                        dialog.setConfirmButton(getString(R.string.close), sweetAlertDialog -> finish());
                        dialog.show();
                    }
                });
    }

    SweetAlertDialog loading;

    public void editDevice(int deviceId, String name, String simNumber) {
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setContentText(getString(R.string.text_loading));
        loading.show();
        api.editDevice(pref.getUserApiHash(), deviceId, 1, name, simNumber)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        loading.cancel();
                        if (response.code() == 200) {
                            loading = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                            loading.setTitleText(getString(R.string.nice));
                            loading.setContentText(getString(R.string.text_changes_succ));
                            loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                                loading.cancel();
                                sync();
                            });
                            loading.show();
                        } else {
                            loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                            loading.setTitleText(getString(R.string.oops));
                            loading.setContentText(getString(R.string.text_changing_fail));
                            loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> {
                                loading.cancel();
                            });
                            loading.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        loading.setCancelable(false);
                        loading.setTitleText(getString(R.string.oops));
                        loading.setContentText(getString(R.string.no_internet));
                        loading.setConfirmButton(getString(R.string.close), sweetAlertDialog -> finish());
                        loading.show();
                    }
                });
    }

    public void sync() {
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText(getString(R.string.text_syncing));
        loading.show();
        BaseActivity.api.getDevices(BaseActivity.pref.getUserApiHash())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.code() == 200) {

                            try {
                                String res = response.body().string();
                                System.err.println(res);
                                JSONArray jsonArray = new JSONArray(res);
                                JSONObject jsonObject = jsonArray.getJSONObject(0);
                                DevicesResponse devicesResponse = new Gson().fromJson(jsonObject.toString(), DevicesResponse.class);
                                List<ItemsItem> devices = devicesResponse.getItems();

                                devicesItems.clear();

                                for (ItemsItem device : devices) {
                                    DevicesItem devicesItem = new DevicesItem();
                                    Log.e(TAG, "devicesItem: " + device.getName());

                                    devicesItem.setId(device.getId());
                                    devicesItem.setName(device.getName());
                                    devicesItem.setSimNumber(device.getDeviceData().getSimNumber());
                                    devicesItem.setAlertState(true);
                                    devicesItem.setPowerState(true);
                                    devicesItem.setSpyState(false);
                                    devicesItem.setPassword("123456");
                                    devicesItem.setNewPassword("");
                                    devicesItems.add(devicesItem);
                                }
//                                setDevices(devices);

                                pref.setDevicesConfig(devicesItems);
                                loading.cancel();
                                loading = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                                loading.setTitleText(getString(R.string.nice));
                                loading.setContentText(getString(R.string.text_sync_succ));
                                loading.setConfirmButton("تایید", new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        startActivity(new Intent(context, SplashActivity.class));

                                    }
                                });
                                loading.show();

                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Log.e(TAG, "onResponse Code:" + response.code());

//                            Toast.makeText(getActivity(), "خطای درخواست زیاد,مدتی بعد تلاش کنید", Toast.LENGTH_LONG)
//                                    .show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("onFailure", "onFailure: " + t.getLocalizedMessage());
                        loading.cancel();
                        loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        loading.setTitleText(getString(R.string.oops));
                        loading.setCancelable(false);
                        loading.setContentText(getString(R.string.no_internet));
                        loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> sync());
                        loading.show();
                    }
                });
    }

}