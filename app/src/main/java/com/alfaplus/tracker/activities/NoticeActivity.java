package com.alfaplus.tracker.activities;

import static com.alfaplus.tracker.Params.TAG;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.alfaplus.tracker.Pref;
import com.alfaplus.tracker.R;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class NoticeActivity extends BaseActivity {

    Pref pref;
    SweetAlertDialog loading;

    @BindView(R.id.text_notice)
    TextView textNotice;

    @BindView(R.id.text_car)
    TextView textCar;

    Bundle bundle;

    MediaPlayer mediaNotice;
    MediaPlayer mediaAlert;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_notice);

        ButterKnife.bind(this);

        pref = new Pref(context);
        mediaNotice = MediaPlayer.create(context, R.raw.notice);
        mediaAlert = MediaPlayer.create(context, R.raw.alert);

        mediaAlert.setWakeMode(this.getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

//        configLanguage();

        if (getIntent() != null && getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
            String message = bundle.getString("message").toLowerCase();
            String number = bundle.getString("number").toLowerCase();

            DevicesItem device = getDeviceByMobile(number);
            textCar.setText(getString(R.string.device_name) + device.getName());

//            List<ItemsItem> devices = pref.getDevices();

//            for (ItemsItem device : devices) {
//                if (device.getDeviceData().getSimNumber().equals(number)) {
//                    textCar.setText("نام متحرک: " + device.getName());
//                }
//            }

//            noticeInterface.onNotice(message);

            if (message.contains("tracker is activated")) {
                startNotice();
                textNotice.setText(R.string.tracker_activated);
            } else if (message.contains("set up fail! pls close the door")) {
                startNotice();
                textNotice.setText(R.string.plz_close_door);
            } else if (message.contains("set up fail! pls turn off acc")) {
                startNotice();
                textNotice.setText(R.string.turn_off_acc);
            } else if (message.contains("tracker is deactivated")) {
                startNotice();
                textNotice.setText(R.string.tracker_deactivated);
            } else if (message.contains("stop engine succeed")) {
                setDevicePowerState(number, false);
//                if (RemoteFragment.isActive) {
//                    RemoteFragment.imageRemote.setImageResource(R.drawable.remote_off);
//                }
                startNotice();
                textNotice.setText(R.string.car_off_succ);
            } else if (message.contains("it will be executed after speed less than 20km/h")) {
                startNotice();
                textNotice.setText(R.string.cmd_runs_speed_low);
            } else if (message.contains("resume engine succeed")) {
                setDevicePowerState(number, true);
//                if (RemoteFragment.isActive) {
//                    RemoteFragment.imageRemote.setImageResource(R.drawable.remote_on);
//                }

                startNotice();
                textNotice.setText(R.string.car_on_succ);
            } else if (message.contains("noadmin ok")) {
                startNotice();
                textNotice.setText(R.string.admin_cleared);
                removeDeviceAdmin(number);
            } else if (message.contains("admin ok")) {
                startNotice();
                textNotice.setText(R.string.number_added);
                addDeviceAdmin(number);
            } else if (message.contains("this number has been authorized")) {
                startNotice();
                textNotice.setText(R.string.duplicat_number);
                addDeviceAdmin(number);
            } else if (message.contains("that authorized phone number is not setup in the tracker")) {
                startNotice();
                textNotice.setText(R.string.unauthorized_number);
                removeDeviceAdmin(number);
            } else if (message.contains("sensitivity ok")) {
                startNotice();
                textNotice.setText(R.string.sensativity_succ);
            } else if (message.contains("help me ok") || message.contains("help ok")) {
                startNotice();
                textNotice.setText(R.string.help_me_succ);
            } else if (message.contains("password ok")) {
                setPassword(device.getSimNumber(), device.getNewPassword());
                startNotice();
                textNotice.setText(R.string.pass_change_succ);
            } else if (message.contains("pwd fail")) {
                startNotice();
                textNotice.setText(R.string.password_incorrect);
            } else if (message.contains("monitor ok")) {
                setDeviceSpyState(number, true);
//                if (SettingsFragment.isActive) {
//                    SettingsFragment.fabCall.show();
//                    SettingsFragment.aSwitchSpy.setChecked(true);
//                }

                startNotice();

                textNotice.setText(R.string.mic_activated);
            } else if (message.contains("tracker ok")) {
                setDeviceSpyState(number, false);
//                if (SettingsFragment.isActive) {
//                    SettingsFragment.fabCall.hide();
//                    SettingsFragment.aSwitchSpy.setChecked(false);
//                }

                startNotice();

                textNotice.setText(R.string.spy_deactivated);
            }

            if (message.contains("power") ||
                    message.contains("bat") &&
                            message.contains("gprs") &&
                            message.contains("gps")) {
                startNotice();

                String[] formattedMessage = message.split("\n");
                List<String> listMessage = new ArrayList<String>();
                listMessage.addAll(Arrays.asList(formattedMessage));

                StringBuilder stringBuilder = new StringBuilder();
                for (String str : translateCheckList(listMessage)) {
                    stringBuilder.append(str).append("\n");
                }

                textNotice.setText(stringBuilder.toString());

            }
            if (message.contains("sensor alarm")) {
                startAlert();

                textNotice.setText(R.string.car_sensor_alarm);
                textNotice.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            if (message.contains("door alarm")) {
                startAlert();

                textNotice.setText(R.string.car_door_alarm);
                textNotice.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            if (message.contains("acc alarm")) {
                startAlert();

                textNotice.setText(R.string.car_acc_alarm);
                textNotice.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            if (message.contains("power alarm")) {
                startAlert();

                textNotice.setText(R.string.car_battery_alarm);
                textNotice.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            if (message.contains("low battery")) {
                startAlert();

                textNotice.setText(R.string.tracker_battery_low);
                textNotice.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            }
            if (message.contains("help me!")) {
//                StringBuilder stringBuilder = new StringBuilder();
//                String[] formattedMessage = message.split("\n");
//
//                stringBuilder.append(formattedMessage[0].replace("help me!", "")).append("\n");
//                stringBuilder.append(formattedMessage[1].replace("lat", "طول")).append("\n");
//                stringBuilder.append(formattedMessage[2].replace("long", "عرض")).append("\n");
//                stringBuilder.append(formattedMessage[3].replace("speed", "سرعت")).append("\n");
//                stringBuilder.append(formattedMessage[4].replace("t", "زمان")).append("\n");
//                stringBuilder.append("برای مشاهده روی نقشه لمس کنید").append("\n");
//                textNotice.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(formattedMessage[5])));
//                    }
//                });
                textNotice.setText(R.string.btn_emergency);
                textNotice.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                startNotice();

            }


        } else {
            Toast.makeText(context, getString(R.string.error_ecured), Toast.LENGTH_SHORT)
                    .show();
            finish();
        }
    }

    public void startNotice() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mediaNotice.start();
            }
        }, 500);

    }

    public void startAlert() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mediaAlert.setLooping(true);
                mediaAlert.start();
            }
        }, 500);
    }

    @OnClick({
            R.id.button_ok,
            R.id.button_cancel
    })
    void click(View v) {

        int id = v.getId();
        if (R.id.button_ok == id) {
            if (mediaNotice.isPlaying()) mediaNotice.stop();
            if (mediaAlert.isPlaying()) mediaAlert.stop();
            startActivity(new Intent(context, SplashActivity.class));
            finish();
        } else if (R.id.button_cancel == id) {
            if (mediaNotice.isPlaying()) mediaNotice.stop();
            if (mediaAlert.isPlaying()) mediaAlert.stop();
            finish();
        }

    }

    public List<String> translateCheckList(List<String> checkList) {
        List<String> translatedCheckList = new ArrayList<>();
        for (String s : checkList) {
            if (s.contains("power"))
                translatedCheckList.add(s.replace("power", context.getString(R.string.car_electricity)).replace("on", getString(R.string.connect)).replace("off", getString(R.string.disconnect)));
            if (s.contains("bat"))
                translatedCheckList.add(s.replace("bat", getString(R.string.car_battery)).replace("on", getString(R.string.connect)).replace("off", getString(R.string.disconnect)));
            if (s.contains("gprs"))
                translatedCheckList.add(s.replace("gprs", getString(R.string.internet)).replace("on", getString(R.string.connect)).replace("off", getString(R.string.disconnect)));
            if (s.contains("gps"))
                translatedCheckList.add(s.replace("gps", getString(R.string.gps)).replace("on", getString(R.string.connect)).replace("off", getString(R.string.disconnect)));
            if (s.contains("acc"))
                translatedCheckList.add(s.replace("acc", getString(R.string.car_acc)).replace("on", getString(R.string.open)).replace("off", getString(R.string.closed)));
            if (s.contains("door"))
                translatedCheckList.add(s.replace("door", getString(R.string.car_door)).replace("on", getString(R.string.open)).replace("off", getString(R.string.closed)));
            if (s.contains("arm"))
                translatedCheckList.add(s.replace("arm", getString(R.string.tracker_alarm)).replace("on", getString(R.string.active)).replace("off", getString(R.string.deactive)));
            if (s.contains("shock"))
                translatedCheckList.add(s.replace("shock", getString(R.string.car_sensor)).replace("on", getString(R.string.active)).replace("off", getString(R.string.deactive)));
//            if (s.contains("speed"))
//                translatedCheckList.add(s.replace("speed", "سرعت"));
//            if (s.contains("lat"))
//                translatedCheckList.add(s.replace("lat", "طول"));
//            if (s.contains("long"))
//                translatedCheckList.add(s.replace("long", "عرض"));
//            if (s.contains("T:"))
//                translatedCheckList.add(s.replace("t:", "زمان: "));

        }
        return translatedCheckList;
    }

    @Override
    protected void onPause() {
        if (mediaNotice.isPlaying()) mediaNotice.stop();
        if (mediaAlert.isPlaying()) mediaAlert.stop();
        super.onPause();
    }

    public interface NoticeInterface {
        void onNotice(String msg);
    }

    public DevicesItem getDeviceByMobile(String mobile) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                return device;
            }
        }
        return null;
    }

    public void setDevicePowerState(String mobile, boolean state) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                device.setPowerState(state);
                pref.setDevicesConfig(devices);
                return;
            }
        }
    }

    public void setDeviceSpyState(String mobile, boolean state) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                device.setSpyState(state);
                pref.setDevicesConfig(devices);
                return;
            }
        }
    }

    public void setPassword(String mobile, String password) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                device.setPassword(password);
                pref.setDevicesConfig(devices);
                return;
            }
        }
    }

    public void removeDeviceAdmin(String mobile) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                if (!device.getAdditionalNotes().isEmpty()) {
                    List<String> numbers = new ArrayList<>(Arrays.asList(device.getAdditionalNotes().split(",")));
                    Log.e(TAG, "numbers: " + numbers + " temp:" + device.getTempAdmin() + " id:" + device.getId());
                    numbers.remove(device.getTempAdmin());
                    Log.e(TAG, "numbers: " + numbers);

                    StringBuilder stringBuilder = new StringBuilder();
                    for (String n : numbers) {
                        stringBuilder.append(n).append(",");
                    }

                    if (stringBuilder.length() > 0) {
                        String strNumbers = stringBuilder.substring(0, stringBuilder.length() - 1);
                        device.setAdditionalNotes(strNumbers);
                    } else {
                        device.setAdditionalNotes("");
                    }
                    pref.setDevicesConfig(devices);
                    updateDevice(device.getId(),
                            device.getName(),
                            device.getAdditionalNotes());
                }
                return;
            }
        }


    }

    public void addDeviceAdmin(String mobile) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                if (device.getAdditionalNotes().isEmpty()) {
                    List<String> numbers = new ArrayList<>();
                    Log.e(TAG, "numbers: " + numbers + " temp:" + device.getTempAdmin() + " id:" + device.getId());
                    numbers.add(device.getTempAdmin());
                    Log.e(TAG, "numbers: " + numbers);

                    StringBuilder stringBuilder = new StringBuilder();
                    for (String n : numbers) {
                        stringBuilder.append(n).append(",");
                    }

                    if (stringBuilder.length() > 0) {
                        String strNumbers = stringBuilder.substring(0, stringBuilder.length() - 1);
                        device.setAdditionalNotes(strNumbers);
                    } else {
                        device.setAdditionalNotes("");
                    }
                    pref.setDevicesConfig(devices);
                    updateDevice(device.getId(),
                            device.getName(),
                            device.getAdditionalNotes());

                } else {
                    List<String> numbers = new ArrayList<>(Arrays.asList(device.getAdditionalNotes().split(",")));
                    Log.e(TAG, "numbers: " + numbers + " temp:" + device.getTempAdmin() + " id:" + device.getId());
                    numbers.add(device.getTempAdmin());
                    Log.e(TAG, "numbers: " + numbers);

                    StringBuilder stringBuilder = new StringBuilder();
                    for (String n : numbers) {
                        stringBuilder.append(n).append(",");
                    }

                    if (stringBuilder.length() > 0) {
                        String strNumbers = stringBuilder.substring(0, stringBuilder.length() - 1);
                        device.setAdditionalNotes(strNumbers);
                    } else {
                        device.setAdditionalNotes("");
                    }
                    pref.setDevicesConfig(devices);
                    updateDevice(device.getId(),
                            device.getName(),
                            device.getAdditionalNotes());
                }
                return;
            }
        }
    }


    public void updateDevice(int deviceId, String name, String numbers) {
        loading = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        loading.setTitleText(getString(R.string.text_loading));
        loading.setCancelable(false);
        loading.show();

        api.editDeviceNote(pref.getUserApiHash(),
                deviceId,
                1,
                name,
                numbers).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    loading = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    loading.setTitleText(getString(R.string.nice));
                    loading.setContentText(getString(R.string.operation_succ));
                    loading.setCancelable(false);
                    loading.setConfirmButton(getString(R.string.ok), sweetAlertDialog -> {
                        loading.cancel();
                        finish();
                    });
                    loading.show();
                } else {
                    loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    loading.setTitleText(getString(R.string.oops));
                    loading.setContentText(getString(R.string.error_contact_support));
                    loading.setCancelable(false);
                    loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> {
                        loading.cancel();
                        updateDevice(deviceId, name, numbers);
                    });
                    loading.show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                loading.setTitleText(getString(R.string.oops));
                loading.setContentText(getString(R.string.no_internet));
                loading.setCancelable(false);
                loading.setConfirmButton(getString(R.string.try_again), sweetAlertDialog -> {
                    loading.cancel();
                    updateDevice(deviceId, name, numbers);
                });
                loading.show();
            }
        });
    }

    public void configLanguage(){
        Locale locale=new Locale(pref.getLanguage());
        Locale.setDefault(locale);

        Resources resources=getResources();
        Configuration configuration=resources.getConfiguration();
        configuration.locale=locale;
        resources.updateConfiguration(configuration,resources.getDisplayMetrics());
    }
}