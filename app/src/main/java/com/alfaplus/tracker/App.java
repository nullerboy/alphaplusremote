package com.alfaplus.tracker;

import android.app.Application;

import androidx.annotation.NonNull;

import com.alfaplus.tracker.responses.ResponseDelays;
import com.google.gson.Gson;

import java.util.Map;

import co.pushe.plus.Pushe;
import co.pushe.plus.notification.NotificationButtonData;
import co.pushe.plus.notification.NotificationData;
import co.pushe.plus.notification.PusheNotification;
import co.pushe.plus.notification.PusheNotificationListener;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Pref pref = new Pref(getApplicationContext());
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/vazirl.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        PusheNotification notification = Pushe.getPusheService(PusheNotification.class);
        notification.setNotificationListener(new PusheNotificationListener() {
            @Override
            public void onNotification(@NonNull NotificationData notificationData) {

            }

            @Override
            public void onCustomContentNotification(@NonNull Map<String, Object> map) {
                ResponseDelays delays = new Gson().fromJson(map.toString(), ResponseDelays.class);
                System.err.println("getEventRefreshDelay:" + delays.getEventRefreshDelay());
                System.err.println("getMapRefreshDelay:" + delays.getMapRefreshDelay());

                pref.setMapRefreshInterval(delays.getMapRefreshDelay());
                pref.setEventRefreshInterval(delays.getEventRefreshDelay());

            }

            @Override
            public void onNotificationClick(@NonNull NotificationData notificationData) {

            }

            @Override
            public void onNotificationDismiss(@NonNull NotificationData notificationData) {

            }

            @Override
            public void onNotificationButtonClick(@NonNull NotificationButtonData notificationButtonData, @NonNull NotificationData notificationData) {

            }
        });

    }
}
