package com.alfaplus.tracker;

import static com.alfaplus.tracker.Params.TAG;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.alfaplus.tracker.activities.EventMapActivity;
import com.alfaplus.tracker.activities.MainActivity;
import com.alfaplus.tracker.activities.NoticeActivity;
import com.alfaplus.tracker.adapters.DevicesConfig.DevicesItem;
import com.alfaplus.tracker.responses.events.DataItem;
import com.alfaplus.tracker.responses.events.EventsResponse;
import com.alfaplus.tracker.responses.events.Items;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyService extends Service {

    private Pref pref;
    public SMSListener smsListener;
    public SMSInterface smsInterface;
    public static boolean isRunning = false;
    private smsReceiver receiver;
    List<DevicesItem> devices;
    private API api;
    private Retrofit retrofit;
    String SERVICE_CHANNEL_ID = "101010";
    int ALERT_CHANNEL_ID = 101011;
    int channelIndex = 2;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
//        BwlLog.begin(TAG);
        super.onCreate();
        pref = new Pref(getBaseContext());

        retrofit = new Retrofit.Builder()
                .baseUrl(pref.getAPiServer())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setLenient()
                        .create()))
                .build();
        api = retrofit.create(API.class);
        smsListener = new SMSListener();
        registerReceiver(smsListener, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!pref.getUserApiHash().isEmpty()) getEvents();
            }
        }, 2000, pref.getEventDelay());
//        if (!pref.getUserApiHash().isEmpty()) getEvents();


    }


    @Override
    public void onDestroy() {
        unregisterReceiver(smsListener);
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.S)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, PendingIntent.FLAG_MUTABLE);
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getBaseContext().getPackageName() + "/" + R.raw.notification);  //Here is FILE_NAME is the name of file that you want to play
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();

        Notification notification = new NotificationCompat.Builder(this, "101010")
                .setContentTitle(getString(R.string.app_name))
                .setSmallIcon(R.drawable.alpha)
                .setContentIntent(pendingIntent)
                .setSound(sound)
                .build();
        smsListener.setSmsInterface((number, message) -> {

            devices = pref.getDevicesConfigs();

            for (DevicesItem device : devices) {
                if (number.equals(device.getSimNumber())) {
                    Log.e("onReceive", number + "," + message);

                    if (message.contains("alarm") || message.contains("battery")) {
                        if (device.getAlertState()) {
                            Intent intentNotice = new Intent(getBaseContext(), NoticeActivity.class);
                            intentNotice.putExtra("message", message);
                            intentNotice.putExtra("number", number);
                            intentNotice.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentNotice);
                        } else if (message.contains("stop engine succeed")) {
                            setDevicePowerState(number, false);
                        } else if (message.contains("resume engine succeed")) {
                            setDevicePowerState(number, true);
                        }
                    } else {
                        Intent intentNotice = new Intent(getBaseContext(), NoticeActivity.class);
                        intentNotice.putExtra("message", message);
                        intentNotice.putExtra("number", number);
                        intentNotice.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intentNotice);
                    }


                }
            }


        });

        startForeground(1, notification);

//        showNotification(getBaseContext(),"تست صدا","",new Intent(),29393);
        return START_REDELIVER_INTENT;
    }


    private void createNotificationChannel() {
        SERVICE_CHANNEL_ID = String.valueOf(System.currentTimeMillis() / 1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    "101010",
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * This class used to monitor SMS
     */

    public void setSmsInterface(SMSInterface smsInterface) {
        this.smsInterface = smsInterface;
    }

    public void setReceiver(smsReceiver receiver) {
        this.receiver = receiver;
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static void setIsRunning(boolean isRunning) {
        MyService.isRunning = isRunning;
    }

    public interface smsReceiver {
        void onReceive(String number, String message);
    }

    public DevicesItem getDeviceByMobile(String mobile) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                return device;
            }
        }
        return null;
    }

    public void setDevicePowerState(String mobile, boolean state) {
        List<DevicesItem> devices = pref.getDevicesConfigs();
        for (DevicesItem device : devices) {
            if (device.getSimNumber().equals(mobile)) {
                device.setPowerState(state);
                pref.setDevicesConfig(devices);
                return;
            }
        }
    }

    public void getEvents() {
        api.getEvents(pref.getUserApiHash())
                .enqueue(new Callback<EventsResponse>() {
                    @Override
                    public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                        if (response.code() == 200) {
                            if (response != null) {
                                Items items = response.body().getItems();
                                int lastEventId = pref.getLastEventID();
                                if (items.getTotal() > 0) {
                                    List<DataItem> dataItems = items.getData();
                                    if (lastEventId == 0) {
                                        pref.setLastEventID(dataItems.get(0).getId());
                                        System.err.println("Old Event : " + dataItems.get(0).getDeviceName() + " " + dataItems.get(0).getMessage());
                                    } else {
                                        for (DataItem data : dataItems) {
                                            System.err.println("Events : " + dataItems.size());
                                            if (lastEventId == data.getId()) return;

                                            Intent intent = new Intent(getBaseContext(), EventMapActivity.class);
                                            intent.putExtra("id", data.getAlertId());
                                            intent.putExtra("deviceName", data.getDeviceName());
                                            intent.putExtra("message", data.getMessage());
                                            intent.putExtra("speed", data.getSpeed());
                                            intent.putExtra("time", data.getTime());
                                            intent.putExtra("lat", data.getLatitude());
                                            intent.putExtra("lng", data.getLongitude());
                                            pref.setLastEventID(data.getId());
                                            showNotification(getBaseContext(),
                                                    data.getName(),
                                                    data.getDeviceName() + " " + data.getMessage(),
                                                    intent,
                                                    createNotificationRequestCode());
                                        }
                                    }
                                }
                            }
                        } else {
                            Log.e(TAG, "onResponse: getEvents Code " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<EventsResponse> call, Throwable t) {

                    }
                });
    }

    public void showNotification(Context context, String title, String message, Intent intent, int reqCode) {
        System.err.println("showNotification -> " + title + " " + message + " " + reqCode);
        String nChannel = getNotificationChannelID();
        CharSequence name = "alfaplus_channel";// The user-visible name of the channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_IMMUTABLE);
//            String CHANNEL_ID = "alfaplus_channel";
            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.notification);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, nChannel)
                    .setSmallIcon(R.drawable.alpha)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setContentIntent(pendingIntent);
            Notification notification = notificationBuilder.build();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                CharSequence name = "Channel Name";// The user-visible name of the channel.
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(nChannel, name, importance);
                mChannel.setSound(sound, audioAttributes);
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationManager.notify(reqCode, notification);
        } else {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_ONE_SHOT);
//            String CHANNEL_ID = "alfaplus_channel";
            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.notification);
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, nChannel)
                    .setSmallIcon(R.drawable.alpha)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setContentIntent(pendingIntent);
            Notification notification = notificationBuilder.build();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(nChannel, name, importance);
                mChannel.setSound(sound, audioAttributes);
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationManager.notify(reqCode, notification);
        }
    }

//    public int createNotificationChannelID() {
//        return 11;
//    }

    public String getNotificationChannelID() {
        return String.valueOf(ALERT_CHANNEL_ID++);
    }


    public int createNotificationRequestCode() {
        channelIndex += 1;
        return channelIndex;
    }

}