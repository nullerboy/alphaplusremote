package com.alfaplus.tracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SMSListener extends BroadcastReceiver {

    public SMSInterface sms;

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        Object[] objects = (Object[]) bundle.get("pdus");

        String number = "";
        StringBuilder message = new StringBuilder();

        for (Object o : objects) {

            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) o);
            number = smsMessage.getDisplayOriginatingAddress();
            if (number.contains("+98")) {
                number = number.replace("+98", "0");
            }
            message.append(smsMessage.getMessageBody());

        }

        sms.onReceive(number, message.toString());

    }

    public void setSmsInterface(SMSInterface smsInterface) {
        this.sms = smsInterface;
    }

}
